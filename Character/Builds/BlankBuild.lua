Build = {
	Diablo = {
		StartMsg = "Running to Diablo",
		KillMsg = "Diablo Spawning",
		TPLoc = {7792, 5292},
	},
	Chaos = {
		StartMsg = "Clearing Chaos Sanctuary",
		KillMsg = "Diablo Spawning",
		TPLoc = {7792, 5292},
	},
	Baal = {
		Kill = true,
		StartMsg = "Baalin",
		TPLoc = {15116, 5006},
		TPClearingMsg = "Tp up, Wait for me to clear I hate ppl who wine!",
		TPSafeMsg = "Tp Safe, Quit being babies!",
		WaveMsg = {
			"Wave 1",
			"Wave 2",
			"Wave 3",
			"Wave 4",
			"Wave 5",
		},
		KillMsg = "Enough Waiting, Lets Do This",
		WaveSkill = {}, --[[ Auras only, can list up to 5 ]]--
		PrecastLocationsX = {15095, 15095, 15095, 15095, 15095},
		PrecastLocationsY = {5031, 5031, 5031, 5031, 5031},
	},
}