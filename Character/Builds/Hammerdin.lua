Build = {
	Diablo = {
		StartMsg = "Running to Diablo",
		KillMsg = "Diablo Spawning",
		TPLoc = {7792, 5292},
	},
	Chaos = {
		StartMsg = "Clearing Chaos Sanctuary",
		KillMsg = "Diablo Spawning",
		TPLoc = {7792, 5292},
	},
	Cows = {
		StartMsg = "Clearing Cow Level",
		KillMsg = "Killing King",
		KillKing = false,
	},
	Baal = {
		Kill = true,
		StartMsg = "Baalin",
		TPLoc = {15116, 5006},
		TPClearingMsg = "Tp up, Wait for me to clear I hate ppl who wine!",
		TPSafeMsg = "Tp Safe, Quit being babies!",
		WaveMsg = {
			"Wave 1",
			"Wave 2",
			"Wave 3",
			"Wave 4",
			"Wave 5",
		},
		KillMsg = "Enough Waiting, Lets Do This",
		WaveSkill = {}, --[[ Auras only, can list up to 5 ]]--
		PrecastLocationsX = {15095, 15095, 15095, 15095, 15095},
		PrecastLocationsY = {5031, 5031, 5031, 5031, 5031},
	},
}

Build.StopAreaAttacks = 4
Build.MaxHolyBoltDistance = 16
Build.MinHolyBoltDistance = 8

Build.PreAttack = function(x, y)
	Skill.Select("Concentration")
	Skill.CastAt(x, y, 'BlessedHammer', 180)
	return true
end

Build.PreWalk = function()
	Skill.Select('Vigor')
end

Build.TownCast = function()
	HolyShieldCheck()
end

-- needs looked over maybe revised
Kill.NPC = function(Boss)
	local Npc = Ao.npcs:find(Boss.uid)
	if npc:IsImmune(ResistType.Magic) then
		while Npc.uid ~= 0 and Npc.mode == NPCMode.Alive do
		    local Corpse = Ao.npcs:findCorpseInRadius(Ao.me.x, Ao.me.y, 15)
			if distance(Ao.me.x, Ao.me.y, Npc.x, Npc.y) > Build.MaxHolyBoltDistance or distance(Ao.me.x, Ao.me.y, Npc.y, Npc.y) < Build.MinHolyBoltDistance then
				local Location = Get.PointInQuadrant(Npc.x, Npc.y, Get.LeastPopulusQuadrant(Npc.x, Npc.y, 12), 12) -- needs work
				Move.ToLocation(Location.x, Location.y)
			end
			if Corpse.uid ~= 0 then
			    Skill.Select("Redemption")
			else
		        Skill.Select("Concentration") -- Redemption is effect is low compared to concentration giving merc a huge boost
		    end
			Skill.CastOn("NPC", Npc.uid, "HolyBolt", 180)
			Npc = Ao.npcs:find(Boss.uid)
		end
	else
	    if distance(Npc.x, Npc.y, Ao.me.x, Ao.me.y) > 25 then
			Move.ToLocation(Npc.x, Npc.y)
		else
			Skill.CastOn("NPC", Npc.uid, "Teleport", 360)
		end
		while Npc.uid ~= 0 and Npc.mode == NPCMode.Alive do
			Npc = Ao.npcs:find(Boss.uid)
			if distance(Ao.me.x, Ao.me.y, Npc.x, Npc.y) > 5 then
				Skill.CastOn("NPC", npc.uid, "Teleport", 360)
			end
			local Corpse = Ao.npcs:findCorpseInRadius(Ao.me.x, Ao.me.y, 15)
			if NeedHeal(true) and Corpse.uid ~= 0 and Ao.npcs:countInRadius(Ao.me.x, Ao.me.y, 15) < 5 then
				Skill.Select("Redemption")
			else
				Skill.Select("Concentration")
			end
			Skill.CastOn("NPC", npc.uid, "BlessedHammer", 180)
		end
	end
end

Kill.Player = function(pl) -- ported from Builds

end

-- not build specific
Kill.AllInRadius = function(x, y, radius) -- ported from Builds
	Kill.HeroesInRadius(x, y, radius)
	local Npc = Ao.npcs:findInRadius(x, y, radius)
	while Npc.uid > 0 do
		Kill.NPC(Npc)
		Npc = Ao.npcs:findInRadius(x, y, radius)
	end
end

-- not build specific
Kill.HeroesInRadius = function(x, y, radius) -- ported from Builds
	local Hero = Ao.npcs:findHeroInRadius(x, y, radius)
	while Hero.uid ~= 0 do
		Kill.NPC(Hero)
		Hero = Ao.npcs:findHeroInRadius(x, y, radius)
	end
end

-- not build specific
Kill.Minions = function() -- ported from 
	Kill.AllInRadius(Ao.me.x, Ao.me.y, 10, true)
end


--Description: Enters Portal and Kills Baal if Build.Baal.Kill = true
Kill.Baal = function()
	local BaalPortal = Ao.objects:find(GameObjectID.BaalsPortal)
	Move.ToLocation(BaalPortal.x, BaalPortal.y)
	Log(2, "Bosses", "Killing Baal", true)
	AO.PublicChat(Build.Baal.KillMsg)
	if Config.Pickit.DumpItems then -- should this not be global like in pickit?
	    Item.Dump() 
	end 
	Skill.PreCast(true) -- needs work
    
    while FindBaalThrone() do 
        AO.Sleep(Settings.General.InteractDelay/20) 
    end -- needs work
    
	while Ao.maps:getLevel() ~= AreaLevel.TheWorldstoneChamber do
	    -- Need a loop to make it enter portal every time
		if Move.ViaPortal('TheWorldstoneChamber') then 
		    break 
		end
	end
	local Npc = Move.ToNpc('BaalCrab')
	if Npc.uid <= 0 then 
	    Ao:Chat('No Baal') 
	    return false 
	end
	Kill.NPC(Npc)
end

Kill.Waves = function() -- ported from bosses.lua
	if wave == 0 or wave == nil then 
		wave = 1 
	end
	if MF.CheckBaal() then 
		MF.PreWave() 
	else 
		return 
	end
	Log(2, "Bosses", "Killing wave "..wave, true)
	PublicChat(Build.Baal.WaveMsg[wave])
	local npc = Ao.npcs:findInRadius(center.x, center.y, center.rad)
	-- killing the wave mobs
	while (npc.uid ~= 0) and FindBaalThrone() do  
		Kill.AllInRadius(center.x, center.y, center.rad)
		npc = Ao.npcs:findInRadius(center.x, center.y, center.rad)
	end
	-- hydra checks to move away
	if (Ao.npcs:findByNpcCode(NPCCode.Hydra).uid ~= 0 or Ao.npcs:findByNpcCode(NPCCode.Hydra2).uid ~= 0 or Ao.npcs:findByNpcCode(NPCCode.Hydra3).uid ~= 0) and wave == 3 then 
	    while (Ao.npcs:findByNpcCode(NPCCode.Hydra).uid ~= 0 or Ao.npcs:findByNpcCode(NPCCode.Hydra2).uid ~= 0 or Ao.npcs:findByNpcCode(NPCCode.Hydra3).uid ~= 0) do
	        if (Ao.me.x ~= Build.Ball.TPLoc[0]) then 
				Move.ToLocation(Build.Ball.TPLoc[0], Build.Ball.TPLoc[1])
	        else 
				Sleep(100) 
			end
	    end
	end
	-- if cant find baal then we need to kill him
	if not MF.CheckBaal() then 
		return 
	end
	if wave < 5 then 
		wave = wave + 1 
		return true
	else 
		return false
	end
end

-- Description: If portal for Uber Tristam exists calls functions to kill ubers
Kill.Ubers = function() -- ported from torchhunt.lua
	if Farm.Portal.UberTristram.x ~= 0 then
		Move.ToLocation(Farm.Portal.UberTristram.x, Farm.Portal.UberTristram.y)
	else
		Log(3, "Farming", "Torch Farming : Cant find Uber Tristram Portal")
		return
	end
	Log(2, "Farming", "Torch Farming: Taking Uber Tristram Portal")
	Move.ViaPortal('Tristram')
	MF.Ubers("UberMephisto", "Torch")
	MF.Ubers("UberBaal", "Torch")
	MF.Ubers("UberDiablo", "Torch")
end
