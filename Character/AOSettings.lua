Bot = {
    Public = false, -- make bot public IE cast TPs and chat with players in game
    Messages = { -- general msgs spammed by bot if PublicChat is true
    	ChickenTown = "Need More Ale",
    	ChickenExit = "Dueces",
    	OnHostile = "Dirty Girl...",
    	HotTP = "Hot TP",
    	ColdTP = "Clear TP",
    	NewGame = "NEXT!",
    	MobsMsg = "%d Close To Tp, %d Total Mobs",
    },
    BlockUserAction = false, -- block user input (for most things)
    StopOnDClone = true, -- stop in game On DClone spawn regardless of other actions
    Pickit = {
        PickitFolder = "Default",
	    MinGoldPile = 1000, -- minimum ammount of gold to pick up
        InTown = true, -- activate pickit while in town
        Dump = false, -- dump items during baal run in safe location before drops happen then prioritize what it picks based on item priority and space available
	    AutoDump = false, -- auto dump items as they are ID'ed in other words disable the use of Cain. Will also not keep items to sell unless specified within pickit.
	    Craft = {
	        Rune = {}, -- runes to make, simply list runes you would like bot to make it will pick required items idepentent of pickit settings.
		    Gems = {}, -- gems to make (ideal for crafters) essentiall a list bot will gather supplies to make ie put Flawless Ruby bot will collect 3 rubies. List is comma seperated
		    Essences = false, -- cube essences automatically outside farming
		    GC = false, -- reroll ilvl 91+ grand charms
	    },
    },
    Interact = {
	    SendInvites = true, -- invite other players
        AcceptInvites = true, -- accept invites from other players
        KillHostile = false, -- kill hostile players
        SquelchLevel = 0, -- the maximum level of the characters to automatically squelch IE 40 will squelch all characters below lvl 40
    },
    Log = {
	    Level = 2, --[[ 1 = debug, 2 = info, 3 = errors, 9 = no logging ]]--
        DisplayMsg = true, -- display message on screen?
        Sold = true, -- log sold items
        Dumped = true, -- log dumped items
        Stashed = true, -- log stashed items
        Skipped = true, -- log skipped items
    },
}