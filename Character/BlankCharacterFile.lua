Character = {
    Setup = {
        Automatic = false, -- true or false value, if true bot will change setup values based on character setup upon initial log in.
        Build = "Hammerdin",
        Buffs = {} -- list of skills to cast and maintain while bot is operating
        Drink = {
            Mana = 30, -- percentage of mana win bot drinks mana pot
        	RejuvMana = 50, -- percentage of mana when bot will drink rejuv
        	Health = 60, -- percentage of health when bot will drink health pot
        	RejuvHealth = 10, -- percentage of health when bot drinks rejuv
        }
        Chicken = {
        	ToTown = 40, -- percentage of health when bot will chicken to toen
        	Exit = 30, -- percentage of health when bot will exit game
        	OutOfPots = true, -- buys more pots in town if run out
	    	OnHostile = false, -- chicken if hosteled by player
	    }
        Merc = {
        	Use = true, -- set to true to use merc
        	Resurrect = false, -- auto resurrect merc when it is dead or when he dies
        	Health = 60, -- value to give merc Helath
        	Rejuv = 30, -- value to give merc Rejuv
        }
        Inventory = { -- layout of your inventory *NOTE:  everything must be aligned to the right as bot places items from lett to right thus if you align to the left bot will not function because it thinks your inventory is full
        	1,1,1,1,1,1,1,1,1,1,
        	1,1,1,1,1,1,1,1,1,1,
        	1,1,1,1,1,1,1,1,1,1,
        	1,1,1,1,1,1,1,1,1,1,
        }
        Belt = {-- values must be entered in increments 4 based on belt size IE 12 purple 4 blue, not 6 purple 6 blue 4 red as 6 is not evenly divisible by 4
            Order = {"Blue", "Red", "Purple", "Purple"} -- order of pots by color based on numbers/quantities specified below.
        	Blue = 4, -- number of Mana pots to maintain
        	Red = 4, -- number of total Health pots to maintain
        	Purple = 8, -- number of total Rejuvs to maintain
        }
    }
    Run = {
        Sequence = "", -- a list or simplified sequence of what you would like the bot to do EXP: 'town andy town meph town chaos town baal'
    },
    Path = {
        Teleport = true, -- choose weather you want to teleport or use character defaults IE pally = vigor, sorc = teleport, assin = burst of speed, barb = leap, necor & druid are stuck to walking, if you do not have skills mentioned will default to walking.
    	Kill = {
    		All = false, -- clear the path to the boss
    		Heroes = false, -- kill all heroes along path to boss
    		Minions = true, -- kill all minions for each boss you run
    	}
    	Get = {
    	    Chests = {
    	        All = false, -- get all chests
    	        Spakly = false, -- get sparkly chests
    	    }
    		Shrines = {} -- list of shrines to activate while patching( if empty then false no need for extra variable )
    	}
    }
}