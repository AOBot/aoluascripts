UnloadDone = function() --0x05
	--[[ Called when changing acts, instead of loading new act is the unload of last act
		Options for use :
			none that I see, exiting doesnt matter much to lua
	--]]
	--Ao:Chat("UnloadDone")
end

GameOver = function() --0xB0
	--[[ Called when exiting a game
		Options for use : 
			none that I see, exiting doesnt matter much to lua
	--]]
	--Ao:Chat("GameOver")
end

LoadAct = function() --0x03
	--[[ Called when changing acts
		Options for use : 
			Moving between areas/acts has been a known way to clear the "interaction bug"
	--]]
	local mapid = Ao:getNumber("LoadActMapId")
	local level = Ao:getNumber("LoadActLevel")
	--Ao:Chat("LoadAct : mapid = "..mapid.." level = "..getEnumKey(AreaLevel, level))
end

MapAdd = function()
	--[[ Called when moving around and loading new tiles for maps
		Options for use :
			could be used as movement confirmation, though do not use arealevel data as "where we are" because its not the case
			definent good use for teleport confirmation, it is called even when teleporting "in place"
			Also good for checking vs taking wp or tp
	--]]
	local x = Ao:getNumber("MapAddX")
	local y = Ao:getNumber("MapAddY")
	local level = Ao:getNumber("MapAddLevel")
	--Ao:Chat("MapAdd : x = "..x.." y = "..y.." level = "..getEnumKey(AreaLevel, level))
end

MapRemove = function()
	--[[ Called similarly to MapAdd event removing past tiles
		Options for use : 
			MapAdd is more effective for movement confirmation, as it is called much more than MapRemove
	--]]
	local x = Ao:getNumber("MapRemoveX")
	local y = Ao:getNumber("MapRemoveY")
	local level = Ao:getNumber("MapRemove")
	--Ao:Chat("MapRemove : x = "..x.." y = "..y.." level = "..getEnumKey(AreaLevel, level))
end

GameFlags = function()
	--[[ Called upon game creation very early
		Options for use : 
			Only real use would be confirmation of difficulty during rush, otherwise core holds all this information
	--]]
	local difficulty = Ao:getNumber("GameFlagsDifficulty")
	local expansion = Ao:getNumber("GameFlagsExpansion")
	--Ao:Chat("GameFlags : difficulty = "..difficulty.." expansion = "..expansion)
end

GameHandshake = function()
	--[[ Called before loading acts, does not confer much information either
		Options for use : 
			Called too early to be of any real use
	--]]
	local unittype = Ao:getNumber("GameHandshakeType")
	local uid = Ao:getNumber("GameHandshakeUID")
	--Ao:Chat("GameHandshake : uid = "..uid)
end

NPCGetHit = function()
	--[[ Called when an npc gets hit, though not every time (seems like it registers only physical attacks)
		Options for use : 
			not much due to its inaccuracy
			seems maybe its called for sure when npc dies, not just hit?
	--]]
	local unittype = Ao:getNumber("NPCGetHitUnitType")
	local uid = Ao:getNumber("NPCGetHitUID")
	local life = Ao:getNumber("NPCGetHitLife")
	--Ao:Chat("NPCGetHit : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid.." life = "..life)
end

PlayerStop = function()
--[[ Sent when a player joins (ie start location) range likely 2 screens
	Options for use : 
		pvp really, and defending leechers/followers
--]]
	local unittype = Ao:getNumber("PlayerStopUnitType")
	local uid = Ao:getNumber("PlayerStopUID")
	local life = Ao:getNumber("PlayerStopLife")
	local x = Ao:getNumber("PlayerStopX")
	local y = Ao:getNumber("PlayerStopY")
	--Ao:Chat("PlayerStop : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid.." life = "..life.." x = "..x.." y = "..y)
end

PlayerInfoJoined = function()
--[[ Works for self
	Options for use :
		not much other than party invite/join
--]]
	local playername = Ao:getString("PlayerInfoPlayerName")
	local accountname = Ao:getString("PlayerInfoAccountName")
	--Ao:Chat("PlayerInfoJoined : playername = "..playername.." accountname = "..accountname)
end

PlayerInfoLeft = function()
--[[
	Options for use :
		one way to check if a master left game
--]]
	local playername = Ao:getString("PlayerInfoPlayerName")
	local accountname = Ao:getString("PlayerInfoAccountName")
	--Ao:Chat("PlayerInfoLeft : playername = "..playername.." accountname = "..accountname)
end

PlayerInfoSlain = function()
	local playername = Ao:getString("PlayerInfoPlayerName")
	local accountname = Ao:getString("PlayerInfoAccountName")
	--Ao:Chat("PlayerInfoSlain : playername = "..playername.." accountname = "..accountname)
end

PlayerInfoDClone = function()
	local playername = Ao:getString("PlayerInfoPlayerName")
	local accountname = Ao:getString("PlayerInfoAccountName")
	--Ao:Chat("PlayerInfoDClone : playername = "..playername.." accountname = "..accountname)
end

PlayerInGame = function()
--[[ Works for self, sent right after Player Join Game
	Options for use : 
		muting, inviting, uid fetching 
--]]
	local playername = Ao:getString("PlayerInGameName")
	local uid = Ao:getNumber("PlayerInGameUID")
	local class = Ao:getNumber("PlayerInGameClass")
	local partyid = Ao:getNumber("PlayerInGamePartyID")
	local level = Ao:getNumber("PlayerInGameLevel")
	--Ao:Chat("PlayerInGame : playername = "..playername.." uid = "..uid.." class = "..getEnumKey(CharacterClass, class).." partyid = "..partyid.." level = "..level)
end

OpenWaypoint = function()
--[[ As stated, called when opening a waypoint
	Options for use : 
		interaction testing/setting
--]]
	local uid = Ao:getNumber("OpenWaypointUID")
	--Ao:Chat("OpenWaypoint : uid = "..uid)
end

AssignWarp = function()
--[[ Tells location of nearby warp when passing by
	Options for use :
		core already has this information in map... though might be a good reference still
--]]
	local unittype = Ao:getNumber("AssignWarpUnitType")
	local uid = Ao:getNumber("AssignWarpUID")
	local id = Ao:getNumber("AssignWarpID")
	local x = Ao:getNumber("AssignWarpX")
	local y = Ao:getNumber("AssignWarpY")
	--Ao:Chat("AssignWarp : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid.." id = "..getEnumKey(WarpType, id).." x = "..x.." y = "..y)
end

RemoveGameObjectPlayer = function()
--[[ Tells us when we are out of a range of a player (2 screen appx)
	Options for use : 
		Since just uid is sent, might be hard to use for chasing down to pk or any other likelihood
--]]
	local uid = Ao:getNumber("RemoveGameObjectUID")
	--Ao:Chat("RemoveGameObjectPlayer : uid = "..uid)
end

RemoveGameObjectNPC = function()
--[[ Sent when we are leaving perception area of npcs
	Options for use : 
		Could be used as a double check for clearing areas
--]]
	local uid = Ao:getNumber("RemoveGameObjectUID")
	local npc = Ao.npcs:find(uid)
	--Ao:Chat("RemoveGameObjectNPC : npccode = "..getEnumKey(NPCCode, npc.id))
	--Ao:Chat("RemoveGameObjectNPC : uid = "..uid)
end

RemoveGameObjectObject = function()
--[[ Sent when leaving perception of an object
	Options for use : 
		Not much use outside of opening chests maybe
--]]
	local uid = Ao:getNumber("RemoveGameObjectUID")
	--Ao:Chat("RemoveGameObjectObject : uid = "..uid)
end

RemoveGameObjectItem = function()
--[[ sent when picking an item
	Options for use : 
		be used to double check an item pick
--]]
	local uid = Ao:getNumber("RemoveGameObjectUID")
	--Ao:Chat("RemoveGameObjectItem : uid = "..uid)
end

RemoveGameObjectWarp = function()
--[[ Leaving perception range of a warp
	Options for use : 
		not much really...
--]]
	local uid = Ao:getNumber("RemoveGameObjectUID")
	--Ao:Chat("RemoveGameObjectWarp : uid = "..uid)
end

SetGameObjectMode = function()
--[[ Sent with assign of object
	Options for use : 
		not much beyond double checking
--]]
	local unittype = Ao:getNumber("SetGameObjectModeUnitType")
	local uid = Ao:getNumber("SetGameObjectModeUID")
	local canchangeback = Ao:getNumber("SetGameObjectModeCanChangeBack")
	local objectmode = Ao:getNumber("SetGameObjectModeObjectMode")
	--Ao:Chat("SetGameObjectMode : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid.." canchangeback = "..canchangeback.." objectmode = "..objectmode)
end

PlayerMove = function()
--[[ Called when another player moves, is a constant call
	Options for use : 
		following a player
		killing opposing pc
--]]
	local uid = Ao:getNumber("PlayerMoveUID")
	local tox = Ao:getNumber("PlayerMoveToX")
	local toy = Ao:getNumber("PlayerMoveToY")
	local fromx = Ao:getNumber("PlayerMoveFromX")
	local fromy = Ao:getNumber("PlayerMoveFromY")
	local movementtype = Ao:getNumber("PlayerMoveMovementType")
	--Ao:Chat("PlayerMove : uid = "..uid.." tox = "..tox.." toy = "..toy.." fromx = "..fromx.." fromy = "..fromy.." movementtype = "..getEnumKey(MovementType, movementtype))
end

ReportKill = function()
--[[ Called when merc kills (and sometimes just when he attacks)
	Options for use : 
		not much as its mainly a merc thing
--]]
	local unittype = Ao:getNumber("ReportKillUnitType")
	local uid = Ao:getNumber("ReportKillUID")
	--Ao:Chat("ReportKill : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid)
end

PlayerReassignPlayer = function()
--[[ Called when transitioning to a new area, also when teleporting for players
	Options for use : 
		Area transition (only "solid" transitions)
		Teleporting
--]]
	local uid = Ao:getNumber("PlayerReassignUID")
	local x = Ao:getNumber("PlayerReassignX")
	local y = Ao:getNumber("PlayerReassignY")
	--Ao:Chat("PlayerReassignPlayer : uid = "..uid.." x = "..x.." y = "..y)
end

PlayerReassignNPC = function()
--[[ Called when teleporting (calling merc) and randomly for npcs
	Options for use : 
		not much use here, other than updated npc location
--]]
	local uid = Ao:getNumber("PlayerReassignUID")
	local x = Ao:getNumber("PlayerReassignX")
	local y = Ao:getNumber("PlayerReassignY")
	--Ao:Chat("PlayerReassignNPC : uid = "..uid.." x = "..x.." y = "..y)
end

POT = function()
--[[ Called when pot is used, and then periodically afterwards
	Options for use : 
		x/y updates mostly
--]]
	local life = Ao:getNumber("POTLife")
	local mana = Ao:getNumber("POTMana")
	local stamina = Ao:getNumber("POTStamina")
	local x = Ao:getNumber("POTX")
	local y = Ao:getNumber("POTY")
	--Ao:Chat("POT : life = "..life.." mana = "..mana.." stamina = "..stamina.." x = "..x.." y = "..y)
end

SmallGoldAdd = function()
--[[ When collecting gold
	Options for use : 
		be able to calculate gold per game?
--]]
	local gold = Ao:getNumber("SmallGoldAddGold")
	--Ao:Chat("SmallGoldAdd : gold = "..gold)
end

ByteToExperience = function()
--[[ When gaining experience
	Options for use : 
		be able to calculate exp per game?
--]]
	local experience = Ao:getNumber("ByteToExperienceExperience")
	--Ao:Chat("ByteToExperience : experience = "..experience)
end

WordToExperience = function()
--[[ When gaining experience
	Options for use : 
		be able to calculate exp per game?
--]]
	local experience = Ao:getNumber("WordToExperienceExperience")
	--Ao:Chat("WordToExperience : experience = "..experience)
end

DWordToExperience = function()
--[[ When gaining experience
	Options for use : 
		be able to calculate exp per game?
--]]
	local experience = Ao:getNumber("DWordToExperienceExperience")
	--Ao:Chat("DWordToExperience : experience = "..experience)
end

AttributeByte = function()
--[[ Called when adjusting stats, at join and then when changing equip/etc
	Options for use : 
		not much as this is just information
--]]
	local stattype = Ao:getNumber("AttributeByteStatType")
	local amount = Ao:getNumber("AttributeByteAmount")
	--Ao:Chat("AttributeByte : stattype = "..getEnumKey(StatType, stattype).." amount = "..amount)
end

AttributeWord = function()
--[[ Called when adjusting stats, at join and then when changing equip/etc
	Options for use : 
		not much as this is just information
--]]
	local stattype = Ao:getNumber("AttributeWordStatType")
	local amount = Ao:getNumber("AttributeWordAmount")
	--Ao:Chat("AttributeWord : stattype = "..getEnumKey(StatType, stattype).." amount = "..amount)
end

AttributeDWord = function()
--[[ Called when adjusting stats, at join and then when changing equip/etc
	Options for use : 
		not much as this is just information
--]]
	local stattype = Ao:getNumber("AttributeDWordStatType")
	local amount = Ao:getNumber("AttributeDWordAmount")
	--Ao:Chat("AttributeDWord : stattype = "..getEnumKey(StatType, stattype).." amount = "..amount)
end

UpdateSkill = function()
--[[  Adding a new skill (switching to BO set)
	Options for use : 
		be able to tell which weaponset we are on?
--]]
	local uid = Ao:getNumber("UpdateSkillUID")
	local skill = Ao:getNumber("UpdateSkillSkill")
	local baselevel = Ao:getNumber("UpdateSkillBaseLevel")
	--Ao:Chat("UpdateSkill : uid = "..uid.." skill = "..getEnumKey(SkillType, skill).." baselevel = "..baselevel)
end

UpdateSkillStatus = function()
--[[ Books of townportal and identify updates
	Options for use : 
		successful use of IDing/TPing
--]]
	local skilltype = Ao:getNumber("UpdateSkillStatusSkillType")
	local amount = Ao:getNumber("UpdateSkillStatusAmount")
	--Ao:Chat("UpdateSkillStatus : skilltype = "..getEnumKey(SkillType, skilltype).." amount = "..amount)
end

AssignSkill = function()
--[[ When a skill is set to left/right (switching weapons, joining game)
	Options for use : 
		Knowing what skill is set....
--]]
	local unittype = Ao:getNumber("AssignSkillUnitType")
	local uid = Ao:getNumber("AssignSkillUID")
	local hand = Ao:getNumber("AssignSkillHand")
	local skilltype = Ao:getNumber("AssignSkillType")
	local chargeditemuid = Ao:getNumber("AssignSkillChargedItemUID")
	--Ao:Chat("AssignSkill : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid.." hand = "..getEnumKey(SkillHand, hand).." skilltype = "..getEnumKey(SkillType, skilltype).. " charged item uid = "..chargeditemuid)
end

GameMessageReceived = function()
--[[ Getting a shrine, 
	Options for use : 
--]]
	local playername = Ao:getString("GameMessageReceivedPlayerName")
	local message = Ao:getString("GameMessageReceivedText")
	Ao:Chat("GameMessageReceived : playername = "..playername.." message = "..message)
end

UpdateQuestInfo = function()
--[[ Game Join, npc interaction, opening quest screen
	Options for use : 
		npc interaction
--]]
	--Ao:Chat("UpdateQuestInfo : Theres a lot of data here...")
end

UpdateGameQuestLog = function()
--[[ Game Join, npc interaction
	Options for use : 
		npc interaction
--]]
	--Ao:Chat("UpdateGameQuestLog : Theres a lot of data here...")
end

UpdateItemStats = function()
--[[ repairing, book of tp, book of id
	Options for use : 
		successful repair?
--]]
	local amount = Ao:getNumber("UpdateItemStatsAmount")
	local uid = Ao:getNumber("UpdateItemStatsUID")
	--Ao:Chat("UpdateItemStats : amount = "..amount.." uid = "..uid.." lots of crazy data here too...")
end

UseStackableItem = function()
--[[ Opening cube, scroll of tp, potion, scroll of id
	Options for use :
		opening cube...
--]]
	--Ao:Chat("UseStackableItem : ummm... no data?")
end

PlayerClearCursor = function()
--[[ When selling
	Options for use : 
		Confirming a sell
--]]
	--Ao:Chat("PlayerClearCursor")
end

UnitUseSkillOnTarget = function()
--[[ When using a specific skill (not just attack) from one npc to another unit
	Options for use : 
		Checking if we are being attacked by an npc (not only method!)
--]]
	local skilltype = Ao:getNumber("UnitUseSkillOnTargetSkillType")
	local uid = Ao:getNumber("UnitUseSkillOnTargetUID")
	local target = Ao:getNumber("UnitUseSkillOnTargetTarget")
	local unittype = Ao:getNumber("UnitUseSkillOnTargetUnitType")
	--Ao:Chat("UnitUseSkillOnTarget : skilltype = "..getEnumKey(SkillType, skilltype).." uid = "..uid.." target = "..target.." unittype = "..getEnumKey(UnitType, unittype))
end

UnitUseSkill = function()
--[[ When a specific skill is used on a location
	Options for use : 
		Checking if we are being attacked via location
--]]
	local skilltype = Ao:getNumber("UnitUseSkillSkillType")
	local uid = Ao:getNumber("UnitUseSkillUID")
	local unittype = Ao:getNumber("UnitUseSkillUnitType")
	local x = Ao:getNumber("UnitUseSkillX")
	local y = Ao:getNumber("UnitUseSkillY")
	--Ao:Chat("UnitUseSkill : skilltype = "..getEnumKey(SkillType, skilltype).." uid = "..uid.." unittype = "..getEnumKey(UnitType, unittype).." x = "..x.." y = "..y)
	--Ao:Chat("UnitUseSkill : skilltype = "..skilltype.." uid = "..uid.." unittype = "..unittype.." x = "..x.." y = "..y)
end

AssignGameObject = function()
--[[ Used when an object comes into view (no shrines)
	Options for use : 
		Opening chests, retrieving shrines
--]]
	local uid = Ao:getNumber("AssignGameObjectUID")
	local objectid = Ao:getNumber("AssignGameObjectObjectID")
	local x = Ao:getNumber("AssignGameObjectX")
	local y = Ao:getNumber("AssignGameObjectY")
	local objectmode = Ao:getNumber("AssignGameObjectObjectMode")
	local interacttype = Ao:getNumber("AssignGameObjectInteractType")
	local destination = Ao:getNumber("AssignGameObjectDestination")
	--Ao:Chat("AssignGameObject : uid = "..uid.." objectid = "..getEnumKey(GameObjectID, objectid).." x = "..x.." y = "..y.." objectmode = "..getEnumKey(GameObjectMode, objectmode).." interacttype = "..getEnumKey(GameObjectInteractType, interacttype).." destination = "..getEnumKey(AreaLevel, destination))
end

AssignPlayer = function()
	local uid = Ao:getNumber("AssignPlayerUID")
	local class = Ao:getNumber("AssignPlayerCharacterClass")
	local name = Ao:getString("AssignPlayerPlayerName")
	local x = Ao:getNumber("AssignPlayerX")
	local y = Ao:getNumber("AssignPlayerY")
	Ao:Chat("AssignPlayer : uid "..uid.." x = "..x.." y = "..y.. "missing data we could easily get from this event")
end

NPCMove = function()
--[[ When an npc moves
	Options for use : 
		Checking for running npcs?
--]]
	local uid = Ao:getNumber("NPCMoveUID")
	local x = Ao:getNumber("NPCMoveX")
	local y = Ao:getNumber("NPCMoveY")
	--Ao:Chat("NPCMove : uid = "..uid.." x = "..x.." y = "..y.." lots of more information in this packet")
end

NPCMoveToObject = function()
--[[ When an npc moves to an object
	Options for use : 
		IDK
--]]
	local uid = Ao:getNumber("NPCMoveToObjectUID")
	local movementtype = Ao:getString("NPCMoveToObjectMovementType")
	local currentx = Ao:getNumber("NPCMoveToObjectCurrentX")
	local currenty = Ao:getNumber("NPCMoveToObjectCurrentY")
	local targettype = Ao:getNumber("NPCMoveToObjectTargetType")
	local targetuid = Ao:getNumber("NPCMoveToObjectTargetUID")
	--Ao:Chat("NPCMoveToObject : uid = "..uid.." movementtype = "..movementtype.." currentx = "..currentx.." currenty = "..currenty.." targettype = "..getEnumKey(UnitType, targettype).." targetuid = "..targetuid)
end

SetNPCMode = function()
	local uid = Ao:getNumber("SetNPCModeUID")
	local npcmode = Ao:getNumber("SetNPCModeMode")
	local x = Ao:getNumber("SetNPCModeX")
	local y = Ao:getNumber("SetNPCModeY")
	local life = Ao:getString("SetNPCModeLife")
	local npc = Ao.npcs:find(uid)
	--Ao:Chat("SetNPCMode : npc.id = "..getEnumKey(NPCCode, npc.id))
	--Ao:Chat("SetNPCMode : uid = "..uid.." npcmode = "..getEnumKey(NPCMode, npcmode).." x = "..x.." y = "..y.." life = "..life)
end

MonsterAttack = function()
	local uid = Ao:getNumber("MonsterAttackUID")
	local attacktype = Ao:getNumber("MonsterAttackAttackType")
	local targetuid = Ao:getNumber("MonsterAttackTargetUID")
	local targettype = Ao:getNumber("MonsterAttackTargetType")
	local x = Ao:getNumber("MonsterAttackX")
	local y = Ao:getNumber("MonsterAttackY")
	--Ao:Chat("MonsterAttack : uid = "..uid.." attacktype = "..attacktype.." targetuid = "..targetuid.." targettype = "..getEnumKey(UnitType, targettype).." x = "..x.." y = "..y)
end

NPCStop = function()
	local uid = Ao:getNumber("NPCStopUID")
	local x = Ao:getNumber("NPCStopX")
	local y = Ao:getNumber("NPCStopY")
	local life = Ao:getNumber("NPCStopLife")
	--Ao:Chat("NPCStop : uid = "..uid.." x = "..x.." y = "..y.." life = "..life)
end

AboutPlayer = function()
	local uid = Ao:getNumber("AboutPlayerUID")
	local partyid = Ao:getNumber("AboutPlayerPartyID")
	local level = Ao:getNumber("AboutPlayerLevel")
	local playerrelationshiptype = Ao:getNumber("AboutPlayerPlayerRelationshipType")
	local isinmyparty = Ao:getNumber("AboutPlayerIsInMyParty") --its a bool, I hope number works
	--Ao:Chat("AboutPlayer : uid = "..uid.." partyid = "..partyid.." playerrelationshiptype = "..getEnumKey(PlayerRelationshipType, playerrelationshiptype))
end

PlayerInSight = function()
	local unittype = Ao:getNumber("PlayerInSightUnitType")
	local uid = Ao:getNumber("PlayerInSightUID")
	--Ao:Chat("PlayerInSight : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid)
end

ButtonActions = function()
	local itemuiaction = Ao:getNumber("ButtonActionsItemUIAction")
	--Ao:Chat("ButtonActions : itemuiaction = "..getEnumKey(ItemUIAction, itemuiaction))
end

SummonAction = function()
	local summonactiontype = Ao:getNumber("SummonActionSummonActionType")
	local skilltree = Ao:getString("SummonActionSkillTree")
	local pettype = Ao:getNumber("SummonActionPetType")
	local playeruid = Ao:getNumber("SummonActionPlayerUID")
	local petuid = Ao:getNumber("SummonActionPetUID")
	--Ao:Chat("SummonAction : summonactiontype = "..getEnumKey(SummonActionType, summonactiontype).." skilltree = "..skilltree.." pettype = "..getEnumKey(PetType, pettype).." playeruid = "..playeruid.." petuid = "..petuid)
end

MercAssign = function()
	local id = Ao:getNumber("MercAssignID")
	local owneruid = Ao:getNumber("MercAssignOwnerUID")
	local uid = Ao:getNumber("MercAssignUID")
	--Ao:Chat("MercAssign : owneruid = "..owneruid.." uid = "..uid)
end

PortalOwnership = function()
	local owneruid = Ao:getNumber("PortalOwnershipOwnerUID")
	local localuid = Ao:getNumber("PortalOwnershipPortalLocalUID")
	local remoteuid = Ao:getNumber("PortalOwnershipPortalRemoteUID")
	--Ao:Chat("PortalOwnership : owneruid = "..owneruid.." localuid = "..localuid.." remoteuid = "..remoteuid)
end

PlayersRelationStatus = function()
	local uid = Ao:getNumber("PlayersRelationStatusUID")
	local partyrelationshiptype = Ao:getNumber("PlayersRelationStatusPartyRelationshipType")
	--Ao:Chat("PlayersRelationStatus : uid = "..uid.." partyrelationshiptype = "..getEnumKey(PartyRelationshipType, partyrelationshiptype))
end

PlayerRelationship = function()
	local subjectuid = Ao:getNumber("PlayerRelationshipSubjectUID")
	local objectuid = Ao:getNumber("PlayerRelationshipObjectUID")
	local relation = Ao:getNumber("PlayerRelationshipRelation")
	--Ao:Chat("PlayerRelationship : subjectuid = "..subjectuid.." objectuid = "..objectuid.." relation = "..relation)
end

AssignPlayerToParty = function()
	local uid = Ao:getNumber("AssignPlayerToPartyUID")
	local number = Ao:getNumber("AssignPlayerToPartyNumber")
	--Ao:Chat("AssignPlayerToParty : no data?")
end

AssignPlayerCorpse = function()
	local assignment = Ao:getNumber("AssignPlayerCorpseAssign")
	local playeruid = Ao:getNumber("AssignPlayerCorpsePlayerUID")
	local corpseuid = Ao:getNumber("AssignPlayerCorpseCorpseUID")
	--Ao:Chat("AssignPlayerCorpse : assignment = "..assignment.." playeruid = "..playeruid.." corpseuid = "..corpseuid)
end

PartyMemberUpdate = function()
	local isplayer = Ao:getNumber("PartyMemberUpdateIsPlayer")
	local lifepercent = Ao:getNumber("PartyMemberUpdateLifePercent")
	local uid = Ao:getNumber("PartyMemberUpdateUID")
	local area = Ao:getNumber("PartyMemberUpdateArea")
	--Ao:Chat("PartyMemberUpdate : isplayer = "..isplayer.." lifepercent = "..lifepercent.." uid = "..uid.." area = "..getEnumKey(AreaLevel, area))
end

DelayedState = function()
	local unittype = Ao:getNumber("DelayedStateUnitType")
	local uid = Ao:getNumber("DelayedStateUID")
	local statetype = Ao:getNumber("DelayedStateStateType")
	--Ao:Chat("DelayedState : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid.." statetype = "..getEnumKey(StateType, statetype))
end

SetState = function()
	local unittype = Ao:getNumber("SetStateUnitType")
	local uid = Ao:getNumber("SetStateUID")
	local statetype = Ao:getNumber("SetStateStateType")
	--Ao:Chat("SetState : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid.." statetype = "..getEnumKey(StateType, statetype).." all states have stats info not being listed")
end

NextBaalWave = function()
	local npccode = Ao:getNumber("NextBaalWaveNPCCode")
	--Ao:Chat("NextBaalWave : npccode = "..getEnumKey(NPCCode, npccode))
end

EndState = function()
	local unittype = Ao:getNumber("EndStateUnitType")
	local uid = Ao:getNumber("EndStateUID")
	local statetype = Ao:getNumber("EndStateStateType")
	--Ao:Chat("EndState : unittype = "..getEnumKey(UnitType, unittype).." uid = "..uid.." statetype = "..getEnumKey(StateType, statetype))
end

AddUnit = function()
	local uid = Ao:getNumber("AddUnitUID")
	local unittype = Ao:getNumber("AddUnitUnitType")
	--Ao:Chat("AddUnit : uid = "..uid.." unittype = "..getEnumKey(UnitType, unittype))
end

AssignNPC = function()
	--Ao:Chat("AssignNPC : lots of data here hah!")
end

Pong = function()
	--Ao:Chat("Pong : ...yeah")
end

SkillsLog = function()
	local uid = Ao:getNumber("SkillsLogUID")
	--Ao:Chat("SkillsLog : uid = "..uid.." lots of more information here to look at")
end

PlayerLifeManaChange = function()
	local x = Ao:getNumber("PlayerLifeManaChangeX")
	local y = Ao:getNumber("PlayerLifeManaChangeY")
	--Ao:Chat("PlayerLifeManachange : x = "..x.." y = "..y.." other info applied straight to me")
end

WalkVerify = function()
	local stamina = Ao:getNumber("WalkVerifyStamina")
	--Ao:Chat("WalkVerify : stamina = "..stamina.." remaining info applied to me")
end

SwitchWeaponSet = function()
	--Ao:Chat("SwitchWeaponSet : no data to use")
end