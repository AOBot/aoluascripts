IncludedFiles = {}

Automate = "\\bot\\Actions\\Automate.lua"
Farm = "\\bot\\Actions\\Farm.lua"
MF = "\\bot\\Actions\\MF.lua"
Rush= "\\bot\\Actions\\Rush.lua"
Shop = "\\bot\\Actions\\Shop.lua"

Container = "\\bot\\Reference\\Container.lua"
Log = "\\bot\\Reference\\Log.lua"
Movement = "\\bot\\Reference\\Movement.lua"
Skill = "\\bot\\Reference\\Skill.lua"
Status = "\\bot\\Reference\\Status.lua"
Town = "\\bot\\Reference\\Town.lua"

--~ Hammerdin = "\\Character\\Builds\\Hammerdin.lua"
--~ Light = "\\Character\\Builds\\Light.lua"
--~ Smiter = "\\Character\\Builds\\Smiter.lua"
--~ Trapsin = "\\Character\\Builds\\Trapsin.lua"

CustomFunctions= "\\bot\\CustomFunctions.lua"
AOSettings = "\\Character\\AOSettings.lua"
Character = "\\Character\\"..Ao.character..".lua"

Actions = {	Automate,Farm,MF,Rush,Shop}
Reference = {Container,Log,Movement,Skill,Status,Town}
--~ Builds = {Hammerdin,Light,Smiter,Trapsin}
Settings = {AOSettings,Character}

function LoadTable(Table)
	for index,value in pairs(Table) do
		local fn, e = _loadfile(value)
		if fn == nil then
			print("File: "..value.." error: "..e)
			Ao:Chat("File: "..value.." error: "..e)
			Log(4, "includes", "File: "..value.." error: "..e) -- needs work
		end
	end
end

function LoadAll()
	LoadTable(Actions)
	LoadTable(Reference)
	LoadTable(Settings)
--~ 	LoadTable(Builds)
	_loadfile("\\Character\\Builds\\"..Config.Character.Build..".lua")
	_loadfile("\\Pickit\\ItemFunctions.lua")
end

function _loadfile(File)
	for index,value in pairs(IncludedFiles) do
		Include = true
		if value == File then
			print("File: "..value.." is already loaded")
			Ao:Chat("File: "..value.." is already loaded")
			Log(4, "includes", "File: "..value.." is already loaded") -- needs work
			Include = false
		end
	end
	if Include then
		loadfile(File)
		table.concat(IncludedFiles, File) -- If post not given, appends it
	end
end