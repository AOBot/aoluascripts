AO = {}

AO.Command = function()

end

AO.Go = function()

end

AO.Sleep = function(ms)
	if ms == nil or ms < 0 then
		Ao:Sleep(100)
	else
		Ao:Sleep(ms)
	end
end

AO.PublicChat = function(Message)
	if message and Settings.General.PublicChat then
		Ao:SendMessage(message)
	end
end

-- Description : Checks if the log file exists
AO.SeperateText = function(String)
	return string.gsub(Text, '(%l)(%u)', function(a,b) return a.." "..b end) -- That's good enough, 1 line
end

-- not sure where else to put this, needs renamed
AO.GetEnumKey = function(enumType, enumValue)
	if type(enumValue) ~= "string" then
		local values = {}
		for k, v in pairs(enumType) do
			values[v] = k
		end
		return values[enumValue];
	else
		return enumValue
	end
end

-- not sure where else to put this, needs renamed
AO.GetEnumValue = function(enumType, enumValue)
	if type(enumValue) == "string" then
		for v, k in pairs(enumType) do
			if v == enumValue then
				return tonumber(k)
			end
		end
		return false
	else
		return enumValue
	end
end

Math = {}
Math.Round = function(Number, Index)
  return tonumber(string.format("%." .. (Index or 0) .. "f", Number))
end

--[[ Copyright (c) 2007 Tim Kelly/Dialectronics ]]--
--[[ 	EO(c)	]]--
Math.Dec2Hex = function(Value)
    local B,K,OUT,I,D=16,"0123456789ABCDEF","",0
    while val>0 do
        I=I+1
        val,D=math.floor(Value/B),math.mod(Value,B)+1
        OUT=string.sub(K,D,D)..OUT
    end
    return OUT
end

Math.ConvertTime = function(Time)
    local time = Time
    local h = round((Time/3600) - 0.5)
    time = time - (h * 3600)
    local m = round((time / 60) - 0.5)
    time = time - (m * 60)
    local s = time 

    if h >= 1 then
        return h.."hour(s) "..m.."min(s) "..s.."s"
    else
        return m.."min(s) "..s.."s"
    end
end