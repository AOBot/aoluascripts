MF = {}

-- Description : Executes Boss passed into it 
MF.Boss = function(Boss) -- ported from bosses
	local t = os.clock()
	Log(2, "Awesom-O", "MF ("..Boss..") : Start", true)
	if Boss == "Baal" then
		MF.Baal()
	elseif Boss == "Diablo" then
		MF.Diablo()
	elseif Boss == "CowKing" then
		MF.Cows()
	elseif Boss == "Travincal" then
		MF.Travincal()
	elseif Boss == "NihlathakBoss" or Boss == "TheCountess" or Boss == "Summoner" then
		MF.Keys(Boss)
	else
		-- Move to boss area
		if Move.ToArea(MF.Areas[Boss]) then
			-- Move to boss
			local Npc = Move.ToNpc(Boss)
			-- Kill boss
			if Boss == "Bonebreaker" or Boss == "BattlemaidSarina" then
				Kill.AllInRadius(Ao.me.x, Ao.me.y, 20)
			else
				if Npc.uid ~= 0 then
					Kill.NPC(Npc)
				else
					Log(3, "Bosses", Boss.." : Not Found")
				end
			end
		end
	end
	Log(2, "Bosses", Boss.." : End", true)
	Log(2, "BossTime", Boss.." : "..os.clock() - t.." seconds")
end

-- Description : Main function for doing baal
local center, wave = {x=7792, y=5292, rad=60}, 1
MF.Baal = function() -- ported from bosses
	local t = os.clock()
	Log(2, "Awesom-O", "MF (Baal) : Start", true)
	AO.PublicChat(Build.Baal.StartMsg)
	-- Move to Throne
	if not Move.ToArea("ThroneOfDestruction") then
		return false
	end
	-- Cast Tp
	if Settings.General.Public then
		Skill.Select("BookOfTownportal") -- not sure how hand will work assuming string is safer
		Skill.CastAt(Settings.Baal.TPLoc[1], Settings.Baal.TPLoc[2])
		Log(1, "Bosses", "Making TP for leechers")
		AO.PublicChat(Build.Baal.TPClearingMsg)
	end
	-- Clear
	local npc = Ao.npcs:findInRadius(center.x, center.y, center.rad)
	while npc.uid ~= 0 do
		if IsWaveMob(npc) > 0 then 
			AO.PublicChat(Build.Baal.TPSafeMsg) return 
		end
		Kill.NPC(npc)
		npc = Ao.npcs:findInRadius(center.x, center.y, center.rad)
	end
	
	-- Kill Waves
	while Kill.Waves() do end
	-- Kill Baal
	if Build.Baal.Kill then Kill.Baal() end
	Log(2, "Bosses", "Baal : End", true)
	Log(2, "BossTime", "Baal : "..os.clock() - t.." seconds")
	t = nil -- why is this needed if its local?
end

-- Description : Moves to location and cast in preperation for baal wave
MF.PreWave = function() -- ported from bosses
	-- Move to location to cast on wave
	if Build.Baal.PrecastLocationX[wave] ~= nil and Build.Baal.PrecastLocationY[wave] ~= nil then
		while (Ao.me.x ~= Build.Baal.PrecastLocationX[wave] and Ao.me.y ~= Build.Baal.PrecastLocationY[wave]) do 
			Move.ToLocation(Build.Baal.PrecastLocationX[wave], Build.Baal.PrecastLocationY[wave]) 
		end -- this should use each wave spot
	else
		while (Ao.me.x ~= center.x and Ao.me.y ~= center.y) do Move.ToLocation(center.x, center.y) end
	end
	-- Pre Wave Skill Functionality
	local npc 
	npc = Ao.npcs:findInRadius(center.x, center.y, center.rad)
	while (npc.uid == 0 and MF.CheckBaal()) do
		if Build.Baal.WaveSkill[wave] ~= nil then
			Skill.Select(Build.Baal.WaveSkill[wave])
		end
		Kill.Preattack(15092, 5028)
		npc = Ao.npcs:findInRadius(center.x, center.y, center.rad)
	end
	-- check if npc is part of wave
	if MF.IsWaveMob(npc) == 0 then
		MF.ClearThrone()
	else
		wave = MF.IsWaveMob(npc)
	end
end

-- Description : Returns which wave it is based on what NPC are found
MF.IsWaveMob = function(npc) -- ported from bosses
	if npc.uid ~= 0 then
		return MF.WaveMob[npc.id]
	else 
		return 0 
	end
end

-- Description : Checks if baal has moved into throne
MF.CheckBaal = function()
	if distance(Ao.me.x, Ao.me.y, center.x, center.y) >= (center.rad - 5) then 
		Move.ToLocation(center.x, center.y) 
	end
	local IsBaal = Ao.npcs:findByNpcCode(NPCCode.BaalThrone)
	if IsBaal.uid <= 0 then 
		return false
	else 
		return true
	end
end

-- Description : Cecks if Wave has spawned while not explores and clears throne
MF.ClearThrone = function() -- ported from bosses
	Move.ToLocation(center.x, center.y)
	Log(1, "Bosses", "Clearing Throne")
	if Settings.General.Public then
		local t = os.clock()
		local total = Ao.npcs:countInRadius(center.x, center.y, center.rad)
		local tp = Ao.npcs:countInRadius(Build.Baal.TPLoc[1], Build.Baal.TPLoc[2], center.rad/3)
		if total > 0 then
			PublicChat(string.format(Config.Messages.MobsMsg, tp, total))
		else
			PublicChat(Config.Messages.ColdTP)
		end
	end
	local npc = Ao.npcs:findInRadius(center.x, center.y, center.rad)
	while npc.uid ~= 0 do
		if MF.IsWaveMob(npc) > 0 then 
			PublicChat(Config.Messages.ColdTP)
			return 
		end
		Kill.NPC(npc)
		npc = Ao.npcs:findInRadius(center.x, center.y, center.rad)
	end
	PublicChat(Config.Messages.ColdTP)
	if Settings.General.Public then
		Move.ToLocation(Build.Baal.TPLoc[1], Build.Baal.TPLoc[2])
		Skill.PreCasts(true)
	end
end

-- Diablo functions
MF.Diablo = function(Clear)
	PublicChat(Build.Diablo.StartMsg)
	if Settings.General.Public then
		Move.ToLocation(Build.Diablo.TPLoc[1], Build.Diablo.TPLoc[2])
		CastTp()
		Sleep(100)
		PreCast(true)
		local total = Ao.npcs:countInRadius(center.x, center.y, center.rad)
		local tp = Ao.npcs:countInRadius(Build.Diablo.TPLoc[1], Build.Diablo.TPLoc[2], center.rad/3)
		PublicChat(string.format(Config.Messages.MobsMsg, tp, total))
		Kill.AllInRadius(Ao.me.x, Ao.me.y, 60)
	end
	-- Clear Area
	if Clear then
		Move.Explore(true)
	end
	-- Pop Seals
	MF.PopSeals
	-- Kill Diablo
	PublicChat(Build.Diablo.KillMsg)
	Move.ToLocation(7792, 5292)
	PreCast(true)
	local Diablo = Ao.npcs:findByNpcCode(NPCCode.Diablo)
	repeat
		Kill.Preattack(Ao.me.x, Ao.me.y)
		Diablo = Ao.npcs:findByNpcCode(NPCCode.Diablo)
	until Diablo.uid > 0
	Kill.NPC(Diablo)
end

MF.PopSeals = function()
	for i=1, 5 do
		-- Set some values
		local lock = Ao.objects:find(MF.Seals[i].Seal)
		local npc = Ao.npcs:findBySuperUnique(MF.Seals[i].Boss)
		local location = MF.Seals[i].x,MF.Seals[i].y
		-- Move to and Pop Seal
		Move.ToObject(MF.Seals[i].Seal, true)
		if lock.objectMode == 0 then
			repeat
				Ao:UnitInteract(UnitType.GameObject, lock.uid)
				lock = Ao.objects:find(lock.objectID)
				if lock.objectMode == 0 then
					Move.ToObject(MF.Seals[i].Seal, true)
				else
					break
				end
			until lock.objectMode > 0
		end
		-- Move to Seal Boss
		Move.ToLocation(location.x, location.y, true)
		-- Kill Boss
		repeat
			npc = Ao.npcs:findBySuperUnique(MF.Seals[i].Boss) -- why are we redecalring variable?
			-- If can't find boss then find random npc and kill it
			if not Kill.Preattack(Ao.me.x, Ao.me.y) or npc.uid == 0 then
				local rand = Ao.npcs:findInRadius(Ao.me.x, Ao.me.y, 10)
				if rando.uid > 0 then
					Kill.NPC(rand)
				end
			end
			npc = Ao.npcs:findBySuperUnique(MF.Seals[seal].Boss) -- why are we redecalring variable thrid time?
		until npc.uid > 0
		-- Kill boss ? why again ?
		Kill.NPC(npc)
		-- Clear whats left around seal
		Kill.AllInRadius(Ao.me.x, Ao.me.y, 10)
		i = i + 1
	end
end

-- Travincal functions
MF.Travincal = functions()
	if not Move.ToArea("Travincal", true) then
		return false
	end
	for i=1, 3 do
		local boss = Move.ToNpc(MF.Council[i], true)
		if boss.uid > 0 then
			Kill.NPC(boss)
		end
	end
end

-- Description : Functions similar to Boss.MF but only accepts Uber Bosses 
-- #NOTE : not sure if needs to be seperate function though possibly could be combined with Boss function down the road
MF.Ubers = function(Boss, FarmType)
	local t = os.clock()
	Log(2, "Farming", FarmType.." Farming : Killing "..AO.SeperateText(Boss), true)
	Skill.PreCast()
	local Npc = Move.ToNpc(Boss)
	if Npc.uid ~= 0 then
		Kill.NPC(Npc)
	end
	Log(2, "Bosses", AO.SeperateText(Boss).." : End", true)
	Log(2, "BossTime", AO.SeperateText(Boss).." : "..os.clock() - t.." seconds")
end

-- Description : Functions similar to Boss.MF but only accepts Key run Bosses 
-- #NOTE : not sure if needs to be seperate function though possibly could be combined with Boss function down the road
MF.Keys = function(Boss)
	local t = os.clock()
	Log(2, "Awesom-O", "Key Farming ("..Boss..") : Start", true)
	-- Move To Boss Area
	if Move.ToArea(MF.Area[Boss]) then
		-- Find Boss
		if Boss == "NihlathakBoss" then
			Move.ToObject("NihlathakWildernessStartPosition")
		elseif Boss == "TheCountess" then
			Move.ToObject("CountessChest")
		end
		local Npc = Move.ToNpc(Boss)
	end
	-- Kill Boss
	if Npc.uid ~= 0 then
		-- Countess is a bitch to find for the bot so we kill everything in the area
		if Boss == "TheCountess" then
			Kill.AllInRadius(Ao.me.x, Ao.me.y, 20)
		else
			Kill.NPC(Npc)
		end
		Log(2, "Bosses", Boss.." : End", true)
		Log(2, "BossTime", Boss.." : "..os.clock() - t.." seconds", true)
	else
		-- Since Countess is not always next to the chest we clear the whole lvl if she is not.
		--[[ possible countess coords method
		Move.ToLocation(12542, 11102)
		Move.ToLocation(12542, 11031)
		Move.ToLocation(12600, 11035)
		Move.ToLocation(12530, 11035)
		]]--
		if Boss == "TheCountess" then
			Log(2, "Boss", "Countess wasnt found. Attempting to clear entire area!")
			local KillAll = Config.Pathing.KillAll
			Config.Pathing.KillAll = true
			Move.Explore(true)
			Config.Pathing.KillAll = KillAll
		else
			Log(3, "Boss", Boss.." : Not found")
		end
		Log(2, "Bosses", Boss.." : End", true)
		Log(2, "BossTime", Boss.." : "..os.clock() - t.." seconds", true)
	end
end


-- Description: Functions similar to Boss.MF but used for clearing area
MF.Areas = function(Area)
	local t = os.clock()
	Log(2, "Awesom-O", "MF ("..Area..") : Start", true)
	local KillAll = Config.Path.KillAll
	local KillHeroes = Config.Pathing.KillHeroes
	if Area == "ChaosSanctuary" then
		Move.ToArea("ChaosSanctuary")
		Config.Pathing.KillAll = true
		Config.Pathing.KillHeroes = true
		Areas.Diablo(true)
	elseif Area == "PitLevel2" then
		Move.ToArea("PitLevel1")
		Move.ToLocation(7557,11092)
		Move.ToArea("PitLevel2")
		Config.Pathing.KillAll = true
		Config.Pathing.KillHeroes = true
		Move.Explore(true)
	else
		Move.ToArea(Area, true)
		Config.Pathing.KillAll = true
		Config.Pathing.KillHeroes = true
		Move.Explore(true)
	end
	Config.Pathing.KillAll = KillAll
	Config.Pathing.KillHeroes = KillHeroes
	Log(2, "Bosses", Area.." : End", true)
	Log(2, "BossTime", Area.." Cleared : "..os.clock() - t.." seconds")
end

-- Cows functions
MF.Cows = function()

	local WirtCorpse, WirtsLeg, TomeOfPortals
	local Akara = NPCCode.Akara

	WirtsLeg = Inventory.FindItem("WirtsLeg")

	if (not WirtsLeg) then 
		-- Get Leg
		if (not Move.ToArea("Tristram")) then
			return false
		end

		if (not MoveToObjectById(GameObjectID.WirtCorpse)) then
			return false
		end

		WirtCorpse = Ao.objects:find(GameObjectID.WirtCorpse) -- probably needs work

		while (WirtCorpse.objectMode < 1) do
			Ao:UnitInteract(UnitType.GameObject, WirtCorpse.uid);
			Ao:Sleep(100)
			WirtCorpse = Ao.objects:find(GameObjectID.WirtCorpse)
		end

		repeat
			WirtsLeg = Ao.items:findByCode("WirtsLeg")
			Ao:Sleep(100)
		until (WirtsLeg ~= 0)

		if (WirtsLeg ~= 0) then
			Ao:PickItem(4, WirtsLeg.uid, false)
		else
			Log(2, "Awesom-o", "WirtCorpse's : Not Found")
			return false
		end

		Move.ToTown () --Is this right?

	end -- go for WirtsLeg end

	TomeOfPortals = Inventory.FindItem("TomeOfPortals")
	if (not TomeOfPortals) then
		NPC.BuyItem("TomeOfPortals","Akara")
		TomeOfPortals = Inventory.FindItem("TomeOfPortals")
	end

	while (TomeOfPortals:size() > 4) do --We can fill our main book and then buy new one
		TomeOfPortals = NPC.BuyItem("TomeOfPortals","Akara") --Returns info about newly bought book
	end

	Item.ToCube(TomeOfPortals)
	Item.ToCube(WirtsLeg)
	Cube.Transmute()

	Move.ViaWarp(Area)
	
	-- added variable to kill king but not sure on implementation yet. Needs testing
	if Build.Cows.KillKing == false then
		while Move.Explore(true) do
			local King = Ao.npcs:findByNpcCode(NPCCode.TheCowKing)
			if King then
				Config.Pathing.KillAll = false
				Config.Pathing.KillHeroes = false
				Teleport.ToLocation(King.x+100,King.y+100)
				Config.Pathing.KillAll = true
				Config.Pathing.KillHeroes = true
			end
		end
	elseif Build.Cows.KillKing == true then
		Move.Explore(true)
	end
end

--[[  Tables  ]]--

MF.Council = {
	[1] = "IsmailVilehand",
	[2] = "GelebFlamefinger",
	[3] = "ToorcIcefist"
}

MF.Seals = {
	[1] = {
			Seal = GameObjectID.DiabloSeal3,
			Boss = SuperUnique.LordDeSeis,
			x = 7770,
			y = 5200
	},
	[2] = {
			Seal = GameObjectID.DiabloSeal5,
			Boss = SuperUnique.GrandVizierOfChaos,
			x = 7676,
			y = 5310
	},
	[3] = {
		   	Seal = GameObjectID.DiabloSeal4,
			Boss = false,
		   	x = 7770,
		   	y = 5200
	},
	[4] = {
			Seal = GameObjectID.DiabloSeal1,
			Boss = SuperUnique.InfectorOfSouls,
			x = 7920,
			y = 5290
	},
	[5] = {
			Seal = GameObjectID.DiabloSeal2,
			Boss = false,
			x = 7770,
			y = 5200
	}	
}

MF.WaveMob = {
	[23] = 1,
	[62] = 1,
	[105] = 2,
	[381] = 2,
	[557] = 3,
	[558] = 4,
	[571] = 5,
	[572] = 5,
	[573] = 5
}

MF.Areas = { -- from Globals.lua
	-- Act 1 --
	['Corpsefire'] = 'DenOfEvil', -- Good
	['Bishibosh'] = 'ColdPlains', -- Good
	['Bonebreaker'] = 'Crypt', -- wasnt found
	['BloodRaven'] = 'BurialGrounds', -- Good
	['Coldcrow'] = 'CaveLevel1', -- Good
	['Rakanishu'] = 'StonyField', -- Good
	['TreeheadWoodfist'] = 'DarkWood', -- Good
	['Griswold'] = 'Tristram', -- Good
	['TheCountess'] = 'TowerCellarLevel5', --wasnt found
	['PitspawnFouldog'] = 'JailLevel2', -- Good
	['BoneAsh'] = 'Cathedral', --Failed to Find (cant Path To Cathedral)
	['TheSmith'] = 'Barracks', -- Good
	['Andariel'] = 'CatacombsLevel4', -- Good
	-- Act 2 --
	['Radament'] = 'SewersLevel3Act2', -- Good
	['CreepingFeature'] = 'StonyTombLevel2', -- Good
	['BloodWitchTheWild'] = 'HallsOfTheDeadLevel3', -- Good
	['Beetleburst'] = 'FarOasis', -- Good
	['ColdwormTheBurrower'] = 'MaggotLairLevel3', -- Good
	['DarkElder'] = 'LostCity', -- Good
	['Fangskin'] = 'ClawViperTempleLevel2', -- Good
	['FireEye'] = 'PalaceCellarLevel3', -- Good
	['Summoner'] = 'ArcaneSanctuary', -- Good
	--['AncientKaatheSoulless'] = 'TalRashasTomb', --Need to figure out right tomb
	['Duriel'] = 'DurielsLair', -- Good
	-- Act 3 --
	['SszarkTheBurning'] = 'SpiderCavern', -- Failed To Get To Spider Cavern
	['WitchDoctorEndugu'] = 'FlayerDungeonLevel3', -- Good
	['Stormtree'] = 'FlayerJungle', -- Good
	['BattlemaidSarina'] = 'RuinedTemple', --wasnt found
	['IcehawkRiftwing'] = 'SewersLevel1Act3', -- Good
	['IsmailVilehand'] = 'Travincal', -- Good
	['GelebFlamefinger'] = 'Travincal', -- Good
	['ToorcIcefist'] = 'Travincal', -- Good
	['BremmSparkfist'] = 'DuranceOfHateLevel3', -- Good
	['WyandVoidbringer'] = 'DuranceOfHateLevel3', -- Good
	['MafferDragonhand'] = 'DuranceOfHateLevel3', -- Good
	['Mephisto'] = 'DuranceOfHateLevel3', -- Good
	-- Act 4 --
	['Izual'] = 'PlainsOfDespair', -- Good
	['HephastoTheArmorer'] = 'RiverOfFlame', -- Good
	['GrandVizierOfChaos'] = 'ChaosSanctuary', --Need Function
	['LordDeSeis'] = 'ChaosSanctuary', --Need Function
	['InfectorOfSouls'] = 'ChaosSanctuary', --Need Function
	['Diablo'] = 'ChaosSanctuary', --Need Function
	-- Act 5 --
	['DacFarren'] = 'BloodyFoothills', -- Good
	['ShenkTheOverseer'] = 'BloodyFoothills', -- Good
	['EldritchTheRectifier'] = 'FrigidHighlands', -- Good
	['EyebackTheUnleashed'] = 'FrigidHighlands', -- Good
	['SharptoothSlayer'] = 'FrigidHighlands', -- Good
	['ThreshSocket'] = 'ArreatPlateau', -- Good
	['Frozenstein'] = 'FrozenRiver', -- Good
	['BonesawBreaker'] = 'GlacialTrail', -- Good
	['SnapchipShatter'] = 'IcyCellar', -- Good
	['Pindleskin'] = 'NihlathaksTemple', -- Good
	['NihlathakBoss'] = 'HallsOfVaught', -- Good
	['Talic'] = 'ArreatSummit', --Need Function
	['Madawc'] = 'ArreatSummit', --Need Function
	['Korlic'] = 'ArreatSummit', --Need Function
	['ColenzotheAnnihilator'] = 'ThroneOfDestruction', --Need Function
	['AchmeltheCursed'] = 'ThroneOfDestruction', --Need Function
	['BartuctheBloody'] = 'ThroneOfDestruction', --Need Function
	['VentartheUnholy'] = 'ThroneofDestruction', --Need Function
	['ListertheTormentor'] = 'ThroneofDestruction', --Need Function
	['Baal'] = 'WorldstoneChamber', --Need Function
	-- Other --
	['TheCowKing'] = 'TheMooMooFarm', -- Test
	['UberDuriel'] = 'ForgottenSands', -- Test
	['UberIzual'] = 'FurnaceOfPain', -- Test
	['Lilith'] = 'MatronsDen' -- Test
}