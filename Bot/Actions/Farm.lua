-- Farm Check (which Farm to perform "Keys", "Organs", "Torches", "Essences", "Supplies")
if Config.Bot.Action == "Farm" then
	local t = os.clock()
	for i = 1, table.getn(Config.Bot.Farm) do
		if Config.Bot.Farm[i] == "Keys" then
			Farm.Keys()
		elseif Config.Bot.Farm[i] == "Organs" then
			Farm.Organs()
		elseif Config.Bot.Farm[i] == "Torches" then
			Farm.Torches()
		elseif Config.Bot.Farm[i] == "Essences" then
			Farm.Essences()
		elseif Config.Bot.Farm[i] == "Supplies" then
			Farm.Supplies()
		end
		Log(2, "BossTime", Config.Bot.Farm[i].." Farming : "..os.clock() - t.." seconds", true)
	end
end

Farm = {}

-- Description : Farms keys from the three bosses evenly, skipping a boss if you have a surplus of keys from them for that run
Farm.Keys = function()
	local t = os.clock()
	Log(2, "Awesom-O", "Key Farming : Start", true)
	-- Countess function
	if Farm.Keys.Terrors:size() <= Farm.Keys.Hates:size() and Farm.Keys.Terrors:size() <= Farm.Keys.Destructions:size() then
		Boss.Keys("Countess")
		Farm.UpdateItems(Farm.Key.Terror, "Keys")
	else
		Log(2, "Farming", "Key Farming : Too Many Key of Terror : Countess Skipped!")
	end
	-- Summoner function
	if Farm.Keys.Hates:size() <= Farm.Keys.Terrors:size() and Farm.Keys.Hates:size() <= Farm.Keys.Destructions:size() then
		Boss.Keys("Summoner")
		Farm.UpdateItems(Farm.Key.Hate, "Keys")
	else
		Log(2, "Farming", "Key Farming : Too Many Key of Hate : Summoner Skipped!")
	end
	Farm.UpdateTables()
	-- Nihlathak function
	if Farm.Keys.Destructions:size() <= Farm.Keys.Hates:size() and Farm.Keys.Destructions:size() <= Farm.Keys.Terrors:size() then
		Boss.Keys("Nihlathak")
		Farm.UpdateItems(Farm.Key.Destruction, "Keys")
	else
		Log(2, "Farming", "Key Farming : Too Many Key of Destruction : Nihlathak Skipped!")
	end
	Log(2, "Awesom-O", "Key Farming : End", true)
end

-- Description : Farms Organs from the three bosses, aquiring enough keys first if need be
Farm.Organs = function() -- from organhunt.lua
	local t = os.clock()
	Log(2, "Awesom-O", "Organ Farming : Start", true)
	-- Loop Control Variables
	local UberDuriel = false
	local UberIzual = false
	local Lilith = false
	-- Goto Act 5
	Move.ToAct(5) -- needs written, not sure weather to pass in number or string?
	-- make sure have enough keys to make thre portals
	if Farm.Keys.Terrors:size() < 3 or Farm.Keys.Hates:size() < 3 and Farm.Keys.Destructions:size() < 3 then
		Log(2, "Awesom-O", "Organ Farming : End (Not enough keys)", true)
		Farm.Keys()
		return
	end
	-- Loop through and Kill Ubers
	repeat
		-- Create Portals
		Cube.Keys()
		if Farm.Portal.ForgottenSands.x ~= 0  and not UberDuriel then
			Move.ViaPortal('ForgottenSands') -- temporary until im able to test if MoveTo will take portal
			Boss.Ubers("UberDuriel", "Organ")
			UberDuriel = true
		elseif Farm.Portal.FurnaceOfPain.x ~= 0 and not UberIzual then
			Move.ViaPortal('FurnaceOfPain') -- temporary until im able to test if MoveTo will take portal
			Boss.Ubers("UberIzual", "Organ")
			UberIzual = true
		elseif Farm.Portal.MatronsDen.x ~= 0 and not Lilith then
			Move.ViaPortal('MatronsDen') -- temporary until im able to test if MoveTo will take portal
			Boss.Ubers("Lilith", "Organ")
			Lilith = true
		end
	until (UberDuriel and UberIzual and Lilith)
	Log(2, "Awesom-O", "Organ Farming : End", true)
end

-- Description : Farms for torch, aquiring enough organs first if need be and keys becuase organs does key check so adding torches will go from keys to torches.
Farm.Torches = function() -- from torchhunt.lua
	local t = os.clock()
	Log(2, "Awesom-O", "Torch Farming : Start", true)
	-- Goto Act 5
	Move.ToAct(5) -- needs written, not sure weather to pass in number or string?
	-- Check if a portal already exsists for leach bots
	if Farm.Portal.UberTristram.x ~= 0 then
		Kill.Ubers()
		return
	end
	-- Check if have enough organs
	if Organ.Horn:size() < 1 or Organ.Eye:size() < 1 or Organ.Brain:size() < 1 then
		Log(3, "Awesom-O", "Torch Farming : End (Not enough Organs)")
		Farm.Organs()
		return
	else
		Cube.Organs()
		Kill.Ubers()
	end
	Log(2, "Awesom-O", "Torch Farming : End", true)
end

-- Description : Farms Essences from the four bosses evenly, skipping a boss if you have a surplus for that run
Farm.Essences = function()
	local t = os.clock()
	Log(2, "Awesom-O", "Essence Farming : Start", true)
	-- Andariel function
	if Farm.Essences.Sufferings:size() <= Farm.Essences.Hatreds:size() and Farm.Essences.Sufferings:size() <= Farm.Essences.Terrors:size() and Farm.Essences.Sufferings:size() <= Farm.Essences.Destructions:size() then
		Boss.MF("Andariel")
		Farm.UpdateItems(Farm.Essence.Suffering, "Essence")
	else
		Log(2, "Farming", "Essence Farming : Too Many Suffering Essence : Andariel Skipped!")
	end
	-- Mephisto function
	if Farm.Essences.Hatreds:size() <= Farm.Essences.Sufferings:size() and Farm.Essences.Hatreds:size() <= Farm.Essences.Terrors:size() and Farm.Essences.Hatreds:size() <= Farm.Essences.Destructions:size() then
		Boss.MF("Mephisto")
		Farm.UpdateItems(Farm.Essence.Hatred, "Essence")
	else
		Log(2, "Farming", "Essence Farming : Too Many Hatred Essence : Mephisto Skipped!")
	end
	-- Diablo function
	if Farm.Essences.Terrors:size() <= Farm.Essences.Hatreds:size() and Farm.Essences.Terrors:size() <= Farm.Essences.Sufferings:size() and Farm.Essences.Terrors:size() <= Farm.Essences.Destructions:size() then
		Boss.MF("Diablo")
		Farm.UpdateItems(Farm.Essence.Terror, "Essence")
	else
		Log(2, "Farming", "Essence Farming : Too Many Terror Essence : Diablo Skipped!")
	end
	-- Baal function
	if Farm.Essences.Destructions:size() <= Farm.Essences.Hatreds:size() and Farm.Essences.Destructions:size() <= Farm.Essences.Terrors:size() and Farm.Essences.Destructions:size() <= Farm.Essences.Sufferings:size() then
		Boss.Baal()
		Farm.UpdateItems(Farm.Essence.Destruction, "Essence")
	else
		Log(2, "Farming", "Essence Farming : Too Many Destruction Essence : Baal Skipped!")
	end
	Log(2, "Awesom-O", "Essence Farming : End", true)
end

Farm.Supplies = function() -- need list of supplies to run for as I dont use those char types

end

-- Description : If you found the item you were farming for then it stashes it, also will update the count
Farm.UpdateItems = function (Item, Type)
	if Item ~= nil then
		if Inventory.FindItem(Item) then
			Item.ToStash(Item)
		end
	end
	if Type == "Keys" then
		Farm.Keys.Terrors = Ao.me.stash:findByCode(Farm.Key.Terror)
		Farm.Keys.Hates = Ao.me.stash:findByCode(Farm.Key.Hates)
		Farm.Keys.Destructions = Ao.me.stash:findByCode(Farm.Key.Destruction)
	elseif Type = "Organs" then
		Farm.Organs.Barins = Ao.me.stash:findByCode(Farm.Organ.Brain)
		Farm.Organs.Horns = Ao.me.stash:findByCode(Farm.Organ.Horn)
		Farm.Organs.Eyes = Ao.me.stash:findByCode(Farm.Organ.Eye)
	elseif Type = "Essence" then
		Farm.Essences.Sufferings = Ao.me.stash:findByCode(Farm.Essence.Suffering)
		Farm.Essences.Hatreds = Ao.me.stash:findByCode(Farm.Essence.Hatred)
		Farm.Essences.Terrors = Ao.me.stash:findByCode(Farm.Essence.Terror)
		Farm.Essences.Destructions = Ao.me.stash:findByCode(Farm.Essence.Destruction)
	end
end


--[[  Tables  ]]  --

Farm.Key = {
	Terror = "pk1",
	Hate = "pk2",
	Destruction = "pk3"
}

Farm.Keys = {
	Terrors = Ao.me.stash:findByCode(Farm.Key.Terror),
	Hates = Ao.me.stash:findByCode(Farm.Key.Hate),
	Destructions = Ao.me.stash:findByCode(Farm.Key.Destruction)
}

Farm.Organ = {
	Brain = "mbr",
	Horn = "dhn",
	Eye = "bey"
}

Farm.Organs = {
	Barins = Ao.me.stash:findByCode(Farm.Organ.Brain),
	Horns = Ao.me.stash:findByCode(Farm.Organ.Horn),
	Eyes = Ao.me.stash:findByCode(Farm.Organ.Eye)
}

Farm.Portal = {
	ForgottenSands = AreaLevel.ForgottenSands, -- not sure how this works?
	FurnaceOfPain = AreaLevel.FurnaceOfPain,
	MatronsDen = AreaLevel.MatronsDen,
	UberTristram = AreaLevel.UberTristram
}

Farm.Essence = {
	Suffering = "tes", -- Andy
	Hatred = "ceh", -- Meph
	Terror = "bet", -- Diablo
	Destruction = "fed" -- Baal
}

Farm.Essences = {
	Sufferings = Ao.me.stash:findByCode(Farm.Essence.Suffering),
	Hatreds = Ao.me.stash:findByCode(Farm.Essence.Hatred),
	Terrors = Ao.me.stash:findByCode(Farm.Essence.Terror),
	Destructions = Ao.me.stash:findByCode(Farm.Essence.Destruction)
}
