function joinTables(t1, t2)
	for k,v in ipairs(t2) do table.insert(t1, v) end return t1
end


Rush = {}

Rush.Start = function(Act = 1) -- from Rush.lua
	RushList = {}
	for i = Act, 5 do
		RushList = joinTables(RushList,Rush.Act[i])
		if Settings.Rush.SideQuests then RushList = joinTables(RushList,Rush.Act[i]) end
	end
	Rush.DoQuest(RushList)
end

Rush.DoQuest = function (Quest)
	if type(Quest) == "string" then Quest = {Quest} end
	for Key,value in pairs(Quest) do
		QuestInfo = Rush.Quest[value]
		for i = 0, table.getn(QuestInfo["Area"]) do -- needs ideas
			if not Move.ToArea(QuestInfo["Area"][i])
		end
	end
end

Rush.Act = {
	[1] = {"Andariel"}
	[2] = {"Cube","Staff","Amulet","Summoner","Duriel"}
	[3] = {"Travincal","Mephisto"}
	[4] = {"Diablo"}
	[5] = {"Ancients","Baal"}
}

Rush.SideQuests = {
	[1] = {"Den","Cain","Tools"}
	[2] = {"Radament"}
	[3] = {"Bird","StatBook"}
	[4] = {"Izual","HellForge"}
	[5] = {"Shenk","SaveAnya"}
}

Rush.JerhynTP = function() -- from Rush.lua

end

Rush.WaitForPlayer = function(radius) -- from Rush.lua


end
Rush.KeepClear = function() -- from Rush.lua

end

Rush.Quest = {["Den"] = {["Boss"] = {}, ["Area"] = {"DenOfEvil"},["Object"] = {},["NPC"] = {},["Msg"] = {"Kill 1 Monster And Wait For Quest"}, ["Action"] = {"Clear"}}}

--Let's keep only Destination in ["Area"], and other areas only to force path OR multiple actions
--In Tools quest we dont need "OuterCloister", "Barracks" since movement.lua sust be smart enough.
--Must be quoted

--Splitted quests into parts as KOrr suggested.
Boss Area Object Msg Action
Rush.Quest = {
    ["Den"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"DenOfEvil"},
			["Object"] = {},
			["Msg"] = {"Kill 1 Monster And Wait For Quest"},
			["Action"] = {"Clear"},
			}
    }
    ["Cain"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"DarkWood"},
			["Object"] = {"InifussTree"},
			["Msg"] = {"Get scroll and talk to Akara"},
			["Action"] = {},
			}
		[2] = {
			["Boss"] = {},
			["Area"] = {"StonyField"},
			["Object"] = {"CairnStoneAlpha"},
			["Msg"] = {"Open Tristram portal then return to town"},
			["Action"] = {},
			}
		[3] = {
			["Boss"] = {},
			["Area"] = {"Tristram"},
			["Object"] = {"CainGibbet"},
			["Msg"] = {"Get Cain"},
			["Action"] = {},
			}
    }
    ["Tools"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"OuterCloister", "Barracks"},
			["Object"] = {"Malus"},
			["Msg"] = {"Get Malus and Talk to Charsi"},
			["Action"] = {},
			}
    }
    ["Andariel"] = {
		[1] = {
			["Boss"] = {"Andariel"},
			["Area"] = {"CatacombsLevel4"},
			["Object"] = {},
			["Msg"] = {"Wait for Andariel death"},
			["Action"] = {"Kill"},
		}
    }
    ["Radament"] = {
		[1] = {
			["Boss"] = {"Radament"},
			["Area"] = {"SewersLevel3Act2"},
			["Object"] = {"HoradricScrollChest"},
			["Msg"] = {"Wait for radament death"},
			["Action"] = {"Kill"},
		}
    }
    ["Cube"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"HallsOfTheDeadLevel3"},
			["Object"] = {"HoradricCubeChest"},
			["Msg"] = {"Take cube"},
			["Action"] = {},
		}
    }
    ["Staff"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"MaggotLairLevel3"},
			["Object"] = {"StaffOfKingsChest"},
			["Msg"] = {"Take staff"},
			["Action"] = {},
		}
    }
    ["Amulet"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"ClawViperTempleLevel2"},
			["Object"] = {"TaintedSunAltar"},
			["Msg"] = {"Take amulet"},
			["Action"] = {},
		}
    }
    ["Summoner"] = {
		[1] = {
			["Boss"] = {"Summoner"},
			["Area"] = {"ArcaneSanctuary"},
			["Object"] = {"YetAnotherTome"},
			["Msg"] = {"Wait for Summoner death, dont press ESC on death"},
			["Action"] = {"kill"},
		}
    }
    ["Duriel"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"GetCorrectTomb"},
			["Object"] = {"HoradricOrifice"},
			["Msg"] = {"Use staff to make hole in wall"},
			["Action"] = {"DoorWaitOpen"},
		}
		[2] = {
			["Boss"] = {"Duriel"},
			["Area"] = {"DurielsLair"},
			["Object"] = {},
			["Msg"] = {"Use staff to make hole in wall"},
			["Action"] = {"Kill"},
		}
    }
    ["Bird"] = {
		[1] = {
			["Boss"] = {"Stormtree"},
			["Area"] = {"FlayerJungle"},
			["Object"] = {},
			["Msg"] = {"Come for bird"},
			["Action"] = {"Kill"},
		}
    }
    ["StatBook"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"RuinedTemple"},
			["Object"] = {"LamEsensTome"},
			["Msg"] = {"Take book"},
			["Action"] = {},
		}
    }
    ["Travincal"] = {
		[1] = {
			["Boss"] = {"IsmailVilehand","GelebFlamefinger","ToorcIcefist"},
			["Area"] = {"Travincal"},
			["Object"] = {"CompellingOrb"},
			["Msg"] = {"Wait for quest, Don't press ESC on death"},
			["Action"] = {"Kill"},
		}
    }
    ["Mephisto"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"DuranceOfHateLevel3"},
			["Object"] = {"bridge"}, --made this up
			["Msg"] = {"Waith for mephisto death"},
			["Action"] = {"ClearBehindBridge"}, --Needs handler - We need to make sure t's safe.
		}
		[2] = {
			["Boss"] = {"Mephisto"},
			["Area"] = {"DuranceOfHateLevel3"},
			["Object"] = {"bridge"}, --made this up
			["Msg"] = {"Wait for mephisto death"},
			["Action"] = {"Kill"},
		}
    }
    ["Izual"] = {
		[1] = {
			["Boss"] = {"Izual"},
			["Area"] = {"PlainsOfDespair"},
			["Object"] = {},
			["Msg"] = {"Come to TP and wait for Izual death"},
			["Action"] = {"Kill"},
		}
    }
	["HellForge"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"RiverOfFlame"},
			["Object"] = {"HellForge"},
			["Msg"] = {"Smash stone with hammer"},
			["Action"] = {}, -- dont need, default behavior would be clear area around tp which would also kill the boss
		}
    }
    ["Diablo"] = {
		[1] = {
			["Boss"] = {"Diablo"},
			["Area"] = {"ChaosSanctuary"},
			["Object"] = {},
			["Msg"] = {"Come and wait until dia dies"},
			["Action"] = {"KillDia"}, -- made this myself
		}
    }
    ["Shenk"] = {
		[1] = {
			["Boss"] = {"Shenk"},
			["Area"] = {"BloodyFoothills"},
			["Object"] = {},
			["Msg"] = {"Wait until Shenk dies"},
			["Action"] = {"Kill"},
		}
    }
    ["SaveAnya"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"FrozenRiver"},
			["Object"] = {"Anya"}, -- made this up
			["Msg"] = {"Save her"},
			["Action"] = {"WaitMelt"},
		}
    }
    ["Ancients"] = {
		[1] = {
			["Boss"] = {},
			["Area"] = {"ArreatSummit"}, -- We dont need "TheAncientsWay" as we can move from other side too  - TheWorldStoneKeepLevel2
			["Object"] = {},
			["Msg"] = {},
			["Action"] = {},
		}
		[2] = {
			["Boss"] = {"AncientBarbarian","AncientBarbarian2","AncientBarbarian3"},
			["Area"] = {"ArreatSummit"}, -- We dont need "TheAncientsWay" as we can move from other side too  - TheWorldStoneKeepLevel2
			["Object"] = {},
			["Msg"] = {},
			["Action"] = {"KillAncients"},
		}
    }
    ["Baal"] = {
		[1] = {
			["Boss"] = {"Baal"},
			["Area"] = {"ThroneOfDestruction","TheWorldstoneChamber"},
			["Object"] = {"BaalsPortal"},
			["Msg"] = {"Come to tp"},
			["Action"] = {"KillWaves","Kill"},
		}
    }
}
