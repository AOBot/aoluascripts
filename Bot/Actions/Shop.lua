Shop = {}

Shop.Start = function(shopnpc) -- from shopbot.lua

end

Shop.It = function(npcid, x, y) -- from shopbot.lua Needs renamed not sure what

end

Npc = {}

Npc.Interact = function(npcid)
	local npc = Ao.npcs:findByNpcCode(npcid)
	if npc.uid ~= 0 then
		Ao:InitWaitingPacket(Move.GS["NPCInfo"])
		Ao:UnitInteract(UnitType.NPC, npc.uid)
		if Ao:WaitForInitPacket(Settings.General.InteractDelay) then
			Ao:TownFolkInteract(UnitType.NPC, npc.uid)
			return true
		else
			return false
		end
	else
		return false
	end
end

Npc.StaticInteract = function(npcid)
	npcid = AO.GetValue(NPCCode, npcid)
	local npc = Ao.npcs:findByNpcCode(npcid)
	if npc.uid ~= 0 then
		Ao:InitWaitingPacket(Move.GS["NPCInfo"])
		Ao:UnitInteract(UnitType.NPC, npc.uid)
		if Ao:WaitForInitPacket(Settings.General.InteractDelay) then
			Ao:TownFolkInteract(UnitType.NPC, npc.uid)
			AO.Sleep(Settings.General.InteractDelay)
			return true
		else
			return false
		end
	else
		return false
	end
end

Npc.MenuSelect = function(menu, npcid)
	menu = AO.GetValue(TownFolkMenuItem, menu)
	npcid = AO.GetValue(NPCCode, npcid)	
	local npc = Ao.npcs:findByNpcCode(npcid)
	if npc.uid ~= 0 then
		if menu == TownFolkMenuItem.Gamble or menu == TownFolkMenuItem.Trade then
			Ao:InitWaitingPacket(Move.GS["WorldItemAction"])
			Ao:TownFolkMenuSelect(menu, npc.uid, 0)
			if Ao:WaitForInitPacket(Settings.General.InteractDelay) then
				return true
			else
				return false
			end
		else
			Ao:TownFolkMenuSelect(menu, npc.uid, 0)
			Sleep(Settings.General.InteractDelay)
			return true
		end
	else
		return false
	end
end

Npc.CloseMenu = function(npcid)
	npcid = getEnumValue(NPCCode, npcid)
    local npc = Ao.npcs:findByNpcCode(npcid)

	if npc.uid ~= 0 then
		Ao:TownFolkCancelInteraction(UnitType.NPC, npc.uid)
		return true
	else
		return false
	end
end

Npc.SellItem = function(npc, item)
	Ao:InitWaitingPacket(Move.GS["OwnedItemAction"])
	repeat
		Ao:PickItemFromContainer(item.uid)
	until Ao:WaitForInitPacket(Settings.Bot.InteractDelay)

	Ao:InitWaitingPacket(Move.GS["TransactionComplete"])
    repeat
        Ao:SellItem(npc.uid, item.uid, 0)
	until Ao:WaitForInitPacket(Settings.Bot.InteractDelay)
end

Npc.BuyItem = function(npc, item, stack)
	if not stack then
		stack = false
	end
	Ao:InitWaitingPacket(Move.GS["TransactionComplete"])
	repeat
		Ao:BuyItem(npc.uid, item.uid, 0, stack)
	until Ao:WaitForInitPacket(2*Settings.Bot.InteractDelay)
end

Npc.ScanItems = function(npcid) -- from actions.lua

end
