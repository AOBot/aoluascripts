-- expansion\white\runes.lua "moderate"

white.runes =
{
	["r33"] = {priority = 9}, -- Zod
	["r32"] = {priority = 9}, -- Cham
	["r31"] = {priority = 9}, -- Jah
	["r30"] = {priority = 9}, -- Ber
	["r29"] = {priority = 9}, -- Sur
	["r28"] = {priority = 9}, -- Lo
	["r27"] = {priority = 9}, -- Ohm
	["r26"] = {priority = 9}, -- Vex
	["r25"] = {priority = 8}, -- Gul
	["r24"] = {priority = 8}, -- Ist
	["r23"] = {priority = 8}, -- Mal
	["r22"] = {priority = 7}, -- Um
	["r21"] = {priority = 7}, -- Pul
	["r20"] = {priority = 6}, -- Lem
	["r19"] = {priority = 5}, -- Fal
	["r18"] = {priority = 4}, -- Ko
	--["r17"] = {priority = 2}, -- Lum
	--["r16"] = {priority = 2}, -- Io
	--["r15"] = {priority = 2}, -- Hel
	--["r14"] = {priority = 2}, -- Dol
	--["r13"] = {priority = 2}, -- Shael
	--["r12"] = {priority = 3}, -- Sol
	--["r11"] = {priority = 3}, -- Amn
	--["r10"] = {priority = 2}, -- Thul
	--["r09"] = {priority = 2}, -- Ort
	--["r08"] = {priority = 3}, -- Ral
	--["r07"] = {}, -- Tal
	--["r06"] = {}, -- Ith
	--["r05"] = {}, -- Eth
	--["r04"] = {}, -- Nef
	--["r03"] = {}, -- Tir
	--["r02"] = {}, -- Eld
	--["r01"] = {}, -- El
}