-- expansion\set\setItems.lua "moderate"

--[[ Notes:
	Duplicate set items are located at the
	bottom of this file.
	Ex: Tal Rasha's Adjudication (Amulet) is located
		at the bottom of the file.

	Added SetIndex in locals to ID
	and check the right item.
	Also added comments on stat range of the item.
	Ex: SetIndex == 68 -- Aldur's Rhythm (Jagged Star)
		--39-47 def is a comment about the range
		of defense for Aldur's Advance.
	If you decide to setup Pickit to check an items
	mods in Settings, make sure the item is indexed
	and uncommented.
--]]

----------------------------------------------------
--------- Expansion "Class Sets" Settings ----------
----------------------------------------------------

local goodAldurs =
{ priority = 4, identify = true,
	goodItem =
		( Unidentified
		    or SetIndex == 68 -- Aldur's Rhythm (Jagged Star)
				and Sockets >= 3  -- 2-3 sock
			or SetIndex == 67  -- Aldur's Deception (Shadow Plate)
				and TotalDefense >= 746 -- 746-857 def
				and SingleResist >= 45 -- 40-50 single resist
			or SetIndex == 69 -- Aldur's Advance (Battle Boots)
				and TotalDefense >= 39 -- 39-47 def
				and SingleResist >= 45 -- 40-50 single resist
            or SetIndex == 66 -- Aldur's Stony Gaze (Hunter's Guise)
				and TotalDefense >= 157 -- 157-171 def
				and SingleResist >= 45 -- 40-50 single resist
		)
}

local goodGriswolds =
{ priority = 4, identify = true,
	goodItem =
		( Unidentified
		    or SetIndex == 82  -- Griswold's Heart (Ornate Plate)
				and TotalDefense >= 930 -- 917-950 def
			or SetIndex == 81 -- Griswold's Valor (Corona)
				and MagicFind >= 25  -- 20-30 MF
				and TotalDefense >= 249 -- 249-290 def
			or SetIndex == 83 -- Griswold's Redemption (Caduceus)
				and MaxDamagePercent >= 200 -- 200-240 edam
				and Sockets >= 4 -- 3-4 sock
			or SetIndex == 84 -- Griswold's Honor (Vortex Shield)
		    	and TotalDefense >= 310 -- 290-333 def
		)
}

local goodIK =
{ priority = 4, identify = true,
	goodItem =
		( Unidentified
		    or SetIndex == 70 -- Immortal King's Will (Avenger Guard)
				and MagicFind >= 32 -- 25-40 MF
				and TotalDefense >= 160 -- 160-175 def
            or SetIndex == 75 -- Immortal King's Stone Crusher (Ogre Maul)
				and CrushingBlow >= 37 -- 35-40 crushing blow
			--------------------- IK (Sacred Armor) "all the same"
			or SetIndex == 72 -- IK (War Belt) "all the same"
			or SetIndex == 73 -- Immortal King's Forge (War Gauntlets)
				and TotalDefense >= 112 -- 108-118 def
			or SetIndex == 74 -- Immortal King's Pillar (War Boots)
				and TotalDefense >= 122 -- 118-128 def
		)
}

local goodMavinas =
{ priority = 4, identify = true,
	goodItem =
		( Unidentified
			or SetIndex == 90 -- M'avina's True Sight (Diadem)
				and TotalDefense >= 205 -- 200-210 def
			---------------------- M'avina's (Grand Matron Bow) "all the same"
			or SetIndex == 91 -- M'avina's Embrace (Kraken Shell)
				and MagicDamageReduction >= 9 -- 5-12 magic reduc
			or SetIndex == 92 -- M'avina's Icy Clutch (Battle Gauntlets)
				and TotalDefense >= 91 -- 84-97 def
			or SetIndex == 93 -- M'avina's Tenet (Sharkskin Belt)
				and TotalDefense >= 83 -- 81-86 def
		)
}

local goodNatalyas =
{ priority = 4, identify = true,
	goodItem =
		( Unidentified
		    or SetIndex == 62 -- Natalya's Totem (Grim Helm)
				and TotalDefense >= 195 -- 195-300 def
				and AllResist >= 15 -- 10-20 allres
				and Strength >= 15 -- 10-20str
				and Dexterity >= 25 -- 20-30dex
			---------------------- Natalya's (Scissors Suwayyah) "all the same"
			or SetIndex == 64 -- Natalya's Shadow (Loricated Mail)
				and TotalDefense >= 540 -- 540-721 def
			    and Sockets >= 3 -- 1-3 sock
			or SetIndex == 65 -- Natalya's Soul (Mesh Boots)
				and TotalDefense >= 112 -- 112-169 def
				and TotalResist >= 40 -- 30-50 cold + light res
		)
}

local goodTalRashas =
{ priority = 4, identify = true,
	goodItem =
		( Unidentified
		    or SetIndex == 78 -- Tal Rasha's Lidless Eye (Swirling Crystal)
				and (LightningMasterySkill + ColdMasterySkill + FireMasterySkill) >= 4
				--and LightningMasterySkill >= 2 -- 1-2 mastery
				--and ColdMasterySkill >= 2 -- 1-2 mastery
				--and FireMasterySkill >= 2 -- 1-2 mastery
			or SetIndex == 80 -- Tal Rasha's Horadric Crest (Death Mask)
				and TotalDefense >= 115 -- 99-131 def
			or SetIndex == 79 -- Tal Rasha's Guardianship (Lacquered Plate)
				and TotalDefense >= 900 -- 833-941 def
			or SetIndex == 76 -- Tal Rasha's Fine-Spun Cloth (Mesh Belt)
				and MagicFind >= 13 -- 10-15 MF
				and TotalDefense >= 35 -- 35-40 def
		)      
}

local goodTrangs =
{ priority = 4, identify = true,
	goodItem =
		( Unidentified
		    or SetIndex == 85 -- Trang-Oul's Guise (Bone Visage)
				and TotalDefense >= 220 -- 180-257 def
			---------------------- Trang-Oul's (Chaos Armor) "all the same"
			or SetIndex == 87 -- Trang-Oul's Wing (Cantor Trophy)
				and FireResist >= 42 -- 38-45 fire res
				and TotalDefense >= 175 -- 175-189 def
			or SetIndex == 89 -- Trang-Oul's Girth (Troll Belt)
				and MaxMana >= 37 -- 25-50 mana
				and TotalDefense >= 134 -- 134-166 def
			or SetIndex == 88 -- Trang-Oul's Claws (Heavy Bracers)
				and TotalDefense >= 70 -- 67-74 def
		)      
}
		
set.items =
{ checkStats = true,
--------------------------------------------------
--------------- Expansion ------------------------
--------------------------------------------------
--[[ Aldur's Watchtower
	["dr8"] = goodAldurs, -- Aldur's Stony Gaze (Hunter's Guise)
	["xtb"] = goodAldurs, -- Aldur's Advance (Battle Boots)
	["uul"] = goodAldurs, -- Aldur's Deception (Shadow Plate)
	["9mt"] = goodAldurs, -- Aldur's Rhythm (Jagged Star)
--]]
--------------------------------------------------
--[[ Bul-Kathos' Children
	["7gd"] = {priority = 4}, -- Bul-Kathos' Sacred Charge (Colossus Blade)
	["7wd"] = {priority = 4}, -- Bul-Kathos' Tribal Guardian (Mythical Sword)
--]]
--------------------------------------------------
--[[ Cow King's Leathers
	["xap"] = {}, -- Cow King's Horns (War Hat)
	["stu"] = {}, -- Cow King's Hide (Studded Leather)
	---------------- Cow King's Hooves (Heavy Boots) (Duplicate)
--]]
--------------------------------------------------
---[[ Griswold's Legacy
	--["xar"] = goodGriswolds, -- Griswold's Heart (Ornate Plate)
	["urn"] = {priority = 7}, -- Griswold's Valor (Corona)
	["7ws"] = {priority = 7}, -- Griswold's Redemption (Caduceus)
	["paf"] = {priority = 7}, -- Griswold's Honor (Vortex Shield)
--]]
--------------------------------------------------
--[[ Heaven's Brethren
	["xrs"] = {}, -- Haemosu's Adamant (Cuirass)
	["7ma"] = {}, -- Dangoon's Teaching (Reinforced Mace)
	["uts"] = {}, -- Taebaek's Glory (Ward)
	["uhm"] = {}, -- Ondal's Almighty (Spired Helm)
--]]
--------------------------------------------------
--[[ Hwanin's Majesty
	["xrn"] = {}, -- Hwanin's Splendor (Grand Crown)
	["9vo"] = {}, -- Hwanin's Justice (Bill)
	["xcl"] = {}, -- Hwanin's Refuge (Tigulated Mail)
	---------------- Hwanin's Blessing (Belt) (Duplicate)
--]]
--------------------------------------------------
---[[ Immortal King
	--["ba5"] = goodIK, -- Immortal King's Will (Avenger Guard)
	--["7m7"] = goodIK, -- Immortal King's Stone Crusher (Ogre Maul)
	["uar"] = {priority = 6}, -- Immortal King's Soul Cage (Sacred Armor)
	--["zhb"] = goodIK, -- Immortal King's Detail (War Belt)
	--["xhg"] = goodIK, -- Immortal King's Forge (War Gauntlets)
	--["xhb"] = goodIK, -- Immortal King's Pillar (War Boots)
--]]
--------------------------------------------------
---[[ M'avina's Battle Hymn
	["ci3"] = {priority = 6}, -- M'avina's True Sight (Diadem)
	--["amc"] = {priority = 4}, -- M'avina's Caster (Grand Matron Bow)
	--["uld"] = goodMavinas, -- M'avina's Embrace (Kraken Shell)
	--["xtg"] = goodMavinas, -- M'avina's Icy Clutch (Battle Gauntlets)
	--["zvb"] = goodMavinas, -- M'avina's Tenet (Sharkskin Belt)
--]]
--------------------------------------------------
--[[ Naj's Ancient Vestige
	["ci0"] = {}, -- Naj's Circlet (Circlet)
	["ult"] = {}, -- Naj's Light Plate (Hellforge Plate)
	["6cs"] = {}, -- Naj's Puzzler (Elder Staff)
--]]
--------------------------------------------------
---[[ Natalya's Odium
	--["xh9"] = goodNatalyas, -- Natalya's Totem (Grim Helm)
	["7qr"] = {priority = 6}, -- Natalya's Mark (Scissors Suwayyah)
	--["ucl"] = goodNatalyas, -- Natalya's Shadow (Loricated Mail)
	--["xmb"] = goodNatalyas, -- Natalya's Soul (Mesh Boots)
--]]
--------------------------------------------------
--[[ Orphan's Call
	["xhm"] = {}, -- Guillaume's Face (Winged Helm)
	["xml"] = {}, -- Whitstan's Guard (Round Shield)
	["xvg"] = {}, -- Magnus' Skin (Sharkskin Gloves)
	["ztb"] = {}, -- Wilhelm's Pride (Battle Belt)
--]]
--------------------------------------------------
--[[ Sander's Folly
	---------------- Sander's Paragon (Cap) (Duplicate)
	["bwn"] = {}, -- Sander's Superstition (Bone Wand)
	["vgl"] = {}, -- Sander's Taboo (Heavy Gloves)
	---------------- Sander's Riprap (Heavy Boots) (Duplicate)
--]]
--------------------------------------------------
--[[ Sazabi's Grand Tribute
	["xhl"] = {}, -- Sazabi's Mental Sheath (Basinet)
	["7ls"] = {}, -- Sazabi's Cobalt Redeemer (Cryptic Sword)
	["upl"] = {}, -- Sazabi's Ghost Liberator (Balrog Skin)
--]]
--------------------------------------------------
---[[ Tal Rasha's Wrappings
	--["oba"] = goodTalRashas, -- Tal Rasha's Lidless Eye (Swirling Crystal)
	--["xsk"] = goodTalRashas, -- Tal Rasha's Horadric Crest (Death Mask)
	["uth"] = {priority = 6}, -- Tal Rasha's Guardianship (Lacquered Plate)
	--["zmb"] = goodTalRashas, -- Tal Rasha's Fine-Spun Cloth (Mesh Belt)
	------------------ Tal Rasha's Adjudication (Amulet) (Duplicate)
--]]
--------------------------------------------------
--[[ The Disciple
	---------------- Telling of Beads (Amulet) (Duplicate)
	["ulg"] = {}, -- Laying of Hands (Bramble Mitts)
	["uui"] = {}, -- Dark Adherent (Dusk Shroud)
	["xlb"] = {}, -- Rite of Passage (Demonhide Boots)
	["umc"] = {}, -- Credendum (Mithril Coil)
--]]
--------------------------------------------------
---[[ Trang-Oul's Avatar
	["uh9"] = goodTrangs, -- Trang-Oul's Guise (Bone Visage)
	--["xul"] = {priority = 4}, -- Trang-Oul's Scales (Chaos Armor)
	["ne9"] = goodTrangs, -- Trang-Oul's Wing (Cantor Trophy)
	--["utc"] = goodTrangs, -- Trang-Oul's Girth (Troll Belt)
	--["xmg"] = goodTrangs, -- Trang-Oul's Claws (Heavy Bracers)
--]]

--------------------------------------------------
--------------- Classic --------------------------
--------------------------------------------------
--[[ Angelic Raiment
	["rng"] = {}, -- Angelic Mantle (Ring Mail)
	["sbr"] = {}, -- Angelic Sickle (Sabre)
	---------------- Angelic Halo (Ring) (Duplicate)
	---------------- Angelic Wings (Amulet) (Duplicate)
--]]
--------------------------------------------------
--[[ Arcanna's Tricks
	["skp"] = {}, -- Arcanna's Head (Skull Cap)
	["ltp"] = {}, -- Arcanna's Flesh (Light Plate)
	["wst"] = {}, -- Arcanna's Deathwand (War Staff)
	---------------- Arcanna's Sign (Amulet) (Duplicate)
--]]
--------------------------------------------------
--[[ Arctic Gear
	["qui"] = {}, -- Arctic Furs (Quilted Armor)
	["vbl"] = {}, -- Arctic Binding (Light Belt)
	---------------- Arctic Mitts (Light Gauntlets) (Duplicate)
	["swb"] = {}, -- Arctic Horn (Short War Bow)
--]]
--------------------------------------------------
--[[ Berserker's Arsenal
	["hlm"] = {}, -- Berserker's Headgear (Helm)
	["spl"] = {}, -- Berserker's Hauberk (Splint Mail)
	["2ax"] = {}, -- Berserker's Hatchet (Double Axe)
--]]
--------------------------------------------------
--[[ Cathan's Traps
	["msk"] = {}, -- Cathan's Visage (Mask)
	["chn"] = {}, -- Cathan's Mesh (Chain Mail)
	["bst"] = {}, -- Cathan's Rule (Battle Staff)
	---------------- Cathan's Sigil (Amulet) (Duplicate)
	---------------- Cathan's Seal (Ring) (Duplicate)
--]]
--------------------------------------------------
--[[ Civerb's Vestments
	["gsc"] = {}, -- Civerb's Cudgel (Grand Scepter)
	---------------- Civerb's Icon (Amulet) (Duplicate)
	["lrg"] = {}, -- Civerb's Ward (Large Shield)
--]]
--------------------------------------------------
--[[ Cleglaw's Brace
	["lsd"] = {}, -- Cleglaw's Tooth (Long Sword)
	["mgl"] = {}, -- Cleglaw's Pincers (Chain Gloves)
	["sml"] = {}, -- Cleglaw's Claw (Small Shield)
--]]
--------------------------------------------------
--[[ Death's Disguise
	["wsd"] = {}, -- Death's Touch (War Sword)
	["lgl"] = {}, -- Death's Hand (Leather Gloves)
	["lbl"] = {}, -- Death's Guard (Sash)
--]]
--------------------------------------------------
--[[ Hsarus' Defense
	["buc"] = {}, -- Hsarus' Iron Fist (Buckler)
	---------------- Hsarus' Iron Stay (Belt) (Duplicate)
	["mbt"] = {}, -- Hsarus' Iron Heel (Chain Boots)
--]]
--------------------------------------------------
--[[ Infernal Tools
	---------------- Infernal Cranium (Cap) (Duplicate)
	---------------- Infernal Sign (Heavy Belt) (Duplicate)
	["gwn"] = {}, -- Infernal Torch (Grim Wand)
--]]
--------------------------------------------------
--[[ Iratha's Finery
	---------------- Iratha's Coil (Crown) (Duplicate)
	---------------- Iratha's Collar (Amulet) (Duplicate)
	---------------- Iratha's Cord (Heavy Belt) (Duplicate)
	---------------- Iratha's Cuff (Light Gauntlets) (Duplicate)
--]]
--------------------------------------------------
--[[ Isenhart's Armory
	["bsd"] = {}, -- Isenhart's Lightbrand (Broad Sword)
	["fhl"] = {}, -- Isenhart's Horns (Full Helm)
	["brs"] = {}, -- Isenhart's Case (Breast Plate)
	["gts"] = {}, -- Isenhart's Parry (Gothic Shield)
--]]
--------------------------------------------------
--[[ Milabrega's Regalia
	---------------- Milabrega's Diadem (Crown) (Duplicate)
	["aar"] = {}, -- Milabrega's Robe (Ancient Armor)
	["kit"] = {}, -- Milabrega's Orb (Kite Shield)
	["wsp"] = {}, -- Milabrega's Rod (War Scepter)
--]]
--------------------------------------------------
--[[ Sigon's Complete Steel
	["ghm"] = {}, -- Sigon's Visor (Great Helm)
	["gth"] = {}, -- Sigon's Shelter (Gothic Plate)
	["hbt"] = {}, -- Sigon's Sabot (Greaves)
	["tow"] = {}, -- Sigon's Guard (Tower Shield)
	["hbl"] = {}, -- Sigon's Wrap (Plated Belt)
	["hgl"] = {}, -- Sigon's Gage (Gauntlets)
--]]
--------------------------------------------------
--[[ Tancred's Battlegear
	["bhm"] = {}, -- Tancred's Skull (Bone Helm)
	["ful"] = {}, -- Tancred's Spine (Full Plate Mail)
	["lbt"] = {}, -- Tancred's Hobnails (Boots)
	["mpi"] = {}, -- Tancred's Crowbill (Military Pick)
	---------------- Tancred's Weird (Amulet) (Duplicate)
--]]
--------------------------------------------------
--[[ Vidala's Rig
	["lbb"] = {}, -- Vidala's Barb (Long Battle Bow)
	["lea"] = {}, -- Vidala's Ambush (Leather Armor)
	["tbt"] = {}, -- Vidala's Fetlock (Light Plated Boots)
	---------------- Vidala's Snare (Amulet) (Duplicate)
--]]

--------------------------------------------------
--------------- Duplicates -----------------------
--------------------------------------------------
---[[ Amulet
	["amu"] =
			{ identify = goodSetAmulet, priority = 4,
				goodItem =
					( Unidentified
						--or SetIndex == 53 -- Angelic Wings
						--or SetIndex == 58 -- Arcanna's Sign
						--or SetIndex == 28 -- Cathan's Sigil
						--or SetIndex == 1 -- Civerb's Icon
						--or SetIndex == 9 -- Iratha's Collar
						or SetIndex == 77 -- Tal Rasha's Adjudication
						--or SetIndex == 33 -- Tancred's Weird
						--or SetIndex == 95 -- Telling of Beads
						--or SetIndex == 20 -- Vidala's Snare
					)
			},
--]]
--------------------------------------------------
--[[ Belt
	["mbl"] =
			{ identify = true,
				goodItem =
					( Unidentified
						--or SetIndex == 5 -- Hsarus' Iron Stay
						--or SetIndex == 110 -- Hwanin's Blessing
					)
			},
--]]
--------------------------------------------------
--[[ Cap
	["cap"] =
			{ identify = true,
				goodItem =
					( Unidentified
						--or SetIndex == 41 -- Infernal Cranium
						--or SetIndex == 123 -- Sander's Paragon
					)
			},
--]]
--------------------------------------------------
--[[ Crown
	["crn"] =
			{ identify = true,
				goodItem =
					( Unidentified
						--or SetIndex == 11 -- Iratha's Coil
						--or SetIndex == 23 -- Milabrega's Diadem
					)
			},
--]]
--------------------------------------------------
--[[ Heavy Belt
	["tbl"] =
			{ identify = true,
				goodItem =
					( Unidentified
						--or SetIndex == 43 -- Infernal Sign
						--or SetIndex == 12 -- Iratha's Cord
					)
			},
--]]
--------------------------------------------------
--[[ Heavy Boots
	["vbt"] =
			{ identify = true,
				goodItem =
					( Unidentified
						--or SetIndex == 119 -- Cow King's Hooves
						--or SetIndex == 124 -- Sander's Riprap
					)
			},
--]]
--------------------------------------------------
--[[ Light Gauntlets
	["tgl"] =
			{ identify = true,
				goodItem =
					( Unidentified
						--or SetIndex == 57 -- Arctic Mitts
						--or SetIndex == 10 -- Iratha's Cuff
					)
			},
--]]
--------------------------------------------------
--[[ Ring
	["rin"] =
			{ identify = true,
				goodItem =
					( Unidentified
						--or SetIndex == 52 -- Angelic Halo
						--or SetIndex == 29 -- Cathan's Seal
					)
			},
--]]
--------------------------------------------------
}