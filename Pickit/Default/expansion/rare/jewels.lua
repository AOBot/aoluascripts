-- expansion\rare\jewels.lua "moderate"

local isGoodRareJewel =
function(item)
	local prefixes, suffixes = 0, 0
	local damMods = false
		
	prefixes =
		count{
			MaxDamagePercent >= 23,
			AllResist >= 8,
			SingleResist >= 28,
			ArmorClass >= 35,
			ToHit >= 51,
			DamageToMana ~= 0,
			TotalResist >= 50,
			--DemonDamagePercent >= 35,
		}
	suffixes =
		count{
			FasterHitRecovery ~= 0,
			LowerRequirementsPercent ~= 0,
			Strength >= 7,
			Dexterity >= 7,
			Energy >= 7,
			--LightMaxDamage >= 81,
		}
	damMods =
		( MaxDamagePercent >= 28
			or (MaxDamage + MinDamage) >= 13
			or MaxDamagePercent >= 23
				and (MaxDamage >= 7 or MinDamage >= 7)
		)
	return
		( Unidentified
			or (prefixes + suffixes) >= 2
			or damMods
			--or prefixes >= 2
			--or suffixes >= 2
		)
end

rare.jewels =
{ checkStats = true,
	["jew"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareJewel
		}
}