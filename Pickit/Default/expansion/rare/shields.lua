-- expansion\rare\shields.lua "moderate"

local isGoodRareShield =
function(item)
	local sock, fbr, goodMod = false, false, 0

	sock = Sockets >= 2
    fbr = FasterBlockRate >= 30
    goodMod = 
    	count{
			FasterHitRecovery >= 10,
    		Strength >= 7,
    		MaxLife >= 31,
    		DefensePercent >= 81,
    		ClassSkillsBonus >= 2,
    		--SkillTabBonus >= 2,
			TotalResist >= 50,
			AllResist >= 16,
    	}
	return
		( Unidentified
			or sock and fbr
			    and goodMod >= 1
			--or sock
				--and goodMod >= 1
			--or fbr
				--and goodMod >= 1
		
		)
end
local goodRareShield =
		{
			priority = 3, identify = true,
				isGoodItem = isGoodRareShield
		}

rare.shields =
{ checkStats = true,
--[[ Normal Shields
	--["buc"] = goodRareShield, -- Buckler
	["sml"] = goodRareShield, -- Small Shield
	["lrg"] = goodRareShield, -- Large Shield
	["kit"] = goodRareShield, -- Kite Shield
	["spk"] = goodRareShield, -- Spiked Shield
	["tow"] = goodRareShield, -- Tower Shield
	["bsh"] = goodRareShield, -- Bone Shield
	["gts"] = goodRareShield, -- Gothic Shield
--]]

---[[ Exceptional Shields
	--["xuc"] = goodRareShield, -- Defender
	--["xml"] = goodRareShield, -- Round Shield
	--["xrg"] = goodRareShield, -- Scutum
	["xit"] = goodRareShield, -- Dragon Shield
	["xpk"] = goodRareShield, -- Barbed Shield
	["xow"] = goodRareShield, -- Pavise
	["xsh"] = goodRareShield, -- Grim Shield
	["xts"] = goodRareShield, -- Ancient Shield
--]]

---[[ Elite Shields
	["uuc"] = goodRareShield, -- Heater
	["uml"] = goodRareShield, -- Luna
	["urg"] = goodRareShield, -- Hyperion
	["uit"] = goodRareShield, -- Monarch
	["uow"] = goodRareShield, -- Aegis
	["uts"] = goodRareShield, -- Ward
	["ush"] = goodRareShield, -- Troll Nest
	["upk"] = goodRareShield, -- Blade Barrier
--]]
}
--[[
------Prefixes-------
-- 12-15% all resists
-- 16-20% all resists
-- 20-30% Single Resist
-- 1-2 Sockets
-- +2 to Paladin Skills
-- +2 to Paladin Skill Tree
------Suffixes--------
-- 10% Block 15% FBR
-- 20% Block 30% FBR
-- 17% FHR
-- +31-40 to Life
--]]