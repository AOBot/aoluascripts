-- expansion\rare\gloves.lua "moderate"

local isGoodRareGloves =
function(item)
	if Ethereal and Identified and ReplenishDurability == 0 then
		return false -- not keeping eth unless repairs
	end
	local skill, ias, stat, other = false, false, false, false
	skill =
		( JavelinAndSpearTab >= 2
			or PassiveAndMagicTab >= 2
			or BowAndCrossBowTab >= 2
			or MartialArtsTab >= 2
		)
	ias = FasterAttackRate >= 20
	stat =
		( Strength >= 7
			or Dexterity >= 13
			--or MaxMana >= 31
		)
	other =
		( FasterHitRecovery >= 10
			or TotalResist >= 50
			or LifeDrainMinDamage >= 3 and ManaDrainMinDamage >= 3
			or ToHitPercent >= 5
			or DefensePercent >= 81
			or ArmorPerLevel ~= 0
		)
	return
		( Unidentified
			or skill and ias and (stat or other)
			--or ias and stat and other
			--or skill and ias
		)
end

rare.gloves =
{ checkStats = true,
	["Gloves"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareGloves
		}
}

--[[
prefix
defperlevel ?
mana 31-40
zontabs 2
sinMAtab 2

suffix
str 10-15
dex 10-15
ar% 5
ll 2-5
ml 2-5
ias 20
gold 41-80
mf 16-25
]]