-- expansion\rare\rings.lua "moderate"

local isGoodRareRing =
function(item)
	local fcr, goodMods, statMods, meleeMods = false, 0, 0, 0
	
	fcr = FasterCastRate >= 10

	goodMods =
		count{
			TotalResist >= 50,
			MagicFind >= 16,
			AllResist >= 9,
		}

	statMods =
		count{
			MaxLife >= 21,
			MaxMana >= 41,
			Strength >= 16,
			--Dexterity >= 10 and Energy >= 11,
			Dexterity >= 10,
			Energy >= 11,
		}

	meleeMods =
		count{
			ToHit >= 81,
			ToHitPercent ~= 0,
			LifeDrainMinDamage >= 5 and ManaDrainMinDamage >= 4,
			MinDamage >= 3 and MaxDamage >= 5,
		}

	return
		( Unidentified
			or fcr
				and (goodMods + statMods) >= 2
			or (statMods + meleeMods) >= 3
			or (goodMods + meleeMods) >= 2
		)
end

rare.rings =
{ checkStats = true,
	["rin"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareRing
		}
}

--[[
prefix
ar 101-120
mana 61-90
sres 31-40
ares 8-11
mf 5-10

suffix
str 16-20
dex 10-15
eng 10-15
life 31-40
ar% 5
ll 6-9
ml 6-9
gold 25-40
mf 16-25
fcr 10
]]