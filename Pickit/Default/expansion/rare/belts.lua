-- expansion\rare\belts.lua "moderate"

local isGoodRareBelt =
function(item)
	if Ethereal and Identified and ReplenishDurability == 0 then
		return false -- not keeping eth unless repairs
	end
	return
		( Unidentified
			or FasterHitRecovery >= 17
				and Strength >= 16
				and
					( MaxLife >= 31
						or TotalResist >= 50
						or DefensePercent >= 81
						or ArmorPerLevel ~= 0
					)
		)
end

rare.belts =
{ checkStats = true,
	["Belt"] =
		{ priority = 4, identify = true,
			isGoodItem = isGoodRareBelt
		}
}

--[[
prefix
mana 21-31
defperlevel ?

suffix
str 21-30
life 41-60
fhr 24
gold 41-80
]]