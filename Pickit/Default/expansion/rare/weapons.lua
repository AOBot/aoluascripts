-- expansion\rare\weapons.lua "moderate"

----------------------------------------------------
-------------------- Settings ----------------------
----------------------------------------------------

-- Minimum Skill Level Setting
local minSkillLevel = 5
-- Fools/Grinding
local foolsMod = MaxDamagePerLevel ~= 0 and ToHitPerLevel ~= 0

----------------- Sorceress Settings --------------------
local isGoodRareSorcWeap =
function(item)
	local sorcSkiller = false

	sorcSkiller = 
		(ClassSkillsBonus + FireTab + FireBallSkill)
		or (ClassSkillsBonus + FireTab + MeteorSkill)
		or (ClassSkillsBonus + FireTab + EnchantSkill)
		--or (ClassSkillsBonus + FireTab + FireBoltSkill)
		--or (ClassSkillsBonus + FireTab + WarmthSkill)
		--or (ClassSkillsBonus + FireTab + InfernoSkill)
		--or (ClassSkillsBonus + FireTab + BlazeSkill)
		--or (ClassSkillsBonus + FireTab + FireWallSkill)
		--or (ClassSkillsBonus + FireTab + HydraSkill)
		--or (ClassSkillsBonus + FireTab + FireMasterySkill)
		or (ClassSkillsBonus + ColdTab + BlizzardSkill)
		or (ClassSkillsBonus + ColdTab + FrozenOrbSkill)
		--or (ClassSkillsBonus + ColdTab + IceBoltSkill)
		--or (ClassSkillsBonus + ColdTab + FrozenArmorSkill)
		--or (ClassSkillsBonus + ColdTab + FrostNovaSkill)
		--or (ClassSkillsBonus + ColdTab + IceBlastSkill)
		--or (ClassSkillsBonus + ColdTab + ShiverArmorSkill)
		--or (ClassSkillsBonus + ColdTab + GlacialSpikeSkill)
		--or (ClassSkillsBonus + ColdTab + ChillingArmorSkill)
		--or (ClassSkillsBonus + ColdTab + ColdMasterySkill)
		or (ClassSkillsBonus + LightningTab + LightningSkill)
		--or (ClassSkillsBonus + LightningTab + ChainLightningSkill)
		--or (ClassSkillsBonus + LightningTab + ChargedBoltSkill)
		--or (ClassSkillsBonus + LightningTab + StaticFieldSkill)
		--or (ClassSkillsBonus + LightningTab + TelekinesisSkill)
		--or (ClassSkillsBonus + LightningTab + NovaSkill)
		--or (ClassSkillsBonus + LightningTab + ThunderStormSkill)
		--or (ClassSkillsBonus + LightningTab + EnergyShieldSkill)
		--or (ClassSkillsBonus + LightningTab + TeleportSkill)
		--or (ClassSkillsBonus + LightningTab + LightningMasterySkill)
		    
	return
		( Unidentified
			or sorcSkiller >= minSkillLevel
          	--or SkillTabBonus >= 2
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Assassin Settings --------------------
local isGoodRareAssaWeap =
function(item)
	local sinSkiller, greatMods = false, false
	
	sinSkiller = 
		(ClassSkillsBonus + TrapsTab + LightningSentrySkill) 
		--or (ClassSkillsBonus + TrapsTab + DeathSentrySkill)
		--or (ClassSkillsBonus + TrapsTab + FireBlastSkill)
		--or (ClassSkillsBonus + TrapsTab + ShockWebSkill)
		--or (ClassSkillsBonus + TrapsTab + BladeSentinelSkill)
		--or (ClassSkillsBonus + TrapsTab + ChargedBoltSentrySkill)
		--or (ClassSkillsBonus + TrapsTab + WakeOfFireSkill)
		--or (ClassSkillsBonus + TrapsTab + BladeFurySkill)
		--or (ClassSkillsBonus + TrapsTab + WakeOfInfernoSkill)
		--or (ClassSkillsBonus + TrapsTab + BladeShieldSkill)
		or (ClassSkillsBonus + ShadowDisciplinesTab + VenomSkill) 
		--or (ClassSkillsBonus + ShadowDisciplinesTab + ClawMasterySkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + PsychicHammerSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + BurstOfSpeedSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + WeaponBlockSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + CloakOfShadowsSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + FadeSkill) 
		--or (ClassSkillsBonus + ShadowDisciplinesTab + ShadowWarriorSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + MindBlastSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + ShadowMasterSkill)
		--or (ClassSkillsBonus + MartialArtsTab + DragonTalonSkill) 
		--or (ClassSkillsBonus + MartialArtsTab + PhoenixStrikeSkill) 
		--or (ClassSkillsBonus + MartialArtsTab + TigerStrikeSkill) 
		--or (ClassSkillsBonus + MartialArtsTab + FistsOfFireSkill)
		--or (ClassSkillsBonus + MartialArtsTab + DragonClawSkill)
		--or (ClassSkillsBonus + MartialArtsTab + CobraStrikeSkill)
		--or (ClassSkillsBonus + MartialArtsTab + ClawsOfThunderSkill)
		--or (ClassSkillsBonus + MartialArtsTab + DragonTailSkill) 
		--or (ClassSkillsBonus + MartialArtsTab + BladesOfIceSkill)
		--or (ClassSkillsBonus + MartialArtsTab + DragonFlightSkill)
		
	greatMods =
			(MaxDamagePercent >= 300 or MaxDamagePercent >= 200 and foolsMod)
				and FasterAttackRate >= 20
				and Sockets >= 2
				--and ReplenishDurability ~= 0
			
	return
		( Unidentified
			or sinSkiller >= minSkillLevel
			or Ethereal and greatMods
          	--or SkillTabBonus >= 2
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Paladin Settings --------------------
local isGoodRarePalWeap =
function(item)
	local pallySkiller = false

	pallySkiller = 
		(ClassSkillsBonus + PaladinCombatTab + BlessedHammerSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + SacrificeSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + SmiteSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + HolyBoltSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + ZealSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + ChargeSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + VengeanceSkill)
		--or (ClassSkillsBonus + PaladinCombatTab + ConversionSkill)
		--or (ClassSkillsBonus + PaladinCombatTab + HolyShieldSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + FistOfTheHeavensSkill)		
		--or (ClassSkillsBonus + OffensiveAurasTab + ConcentrationSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + FanaticismSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + ConvictionSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + MightSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + HolyFireSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + ThornsSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + BlessedAimSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + HolyFreezeSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + HolyShockSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + SanctuarySkill) 		
		--or (ClassSkillsBonus + DefensiveAurasTab + RedemptionSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + PrayerSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + ResistFireSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + DefianceSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + ResistColdSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + CleansingSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + ResistLightningSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + VigorSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + MeditationSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + SalvationSkill)

	return
		( Unidentified
			or pallySkiller >= minSkillLevel
          	--or SkillTabBonus >= 2
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Necromancer Settings --------------------
local isGoodRareNecWeap =
function(item)
	local necroSkiller = false
	
	necroSkiller = 
		(ClassSkillsBonus + PoisonAndBoneTab + BoneSpearSkill)
		or (ClassSkillsBonus + PoisonAndBoneTab + BoneSpiritSkill)
		or (ClassSkillsBonus + PoisonAndBoneTab + PoisonNovaSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + TeethSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BoneArmorSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + PoisonDaggerSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + CorpseExplosionSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BoneWallSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + PoisonExplosionSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BonePrisonSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + RaiseSkeletonSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + SkeletonMasterySkill)
		--or (ClassSkillsBonus + NecroSummoningTab + ClayGolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + GolemMasterySkill)
		--or (ClassSkillsBonus + NecroSummoningTab + RaiseSkeletalMageSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + BloodgolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + SummonResistSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + IronGolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + FiregolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + ReviveSkill)
		--or (ClassSkillsBonus + CursesTab + LowerResistSkill)
		--or (ClassSkillsBonus + CursesTab + DecrepifySkill)
		--or (ClassSkillsBonus + CursesTab + AmplifyDamageSkill)
		--or (ClassSkillsBonus + CursesTab + DimVisionSkill)
		--or (ClassSkillsBonus + CursesTab + WeakenSkill)
		--or (ClassSkillsBonus + CursesTab + IronMaidenSkill)
		--or (ClassSkillsBonus + CursesTab + TerrorSkill)
		--or (ClassSkillsBonus + CursesTab + ConfuseSkill)
		--or (ClassSkillsBonus + CursesTab + LifeTapSkill)
		--or (ClassSkillsBonus + CursesTab + AttractSkill)

	return
		( Unidentified
			or necroSkiller >= minSkillLevel
          	--or SkillTabBonus >= 2
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Non Class Settings --------------------
local goodRareWeapon =
{ priority = 3, identify = true, goodItem =
	(Ethereal and
		(Unidentified or
			(MaxDamagePercent >= 300 or MaxDamagePercent >= 200 and foolsMod)
				and FasterAttackRate >= 30
				and (Sockets >= 2 or ReplenishDurability ~= 0)
		)
	)
}

local goodRareThrowingWeapon =
{ priority = 3, identify = true, goodItem =
	(Ethereal and
		(Unidentified or
			(MaxDamagePercent >= 300 or MaxDamagePercent >= 200 and foolsMod)
				and FasterAttackRate >= 30
				and ReplenishQuantity ~= 0
		)
	)
}

local goodRareBow =
{ priority = 3, identify = true, goodItem =
	(Unidentified or
		(MaxDamagePercent >= 300 or MaxDamagePercent >= 200 and foolsMod)
			and FasterAttackRate >= 20
			and Sockets >= 2
	)
}

local goodRareDagger =
{ priority = 3, identify = true, goodItem =
	(Ethereal and
		(Unidentified or
			(MaxDamagePercent >= 300 or MaxDamagePercent >= 200 and foolsMod)
				and FasterAttackRate >= 30
				and ReplenishDurability ~= 0
				--and IgnoreTargetDefense ~= 0
		)
	)
}

local goodRarePhaseBlade =
{ priority = 3, identify = true, goodItem =
	(Unidentified or
		(MaxDamagePercent >= 300 or MaxDamagePercent >= 200 and foolsMod)
			and FasterAttackRate >= 30
			and Sockets >= 2
	)
}

local rareAct2MercWeapon =
{ priority = 3, identify = true, goodItem =
	(Ethereal and
		(Unidentified or
			(MaxDamagePercent >= 425 or MaxDamagePercent >= 275 and foolsMod)
				and FasterAttackRate >= 30
				and Sockets >= 2
				--and LifeDrainMinDamage >= 7
				--and AmplifyDamageOnStrikingChance ~= 0 
		)
	)
}

---------------------------------------------------
----------------- Rare Weapons --------------------
---------------------------------------------------

rare.weapons =
{ checkStats = true,
---[[ Sorceress Orbs
	["Orb"] =
		{ priority = 3, identify = true,
   			isGoodItem = isGoodRareSorcWeap
		},
--]]

--[[ Sorceress Staffs
	["Staff"] =
		{ priority = 3, identify = true,
   			isGoodItem = isGoodRareSorcWeap
		},
--]]

---[[ Assassin Claws
	["HandToHand2"] =  
		{ priority = 3, identify = true,
   			isGoodItem = isGoodRareAssaWeap
		},
--]]

--[[ Scepters
	["Scepter"] =  
		{ priority = 3, identify = true,
			isGoodItem = isGoodRarePalWeap
		},
--]]

--[[ Wands
	["Wand"] = 
		{ priority = 3, identify = true,
   			isGoodItem = isGoodRareNecWeap
		},
--]]

---[[ Amazon Javelins
	["AmazonJavelin"] = 
		{ priority = 3, identify = true, goodItem =
			(Ethereal and
				(Unidentified or
					((ClassSkillsBonus + SkillTabBonus) >= 4
						--or MaxDamagePercent >= 300 
						--or MaxDamagePercent >= 200 and foolsMod
					)
							and FasterAttackRate >= 30
							and ReplenishQuantity ~= 0
				)
			)
		},
--]]

--[[ Amazon Spears
	["AmazonSpear"] = 
		{ priority = 3, identify = true, goodItem =
			(Ethereal and
				(Unidentified or
					((ClassSkillsBonus + SkillTabBonus) >= 4
						or MaxDamagePercent >= 300 
						or MaxDamagePercent >= 200 and foolsMod
					)
							and FasterAttackRate >= 30
							and Sockets >= 2
							and ReplenishDurability ~= 0
				)
			)
		},
--]]

--[[ Amazon Bows
	["AmazonBow"] = 
		{ priority = 3, identify = true, goodItem =
			(Unidentified or
					(MaxDamagePercent >= 300 
						or MaxDamagePercent >= 200 and foolsMod
						or (ClassSkillsBonus + SkillTabBonus) >= 4
					)
							and FasterAttackRate >= 20
							and Sockets >= 2
			)
		},
--]]

----------------------------------------
-- Axes 
----------------------------------------
--[[ Normal Axes
	["hax"] = goodRareWeapon, -- Hand Axe
	["axe"] = goodRareWeapon, -- Axe
	["2ax"] = goodRareWeapon, -- Double Axe
	["mpi"] = goodRareWeapon, -- Military Pick
	["wax"] = goodRareWeapon, -- War Axe
	["lax"] = goodRareWeapon, -- Large Axe
	["bax"] = goodRareWeapon, -- Broad Axe
	["btx"] = goodRareWeapon, -- Battle Axe
	["gax"] = goodRareWeapon, -- Great Axe
	["gix"] = goodRareWeapon, -- Giant Axe
--]]

--[[ Exceptional Axes
	["9ha"] = goodRareWeapon, -- Hatchet
	["9ax"] = goodRareWeapon, -- Cleaver
	["92a"] = goodRareWeapon, -- Twin Axe
	["9mp"] = goodRareWeapon, -- Crowbill
	["9wa"] = goodRareWeapon, -- Naga
	["9la"] = goodRareWeapon, -- Military Axe
	["9ba"] = goodRareWeapon, -- Bearded Axe
	["9bt"] = goodRareWeapon, -- Tabar
	["9ga"] = goodRareWeapon, -- Gothic Axe
	["9gi"] = goodRareWeapon, -- Ancient Axe
--]]

---[[ Elite Axes
    --["7ha"] = goodRareWeapon, -- Tomahalk
	--["7ax"] = goodRareWeapon, -- Small Crescent
	["72a"] = goodRareWeapon, -- Ettin Axe
	["7mp"] = goodRareWeapon, -- War Spike
	["7wa"] = goodRareWeapon, -- Berserker Axe
	--["7la"] = goodRareWeapon, -- Feral Axe
	--["7ba"] = goodRareWeapon, -- Silver Edged Axe
	--["7bt"] = goodRareWeapon, -- Decapitator
	["7ga"] = goodRareWeapon, -- Champion Axe
	["7gi"] = goodRareWeapon, -- Glorious Axe
--]]

----------------------------------------
-- Bows
----------------------------------------
--[[ Normal Bows
	["sbw"] = goodRareBow, -- Short Bow
	["hbw"] = goodRareBow, -- Hunter's Bow
	["lbw"] = goodRareBow, -- Long Bow
	["cbw"] = goodRareBow, -- Composite Bow
	["sbb"] = goodRareBow, -- Short Battle Bow
	["lbb"] = goodRareBow, -- Long Battle Bow
	["swb"] = goodRareBow, -- Short War Bow
	["lwb"] = goodRareBow, -- Long War Bow
--]]

--[[ Exceptional Bows
	["8sb"] = goodRareBow, -- Edge Bow
	["8hb"] = goodRareBow, -- Razor Bow
	["8lb"] = goodRareBow, -- Cedar Bow
	["8cb"] = goodRareBow, -- Double Bow
	["8s8"] = goodRareBow, -- Short Siege Bow
	["8l8"] = goodRareBow, -- Large Siege Bow
	["8sw"] = goodRareBow, -- Rune Bow
	["8lw"] = goodRareBow, -- Gothic Bow
--]]

---[[ Elite Bows
	--["6sb"] = goodRareBow, -- Spider Bow
	--["6hb"] = goodRareBow, -- Blade Bow
	--["6lb"] = goodRareBow, -- Shadow Bow
	["6cb"] = goodRareBow, -- Great Bow
	["6s7"] = goodRareBow, -- Diamond Bow
	["6l7"] = goodRareBow, -- Crusader Bow
	["6sw"] = goodRareBow, -- Ward Bow
	["6lw"] = goodRareBow, -- Hydra Bow
--]]

----------------------------------------
-- Crossbows
----------------------------------------
--[[ Normal Crossbows
	["lxb"] = goodRareBow, -- Light Crossbow
	["mxb"] = goodRareBow, -- Crossbow
	["hxb"] = goodRareBow, -- Heavy Crossbow
	["rxb"] = goodRareBow, -- Repeating Crossbow
--]]

--[[ Exceptional Crossbows
	["8lx"] = goodRareBow, -- Arbalest
	["8mx"] = goodRareBow, -- Siege Crossbow
	["8hx"] = goodRareBow, -- Ballista
	["8rx"] = goodRareBow, -- Chu-Ko-Nu
--]]

--[[ Elite Crossbows
	["6lx"] = goodRareBow, -- Pellet Bow
	["6mx"] = goodRareBow, -- Gorgon Crossbow
	["6hx"] = goodRareBow, -- Colossus Crossbow
	["6rx"] = goodRareBow, -- Demon Crossbow
--]]

----------------------------------------
-- Daggers
----------------------------------------
--[[ Normal Daggers
	["dgr"] = goodRareDagger, -- Dagger
	["dir"] = goodRareDagger, -- Dirk
	["kri"] = goodRareDagger, -- Kris
	["bld"] = goodRareDagger, -- Blade
--]]

--[[ Exceptional Daggers
	["9dg"] = goodRareDagger, -- Poignard
	["9di"] = goodRareDagger, -- Rondel
	["9kr"] = goodRareDagger, -- Cinquedeas
	["9bl"] = goodRareDagger, -- Stiletto
--]]

---[[ Elite Daggers
	["7dg"] = goodRareDagger, -- Bone Knife
	["7di"] = goodRareDagger, -- Mithral Point
	["7kr"] = goodRareDagger, -- Fanged Knife
	["7bl"] = goodRareDagger, -- Legend Spike
--]]

----------------------------------------
-- Maces 
----------------------------------------
--[[ Normal Maces
	["clb"] = goodRareWeapon, -- Club
	["spc"] = goodRareWeapon, -- Spiked Club
	["mac"] = goodRareWeapon, -- Mace
	["mst"] = goodRareWeapon, -- Morning Star
	["fla"] = goodRareWeapon, -- Flail
	["whm"] = goodRareWeapon, -- War Hammer
	["mau"] = goodRareWeapon, -- Maul
	["gma"] = goodRareWeapon, -- Great Maul
--]]

--[[ Exceptional Maces
	["9cl"] = goodRareWeapon, -- Cudgel
	["9sp"] = goodRareWeapon, -- Barbed Club
	["9ma"] = goodRareWeapon, -- Flanged Mace
	["9mt"] = goodRareWeapon, -- Jagged Star
	["9fl"] = goodRareWeapon, -- Knout
	["9wh"] = goodRareWeapon, -- Battle Hammer
	["9m9"] = goodRareWeapon, -- War Club
	["9gm"] = goodRareWeapon, -- Martel de Fer
--]]

---[[ Elite Maces
    --["7cl"] = goodRareWeapon, -- Truncheon
	--["7sp"] = goodRareWeapon, -- Tyrant Club
	--["7ma"] = goodRareWeapon, -- Reinforced Mace
	--["7mf"] = goodRareWeapon, -- Devil Star
	["7fl"] = goodRareWeapon, -- Scourge
	["7wh"] = goodRareWeapon, -- Legendary Mallet
	["7m7"] = goodRareWeapon, -- Ogre Maul
	["7gm"] = goodRareWeapon, -- Thunder Maul
--]]

----------------------------------------
-- Polearms
----------------------------------------
--[[ Normal Polearms
	["bar"] = rareAct2MercWeapon, -- Bardiche
	["vou"] = rareAct2MercWeapon, -- Voulge
	["scy"] = rareAct2MercWeapon, -- Scythe
	["pax"] = rareAct2MercWeapon, -- Poleaxe
	["hal"] = rareAct2MercWeapon, -- Halberd
	["wsc"] = rareAct2MercWeapon, -- War Scythe
--]]

--[[ Exceptional Polearms
	["9b7"] = rareAct2MercWeapon, -- Lochaber Axe
	["9vo"] = rareAct2MercWeapon, -- Bill
	["9s8"] = rareAct2MercWeapon, -- Battle Scythe
	["9pa"] = rareAct2MercWeapon, -- Partizan
	["9h9"] = rareAct2MercWeapon, -- Bec-De-Corbin
	["9wc"] = rareAct2MercWeapon, -- Grim Scythe
--]]

---[[ Elite Polearms
	["7o7"] = rareAct2MercWeapon, -- Ogre Axe
	["7vo"] = rareAct2MercWeapon, -- Colossus Voulge
	["7s8"] = rareAct2MercWeapon, -- Thresher
	["7pa"] = rareAct2MercWeapon, -- Cryptic Axe
	["7h7"] = rareAct2MercWeapon, -- Great Poleaxe
	["7wc"] = rareAct2MercWeapon, -- Giant Thresher
--]]

----------------------------------------
-- Spears 
----------------------------------------
--[[ Normal Spears
	["spr"] = rareAct2MercWeapon, -- Spear
	["tri"] = rareAct2MercWeapon, -- Trident
	["brn"] = rareAct2MercWeapon, -- Brandistock
	["spt"] = rareAct2MercWeapon, -- Spetum
	["pik"] = rareAct2MercWeapon, -- Pike
--]]

--[[ Exceptional Spears
	["9sr"] = rareAct2MercWeapon, -- War Spear
	["9tr"] = rareAct2MercWeapon, -- Fuscina
	["9br"] = rareAct2MercWeapon, -- War Fork
	["9st"] = rareAct2MercWeapon, -- Yari
	["9p9"] = rareAct2MercWeapon, -- Lance
--]]

---[[ Elite Spears
	["7sr"] = rareAct2MercWeapon, -- Hyperion Spear
	["7tr"] = rareAct2MercWeapon, -- Stygian Pike
	["7br"] = rareAct2MercWeapon, -- Mancatcher
	["7st"] = rareAct2MercWeapon, -- Ghost Spear
	["7p7"] = rareAct2MercWeapon, -- War Pike
--]]

----------------------------------------
-- Swords
----------------------------------------
---[[ Normal Swords
	--["ssd"] = goodRareWeapon, -- Short Sword
	--["scm"] = goodRareWeapon, -- Scimitar
	--["sbr"] = goodRareWeapon, -- Sabre
	--["flc"] = goodRareWeapon, -- Falchion
	["crs"] = goodRareWeapon, -- Crystal Sword
	--["bsd"] = goodRareWeapon, -- Broad Sword
	--["lsd"] = goodRareWeapon, -- Long Sword
	--["wsd"] = goodRareWeapon, -- War Sword
	--["2hs"] = goodRareWeapon, -- Two-Handed Sword
	--["clm"] = goodRareWeapon, -- Claymore
	--["gis"] = goodRareWeapon, -- Giant Sword
	--["bsw"] = goodRareWeapon, -- Bastard Sword
	--["flb"] = goodRareWeapon, -- Flamberge
	--["gsd"] = goodRareWeapon, -- Great Sword
--]]

---[[ Exceptional Swords
	--["9ss"] = goodRareWeapon, -- Gladius
	--["9sm"] = goodRareWeapon, -- Cutlass
	--["9sb"] = goodRareWeapon, -- Shamshir
	--["9fc"] = goodRareWeapon, -- Tulwar
	["9cr"] = goodRareWeapon, -- Dimensional Blade
	--["9bs"] = goodRareWeapon, -- Battle Sword
	--["9ls"] = goodRareWeapon, -- Rune Sword
	--["9wd"] = goodRareWeapon, -- Ancient Sword
	--["92h"] = goodRareWeapon, -- Espandon
	--["9cm"] = goodRareWeapon, -- Dacian Falx
	--["9gs"] = goodRareWeapon, -- Tusk Sword
	--["9b9"] = goodRareWeapon, -- Gothic Sword
	--["9fb"] = goodRareWeapon, -- Zweihander
	--["9gd"] = goodRareWeapon, -- Executioner Sword
--]]

---[[ Elite Swords
	--["7ss"] = goodRareWeapon, -- Falcata
	--["7sm"] = goodRareWeapon, -- Ataghan
	--["7sb"] = goodRareWeapon, -- Elegant Blade
	--["7fc"] = goodRareWeapon, -- Hydra Edge
	["7cr"] = goodRarePhaseBlade, -- Phase Blade
	["7bs"] = goodRareWeapon, -- Conquest Sword
	["7ls"] = goodRareWeapon, -- Cryptic Sword
	["7wd"] = goodRareWeapon, -- Mythical Sword
	["72h"] = goodRareWeapon, -- Legend Sword
	["7cm"] = goodRareWeapon, -- Highland Blade
	["7gs"] = goodRareWeapon, -- Balrog Blade
	["7b7"] = goodRareWeapon, -- Champion Sword
	["7fb"] = goodRareWeapon, -- Colossal Sword
	["7gd"] = goodRareWeapon, -- Colossus Blade
--]]

----------------------------------------
-- Javelins 
----------------------------------------
--[[ Normal Javelins
	["jav"] = goodRareThrowingWeapon, -- Javelin
	["pil"] = goodRareThrowingWeapon, -- Pilum
	["ssp"] = goodRareThrowingWeapon, -- Short Spear
	["glv"] = goodRareThrowingWeapon, -- Glaive
	["tsp"] = goodRareThrowingWeapon, -- Throwing Spear
--]]

--[[ Exceptional Javelins
	["9ja"] = goodRareThrowingWeapon, -- War Javelin
	["9pi"] = goodRareThrowingWeapon, -- Great Pilum
	["9s9"] = goodRareThrowingWeapon, -- Simbilan
	["9gl"] = goodRareThrowingWeapon, -- Spiculum
	["9ts"] = goodRareThrowingWeapon, -- Harpoon
--]]

---[[ Elite Javelins
	["7ja"] = goodRareThrowingWeapon, -- Hyperion Javelin
	["7pi"] = goodRareThrowingWeapon, -- Stygian Pilum
	["7s7"] = goodRareThrowingWeapon, -- Balrog Spear
	["7gl"] = goodRareThrowingWeapon, -- Ghost Glaive
	["7ts"] = goodRareThrowingWeapon, -- Winged Harpoon
--]]

----------------------------------------
-- Throwing Weapons
----------------------------------------
--[[ Normal Throwables
	["tkf"] = goodRareThrowingWeapon, -- Throwing Knife
	["tax"] = goodRareThrowingWeapon, -- Throwing Axe
	["bkf"] = goodRareThrowingWeapon, -- Balanced Knife
	["bal"] = goodRareThrowingWeapon, -- Balanced Axe
--]]

--[[ Exceptional Throwables
	["9tk"] = goodRareThrowingWeapon, -- Battle Dart
	["9ta"] = goodRareThrowingWeapon, -- Francisca
	["9bk"] = goodRareThrowingWeapon, -- War Dart
	["9b8"] = goodRareThrowingWeapon, -- Hurlbat
--]]

---[[ Elite Throwables
	["7tk"] = goodRareThrowingWeapon, -- Flying Knife
	["7ta"] = goodRareThrowingWeapon, -- Flying Axe
	["7bk"] = goodRareThrowingWeapon, -- Winged Knife
	["7b8"] = goodRareThrowingWeapon, -- Winged Axe
--]]

}