-- expansion\rare\amulets.lua "moderate"

local isGoodRareAmulet =
function(item)
-- Good Skill Amount Settings
	local minSkill = 2 -- Class Skills
	local minTab = 2 -- Skill Tab
-------------------------------------------------
	local anySkill, casterSkill, meleeSkill, fcr = false, false, false, false
	local statMods, goodMods, meleeMods = false, 0, 0

	anySkill = (ClassSkillsBonus >= 2 or SkillTabBonus >= 2) 
	casterSkill =
			( PaladinSkills >= minSkill 
			or SorceressSkills >= minSkill 
			or DruidSkills >= minSkill
			or AssassinSkills >= minSkill
			or NecromancerSkills >= minSkill
			or AmazonSkills >= minSkill
			or BarbarianSkills >= minSkill

			or PaladinCombatTab >= minTab
	      	or LightningTab >= minTab
			--or OffensiveAurasTab >= minTab
	        --or DefensiveAurasTab >= minTab
	        or FireTab >= minTab
	        or ColdTab >= minTab
	        --or CursesTab >= minTab
	        or PoisonAndBoneTab >= minTab
	        or NecroSummoningTab >= minTab
	        or WarcriesTab >= minTab
	        or ElementalTab >= minTab
			--or DruidSummoningTab >= minTab
	        or TrapsTab >= minTab
			or BarbarianCombatTab >= minTab
			or MasteriesTab >= minTab
			or JavelinAndSpearTab >= minTab
			)

	meleeSkill =
	    	( BarbarianSkills >= minSkill
	        or AmazonSkills >= minSkill
			or PaladinSkills >= minSkill
			or DruidSkills >= minSkill
			or AssassinSkills >= minSkill

			or PaladinCombatTab >= minTab
	        or BowAndCrossBowTab >= minTab
	        --or PassiveAndMagicTab >= minTab
	        or JavelinAndSpearTab >= minTab
	        or BarbarianCombatTab >= minTab
	        --or MasteriesTab >= minTab
	        or ShapeShiftingTab >= minTab
	        or ShadowDisciplinesTab >= minTab
	        or MartialArtsTab >= minTab
			)
	fcr = FasterCastRate >= 10
	statMods =
			( Strength >= 25 or Dexterity >= 16 or Strength >= 16 and Dexterity >= 11)
			and (MaxLife >= 45 or MaxMana >= 75 or MaxLife >= 36 and (MaxMana >= 56 or Energy >= 11))
	goodMods =
		count{
            (AllResist >= 14 or DoubleResist >= 55 or SingleResist >= 35),
            MagicFind >= 10, --note: 10 mf = perfect prefix mf or any suffix mf
            GoldFind >= 65,
            LifeRegen >= 5,
            DamageToMana >= 12,
            PoisonLengthReduction >= 50,
            TeleportChargesLevel ~= 0,
		}			
	meleeMods =
		count{
			(LifeDrainMinDamage + ManaDrainMinDamage) >= 10,
			(MaxDamage >= 4 or MinDamage >= 9 or MaxDamage >= 3 and MinDamage >= 7),
			ToHitPercent ~= 0,			
		}
	return
		( Unidentified
		    or fcr and casterSkill and statMods and goodMods >= 1
			or meleeSkill and statMods and goodMods >= 1 and meleeMods >= 1
			--or anySkill and (fcr or meleeMods >= 1) and statMods and goodMods >= 1
		)
end

rare.amulets =
{ checkStats = true,
	["amu"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareAmulet
		}
}

--[[ High affix values
prefix
mana 61-90
sres 31-40
ares 16-20
cskill 2
tskill 2
dam2mana 7-12
mf 5-10

suffix
str 21-30
dex 16-20
eng 16-20
life 41-60
ll 6
ml 7-8
mf 16-25
fcr 10
]]