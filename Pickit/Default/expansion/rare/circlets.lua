-- expansion\rare\circlets.lua "moderate"

local isGoodRareCirclet =
function(item)
-- Good Skill Amount Settings
	local minSkill = 2 -- Class Skills
	local minTab = 2 -- Skill Tab
-----------------------------------------------------------------
	local casterSkill, meleeSkill, addStats = false, false, false
	local fcr, frw = false, false
	local goodMods, statMods, meleeMods = 0, 0, 0

	casterSkill =
		( PaladinSkills >= minSkill 
		or SorceressSkills >= minSkill 
		or DruidSkills >= minSkill
		or AssassinSkills >= minSkill
		or NecromancerSkills >= minSkill
		or AmazonSkills >= minSkill
		or BarbarianSkills >= minSkill

		or PaladinCombatTab >= minTab
		or LightningTab >= minTab
		--or OffensiveAurasTab >= minTab
		--or DefensiveAurasTab >= minTab
		or FireTab >= minTab
		or ColdTab >= minTab
		--or CursesTab >= minTab
		or PoisonAndBoneTab >= minTab
		or NecroSummoningTab >= minTab
		or WarcriesTab >= minTab
		or ElementalTab >= minTab
		--or DruidSummoningTab >= minTab
		or TrapsTab >= minTab
		or BarbarianCombatTab >= minTab
		or MasteriesTab >= minTab
		or JavelinAndSpearTab >= minTab
		)

	meleeSkill =
		( BarbarianSkills >= minSkill
		or AmazonSkills >= minSkill
		or PaladinSkills >= minSkill
		or DruidSkills >= minSkill
		or AssassinSkills >= minSkill

		or PaladinCombatTab >= minTab
		or BowAndCrossBowTab >= minTab
		--or PassiveAndMagicTab >= minTab
		or JavelinAndSpearTab >= minTab
		or BarbarianCombatTab >= minTab
		--or MasteriesTab >= minTab
		or ShapeShiftingTab >= minTab
		or ShadowDisciplinesTab >= minTab
		or MartialArtsTab >= minTab
		)
			
	fcr = FasterCastRate >= 20
	
	frw = FasterMoveVelocity >= 30
			 
	addStats = (MaxLife + MaxMana + Strength + Dexterity + Energy)
				
	goodMods =
		count{
			Sockets == 2,
			TripleResist >= 50,
			DoubleResist >= 60,
			AllResist >= 12,
		}
		
	statMods =
		count{
			MaxLife >= 31,
			MaxMana >= 41,
			MaxLife >= 16 and MaxMana >= 16,
			Strength >= 16,
			Dexterity >= 10 and Energy >= 10,
		}

	meleeMods =
		count{
			ToHitPerLevel ~= 0,
			ToHit >= 81,
			MaxDamagePercent >= 21,
			--LifeDrainMinDamage >= 5 and ManaDrainMinDamage >= 5,
			(LifeDrainMinDamage + ManaDrainMinDamage) >= 9,
			FasterHitRecovery >= 10,
			MaxDamage >= 6 and MinDamage >= 4,
		}
	
	return
		( Unidentified
			or fcr and casterSkill 
				and (goodMods + statMods) >= 2
			or frw and meleeSkill 
				and (goodMods + meleeMods + statMods) >= 2
			--or (casterSkill or meleeSkill)
				--and addStats >= 53
			--or SkillTabBonus >= 2
			--or ClassSkillsBonus >= 2
		)
end

rare.circlets =
{ checkStats = true,
	["Circlet"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareCirclet
		}
}
--[[
prefix
ar 101-120
edam 21-30
mana 61-90
sres 31-40
ares 16-20
cskill 2
tskill 2
soc 2

suffix
str 21-30
dex 16-20
eng 16-20
life 41-60
ll 6-9
ml 6-9
fcr 20
rw 30
]]