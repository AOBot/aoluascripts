-- expansion\rare\boots.lua "moderate"

local isGoodRareBoots =
function(item)
	if Ethereal and Identified and ReplenishDurability == 0 then
		return false -- not keeping eth unless repairs
	end
	local frw, fhr, prefix, suffix = false, false, 0, 0
	
	frw = FasterMoveVelocity >= 20
	fhr = FasterHitRecovery >= 10
	prefix =
		count{
			DefensePercent >= 81,
			ArmorPerLevel ~= 0,
			MaxMana >= 31,
		}
	suffix =
		count{
			Dexterity >= 7,
			LifeRegen >= 4,
			MagicFind >= 16,
			GoldFind >= 61,
		}
	return
		( Unidentified
			or (not Ethereal and
				TripleResist >= 100 and fhr
				or TripleResist >= 55 and frw and fhr 
					--and suffix == 1
				or DoubleResist >= 65 and frw and fhr 
					--and (prefix + suffix) >= 2
				or MagicFind >= 16 and GoldFind >= 61 and frw 
					and (TripleResist >= 55 or DoubleResist >= 65)
				)
			or (Ethereal and
				TripleResist >= 100 and fhr
				or TripleResist >= 55 and frw and fhr
				or DoubleResist >= 65 and frw and fhr
				)
		)
end

rare.boots =
{ checkStats = true,
	["Boots"] =
		{ priority = 4, identify = true,
			isGoodItem = isGoodRareBoots
		}
}

--[[
prefix
defperlevel ?
mana 31-40
sres 31-40

suffix
dex 6-9
gold 41-80
mf 16-25
frw 30
]]