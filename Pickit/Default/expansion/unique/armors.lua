-- expansion\unique\armors.lua "moderate"

unique.armors =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- ** Normal Armors ** --
-----------------------------------------------------
--[[ Greyform (Quilted Armor)
	["qui"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and TotalDefense >= 30 --note: 28-31 total def
				    or not Ethereal
						and TotalDefense >= 30 --note: 28-31 total def
				)
		},

--]]
-----------------------------------------------------
--[[ Blinkbat's Form (Leather Armor)
	["lea"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and TotalDefense >= 41 --note: 39-42 total def
				    or not Ethereal
						and TotalDefense >= 41 --note: 39-42 total def
				)
		},

--]]
-----------------------------------------------------
--[[ The Centurion (Hard Leather Armor)
	["hla"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and TotalDefense >= 51 --note: 51-54 total def
				    or not Ethereal
						and TotalDefense >= 53 --note: 51-54 total def
				)
		},

--]]
-----------------------------------------------------
--[[ Twitchthroe (Studded Leather)
	["stu"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and TotalDefense >= 57 --note: 57-60 total def
				    or not Ethereal
						and TotalDefense >= 59 --note: 57-60 total def
				)
		},

--]]
-----------------------------------------------------
--[[ Darkglow (Ring Mail)
	["rng"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 70 --note: 70-100 ed
				    or not Ethereal
						and DefensePercent >= 85 --note: 70-100 ed
				)
		},

--]]
-----------------------------------------------------
--[[ Hawkmail (Scale Mail)
	["scl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 80 --note: 80-100 ed
				    or not Ethereal
						and DefensePercent >= 90 --note: 80-100 ed
				)
		},

--]]
-----------------------------------------------------
--[[ Venom Ward (Breast Plate)
	["brs"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 60 --note: 60-100 ed
				    or not Ethereal
						and DefensePercent >= 80 --note: 60-100 ed
				)
		},

--]]
-----------------------------------------------------
--[[ Sparking Mail (Chain Mail)
	["chn"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 75 --note: 75-85 ed
					    and AttackerTakesLightningDamage >= 10 --note: 10-14 ldam
				    or not Ethereal
						and DefensePercent >= 80 --note: 75-85 ed
					    and AttackerTakesLightningDamage >= 12 --note: 10-14 ldam
				)
		},

--]]
-----------------------------------------------------
--[[ Iceblink (Splint Mail)
	["spl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 70 --note: 70-80 ed
				    or not Ethereal
						and DefensePercent >= 75 --note: 70-80 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Heavenly Garb (Light Plate)
	["ltp"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},
--]]
-----------------------------------------------------
--[[ Boneflesh (Plate Mail)
	["plt"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 100 --note: 100-120 ed
				    or not Ethereal
						and DefensePercent >= 110 --note: 100-120 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Rockfleece (Field Plate)
	["fld"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 100 --note: 100-130 ed
				    or not Ethereal
						and DefensePercent >= 120 --note: 100-130 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Rattlecage (Gothic Plate)
	["gth"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and TotalDefense >= 328 --note: 328-335 total def
				    or not Ethereal
						and TotalDefense >= 331 --note: 328-335 total def
				)
		},
--]]
-----------------------------------------------------
--[[ Goldskin (Full Plate Mail)
	["ful"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 120 --note: 120-150 ed
				    or not Ethereal
						and DefensePercent >= 135 --note: 120-150 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Silks of the Victor (Ancient Armor)
	["aar"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	and DefensePercent >= 100 --note: 100-120 ed
				    or not Ethereal
						and DefensePercent >= 110 --note: 100-120 ed
				)
		},
	
--]]
-----------------------------------------------------
-- ** Exceptional Armors ** --
-----------------------------------------------------
--[[ The Spirit Shroud (Ghost Armor)
	["xui"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MagicDamageReduction >= 7 --note: 7-11 magic dam redu
				    or not Ethereal
						and MagicDamageReduction >= 9 --note: 7-11 magic dam redu
				)
		},
--]]
-----------------------------------------------------
---[[ Skin of the Vipermagi (Serpentskin Armor)
	["xea"] =
		{ 
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    --or Ethereal
				    	--and AllResist >= 20 --note: 20-35 all res
						--and MagicDamageReduction >= 9 --note: 9-13 mdr
				    or not Ethereal
						and AllResist >= 30 --note: 20-35 all res
						--and MagicDamageReduction >= 9 --note: 9-13 mdr
				)
		},
--]]
-----------------------------------------------------
--[[ Skin of the Flayed One (Demonhide Armor) (Repairs 1 Durability In 10 Seconds)
	["xla"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				        --and DefensePercent >= 150 --note: 150-190 ed
				        --and LifeDrainMinDamage >= 5 --note: 5-7 lifestolen
				        --and LifeRegen >= 15 --note: 15-25 replen life
					--or not Ethereal
						--and DefensePercent >= 150 --note: 150-190 ed
				        --and LifeDrainMinDamage >= 7 --note: 5-7 lifestolen
				        --and LifeRegen >= 20 --note: 15-25 replen life
				)
		},
--]]
-----------------------------------------------------
--[[ Iron Pelt (Trellised Armor)
	["xtu"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and DefensePercent >= 50 --note: 50-100 ed
					    --and DamageReduction >= 15 --note: 15-20 dam redu
					    --and MagicDamageReduction >= 10 --note: 10-16 mdr
				    or not Ethereal
						--and DefensePercent >= 50 --note: 50-100 ed
					    and DamageReduction >= 18 --note: 15-20 dam redu
					    and MagicDamageReduction >= 13 --note: 10-16 mdr
				)
		},
--]]
-----------------------------------------------------
--[[ Crow Caw (Tigulated Mail)
	["xcl"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and DefensePercent >= 150 --note: 150-180 ed
				    --or not Ethereal
						--and DefensePercent >= 165 --note: 150-180 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Spirit Forge (Linked Mail)
	["xng"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and DefensePercent >= 120 --note: 120-160 ed
				    --or not Ethereal
						--and DefensePercent >= 140 --note: 120-160 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Duriel's Shell (Cuirass)
	["xrs"] =
		{
			priority = 4, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
						--and DefensePercent >= 160 --note: 160-200 ed
				    --or not Ethereal
						--and DefensePercent >= 180 --note: 160-200 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Shaftstop (Mesh Armor)
	["xhn"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and DefensePercent >= 180 --note: 180-220 ed
					or not Ethereal
						and DefensePercent >= 210 --note: 180-220 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Skullder's Ire (Russet Armor) (Repairs 1 Durability In 5 Seconds)
	["xpl"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and DefensePercent >= 160 --note: 160-200 ed
					or not Ethereal
						and DefensePercent >= 190 --note: 160-200 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Que-Hegan's Wisdom (Mage Plate)
	["xtp"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and DefensePercent >= 140 --note: 140-160 ed
					    --and MagicDamageReduction >= 6 --note: 6-10 mdr
				    or not Ethereal
						--and DefensePercent >= 140 --note: 140-160 ed
					    and MagicDamageReduction >= 10 --note: 6-10 mdr
				)
		},
--]]
-----------------------------------------------------
---[[ Guardian Angel (Templar Coat)
	["xlt"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and DefensePercent >= 180 --note: 180-200 ed
					--or not Ethereal
						--and DefensePercent >= 190 --note: 180-200 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Toothrow (Sharktooth Armor)
	["xld"] =
		{
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and DefensePercent >= 160 --note: 160-220 ed
					--or not Ethereal
						--and DefensePercent >= 190 --note: 160-220 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Atma's Wail (Embossed Plate)
	["xth"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and DefensePercent >= 120 --note: 120-160 ed
					or not Ethereal
						and DefensePercent >= 140 --note: 120-160 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Black Hades (Chaos Armor)
	["xul"] =
		{
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 140 --note: 140-200 ed
					    --and DemonDamagePercent >= 30 --note: 30-60 dmg to demon
					    --and DemonToHit >= 200 --note: 200-250 AR to demon
					or not Ethereal
						--and DefensePercent >= 140 --note: 140-200 ed
					    and DemonDamagePercent >= 55 --note: 30-60 dmg to demon
					    --and DemonToHit >= 200 --note: 200-250 AR to demon
				)
		},
--]]
-----------------------------------------------------
--[[ Corpsemourn (Ornate Plate)
	["xar"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and DefensePercent >= 150 --note: 150-180 ed
					or not Ethereal
						and DefensePercent >= 165 --note: 150-180 ed
				)
		},
--]]
-----------------------------------------------------
-- ** Elite Armors ** --
-----------------------------------------------------
---[[ Ormus' Robes (Dusk Shroud)
	["uui"] =
		{ -------------- see bottom of file
			priority = 6, identify = true,
			isGoodItem =
				function(item)
					return (Unidentified or isGoodOrmus(item))
				end
		},
--]]
-----------------------------------------------------
--[[ The Gladiator's Bane (Wire Fleece)
	["utu"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 150 --note: 150-200 ed
					    --and DamageReduction >= 15 --note: 15-20 dam redu
					    --and MagicDamageReduction >= 15 --note: 15-20 mdr
					or not Ethereal
						--and DefensePercent >= 150 --note: 150-200 ed
					    and DamageReduction >= 17 --note: 15-20 dam redu
					    and MagicDamageReduction >= 17 --note: 15-20 mdr
				)
		},
--]]
-----------------------------------------------------
---[[ Arkaine's Valor (Balrog Skin)
	["upl"] =
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
						--and AllSkillsBonus >= 1 --note: 1-2 all skill
						--and DefensePercent >= 150 --note: 150-180 ed
						--and DamageReduction >= 10 --note: 10-15 dam redu
				    or not Ethereal
						--and AllSkillsBonus >= 1 --note: 1-2 all skill
						--and DefensePercent >= 150 --note: 150-180 ed
						--and DamageReduction >= 10 --note: 10-15 dam redu
				)
		},
--]]
-----------------------------------------------------
---[[ Leviathan (Kraken Shell) (Indestructible)
	["uld"] =
		{
			priority = 7, identify = false,
			goodItem =
				( Unidentified
					or DamageResist >= 15 --note: 15-25 dam redu
						--and DefensePercent >= 170 --note: 170-200 ed
						--and Strength >= 40 --note: 40-50 str
						--and ArmorClass >= 100 --note: 100-150 to def
				)
		},
--]]
-----------------------------------------------------
---[[ Steel Carapace (Shadow Plate) (Repairs 1 Durability in 20 Seconds)
	["uul"] =
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 190 --note: 190-220 ed
					    --and DamageReduction >= 9 --note: 9-14 dam redu
					    --and ColdResist >= 40 --note: 40-60 cold res
					    --and ManaRecoveryBonus >= 10 --note: 10-15 regen mana
					--or not Ethereal
						--and DefensePercent >= 190 --note: 190-220 ed
					    --and DamageReduction >= 11 --note: 9-14 dam redu
					    --and ColdResist >= 40 --note: 40-60 cold res
					    --and ManaRecoveryBonus >= 10 --note: 10-15 regen mana
				)
		},
--]]
-----------------------------------------------------
---[[ Templar's Might or Tyrael's Might (Sacred Armor) (Tyrael's is Indestructible)
	["uar"] =
		{
			priority = 9, identify = false,
			goodItem =
				( Unidentified
				    or UniqueIndex == 366 and Ethereal -- Templar's Might
						--and DefensePercent >= 170 --note: 170-220 ed
					    --and ArmorClassVsMissile >= 250 --note: 250-300 dvm
					    --and Strength >= 10 --note: 10-15 str
					    --and Vitality >= 10 --note: 10-15 vit
					    --and MaxStamina >= 40 --note: 40-50 max stamina
					    --and OffensiveAurasTab >= 1 --note: 1-2 OffensiveAura
					or UniqueIndex == 366 and not Ethereal -- Templar's Might
						--and DefensePercent >= 170 --note: 170-220 ed
					    --and ArmorClassVsMissile >= 250 --note: 250-300 dvm
					    --and Strength >= 10 --note: 10-15 str
					    --and Vitality >= 10 --note: 10-15 vit
					    --and MaxStamina >= 40 --note: 40-50 max stamina
					    --and OffensiveAurasTab >= 1 --note: 1-2 OffensiveAura
					    
					or UniqueIndex == 311 -- Tyrael's Might
					    --and DefensePercent >= 120 --note: 120-150 ed
					    --and DemonDamagePercent >= 50 --note: 50-100 dmg to demon
					    --and Strength >= 20 --note: 20-30 str
					    --and AllResist >= 20 --note: 20-30 all res
				)
		},
--]]
-----------------------------------------------------
}

function isGoodOrmus(item)
	local minM = 13 --note: 10-15 mastery
	local fSkill, cSkill, lSkill = false, false, false
	local anyM, totaldef = false, false

	fSkill =
		( FireBallSkill > 0
			or MeteorSkill > 0
			or EnchantSkill > 0
			--or FireBoltSkill > 0
			--or WarmthSkill > 0
			--or InfernoSkill > 0
			--or BlazeSkill > 0
			--or FireWallSkill > 0
		)

	cSkill =
		( BlizzardSkill > 0
			--or IceBoltSkill > 0
			--or FrozenArmorSkill > 0
			--or FrostNovaSkill > 0
			--or IceBlastSkill > 0
			--or ShiverArmorSkill > 0
			--or GlacialSpikeSkill > 0
			--or ChillingArmorSkill > 0
		)

	lSkill =
		( LightningSkill > 0
			or ChainLightningSkill > 0
			--or ChargedBoltSkill > 0
			--or StaticFieldSkill > 0
			--or TelekinesisSkill > 0
			--or NovaSkill > 0
			--or ThunderStormSkill > 0
		)

	anyM =
		( PassiveFireMastery >= minM
			or PassiveColdMastery >= minM
			or PassiveLightningMastery >= minM
		)
		
	totaldef =
	    ( TotalDefense >= 371 --note: 371-487 total def

		)

	return
		(
			fSkill
				and PassiveFireMastery >= minM
				--and totaldef
		--note: returns for fire ormus
			or cSkill
				and PassiveColdMastery >= minM
				--and totaldef
		--note: returns for cold ormus
			or lSkill
				and PassiveLightningMastery >= minM
				--and totaldef
		--note: returns for light ormus
			or EnergyShieldSkill > 0
				and anyM
		--note: returns for es ormus
			--or TeleportSkill > 0
				--and anyM
		--note: returns for tele ormus
		)
end