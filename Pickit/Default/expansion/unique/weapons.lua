-- expansion\unique\weapons.lua "moderate"

unique.weapons =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- Axes ---------------------------------------------
-----------------------------------------------------
-- ** Normal Axes ** --

--[[ The Gnasher (Hand Axe)
	["hax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 60 --note: 60-70 edam
					or not Ethereal
						and MaxDamagePercent >= 60 --note: 60-70 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Deathspade (Axe)
	["axe"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 60 --note: 60-70 edam
					or not Ethereal
						and MaxDamagePercent >= 60 --note: 60-70 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Bladebone (Double Axe)
	["2ax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 30 --note: 30-50 edam
					or not Ethereal
						and MaxDamagePercent >= 30 --note: 30-50 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Skull Splitter (Military Pick)
	["mpi"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 60 --note: 60-100 edam
					    and LightMaxDamage >= 12 --note: 12-15 light dam
					    and ToHit >= 50 --note: 50-100 AR
					or not Ethereal
						and MaxDamagePercent >= 60 --note: 60-100 edam
					    and LightMaxDamage >= 12 --note: 12-15 light dam
					    and ToHit >= 50 --note: 50-100 AR
				)
		},
--]]
-----------------------------------------------------
--[[ Rakescar (War Axe)
	["wax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 75 --note: 75-150 edam
					or not Ethereal
						and MaxDamagePercent >= 75 --note: 75-150 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Axe of Fechmar (Large Axe)
	["lax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 70 --note: 70-90 edam
					or not Ethereal
						and MaxDamagePercent >= 70 --note: 70-90 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Goreshovel (Broad Axe)
	["bax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 40 --note: 40-50 edam
					or not Ethereal
						and MaxDamagePercent >= 40 --note: 40-50 edam
				)
		},
--]]
-----------------------------------------------------
--[[ The Chieftain (Battle Axe)
	["btx"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and AllResist >= 10 --note: 10-20 all res
					or not Ethereal
						and AllResist >= 10 --note: 10-20 all res
				)
		},
--]]
-----------------------------------------------------
--[[ Brainhew (Great Axe)
	["gax"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 50 --note: 50-80 edam
					    and ManaDrainMinDamage >= 10 --note: 10-13 manastolen
					or not Ethereal
						and MaxDamagePercent >= 50 --note: 50-80 edam
					    and ManaDrainMinDamage >= 10 --note: 10-13 manastolen
				)
		},
--]]
-----------------------------------------------------
--[[ Humongous (Giant Axe)
	["gix"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 80 --note: 80-120 edam
					    and Strength >= 20 --note: 20-30 str
					or not Ethereal
						and MaxDamagePercent >= 80 --note: 80-120 edam
					    and Strength >= 20 --note: 20-30 str
				)
		},
--]]
-----------------------------------------------------
-- ** Exceptional Axes ** --
-----------------------------------------------------
--[[ Coldkill (Hatchet)
	["9ha"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 150 --note: 150-190 edam
					or not Ethereal
						and MaxDamagePercent >= 150 --note: 150-190 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Butcher's Pupil (Cleaver) (Indestructible)
	["9ax"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 150 --note: 150-200 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Islestrike (Twin Axe)
	["92a"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 170 --note: 170-190 edam
					or not Ethereal
						and MaxDamagePercent >= 170 --note: 170-190 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Pompeii's Wrath (Crowbill) 
	["9mp"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 140 --note: 140-170 edam
					or not Ethereal
						and MaxDamagePercent >= 140 --note: 140-170 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Guardian Naga (Naga)
	["9wa"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 150 --note: 150-180 edam
					or not Ethereal
						and MaxDamagePercent >= 150 --note: 150-180 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Warlord's Trust (Military Axe) (Repairs 1 Durability In 4 Seconds)
	["9la"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
					--or not Ethereal
			--note: all the same, unless ethereal
				)
		},
--]]
-----------------------------------------------------
--[[ Spellsteel (Bearded Axe) 
	["9ba"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MagicDamageReduction >= 12 --note: 12-15 magic redu
					or not Ethereal
						and MagicDamageReduction >= 12 --note: 12-15 magic redu
				)
		},
--]]
-----------------------------------------------------
--[[ Stormrider (Tabar)
	["9bt"] =
		{
			priority = 3, identify = true,
			goodItem =
			    ( Unidentified
			    	or Ethereal
			    		and SkillOnStriking >= 13 --note: 13-20 ChargedBolt
					or not Ethereal
			        	and SkillOnStriking >= 13 --note: 13-20 ChargedBolt
				)
		},
	
--]]
-----------------------------------------------------
--[[ Boneslayer Blade (Gothic Axe)
	["9ga"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 180 --note: 180-220 edam
					    and SkillOnGetHit >= 12 --note: 12-28 HolyBolt
					or not Ethereal
						and MaxDamagePercent >= 180 --note: 180-220 edam
					    and SkillOnGetHit >= 12 --note: 12-28 HolyBolt
				)
		},
--]]
-----------------------------------------------------
--[[ The Minotaur (Ancient Axe)
	["9gi"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 140 --note: 140-200 edam
					    and Strength >= 15 --note: 15-20 str
					or not Ethereal
						and MaxDamagePercent >= 140 --note: 140-200 edam
					    and Strength >= 15 --note: 15-20 str
				)
		},
--]]
-----------------------------------------------------
-- ** Elite Axes ** --
-----------------------------------------------------
--[[ Razor's Edge (Tomahawk)
	["7ha"] =  
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and MaxDamagePercent >= 175 --note: 175-225 edam
					or not Ethereal
						and MaxDamagePercent >= 200 --note: 175-225 edam
				)
		},
--]]
-----------------------------------------------------
---[[ Rune Master (Ettin Axe)
	["72a"] = 
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and Sockets >= 3 --note: 3-5 sock
						--and MaxDamagePercent >= 220 --note: 220-270 edam
					or not Ethereal
						and Sockets >= 5 --note: 3-5 sock
						--and MaxDamagePercent >= 245 --note: 220-270 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Cranebeak (War Spike)
	["7mp"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and MaxDamagePercent >= 240 --note: 240-300 edam
						--and MagicFind >= 20 --note: 20-50 mf
					or not Ethereal
						and MaxDamagePercent >= 270 --note: 240-300 edam
						--and MagicFind >= 35 --note: 20-50 mf
				)
		},
--]]
-----------------------------------------------------
---[[ Death Cleaver (Berserker Axe) 
	["7wa"] =  
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and MaxDamagePercent >= 230 --note: 230-280 edam
						--and HealAfterKill >= 6 --note: 6-9 life after kill
					or not Ethereal
						and MaxDamagePercent >= 255 --note: 230-280 edam
						--and HealAfterKill >= 8 --note: 6-9 life after kill
				)
		},
--]]
-----------------------------------------------------
--[[ Ethereal Edge (Silver-Edged Axe) (Indestructible) (Ethereal)
	["7ba"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 165 --note: 150-180 edam
						and DemonDamagePercent >= 175 -- 150-200 dmg to demon
						--and ToHit >= 270 --note: 270-350 AR
						--and AbsorbFire >= 10 --note: 10-12 fire absorb
						--and HealAfterDemonKill >= 5 --note: 5-10 life after demon
				)
		},
--]]
-----------------------------------------------------
--[[ Hellslayer (Decapitator)
	["7bt"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and SkillOnAttack >= 16 --note: 16-20 fireball on attk
					or not Ethereal
						and SkillOnAttack >= 20 --note: 16-20 fireball on attk
				)
		},
--]]
-----------------------------------------------------
--[[ Messerschmidt's Reaver (Champion Axe)
	["7ga"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					--or not Ethereal
			--note: all the same, unless ethereal
				)
		},
--]]
-----------------------------------------------------
--[[ Executioner's Justice (Glorious Axe) 
	["7gi"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and MaxDamagePercent >= 240 --note: 240-290 edam
					or not Ethereal
						and MaxDamagePercent >= 265 --note: 240-290 edam
				)
		},
--]]
 
-----------------------------------------------------
-- Bows ---------------------------------------------
-----------------------------------------------------
-- ** Normal Bows ** --

--[[ Pluckeye (Short Bow)
	["sbw"] = 
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Witherstring (Hunter's Bow)
	["hbw"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 40 --note: 40-50 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Raven Claw (Long Bow)
	["lbw"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 60 --note: 60-70 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Rogue's Bow (Composite Bow) 
	["cbw"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 40 --note: 40-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Stormstrike (Short Battle Bow) 
	["sbb"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 70 --note: 70-90 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Wizendraw (Long Battle Bow)
	["lbb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 70 --note: 70-80 edam
						and ToHit >= 50 --note: 50-100 AR
						and PassiveColdPierce >= 20 --note: 20-35 -cold res
				)
		},	
--]]
-----------------------------------------------------
--[[ Hellclap (Short War Bow)
	["swb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 70 --note: 70-90 edam
						and FireMaxDamage >= 30 --note: 30-50 max fire dam
						and ToHit >= 50 --note: 50-75 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Blastbark (Long War Bow)
	["lwb"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 70 --note: 70-130 edam
				)
		},	
--]]
----------------------------------------------------- 
-- ** Exceptional Bows ** --
-----------------------------------------------------
--[[ Skystrike (Edge Bow)
	["8sb"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 150 --note: 150-200 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Riphook (Razor Bow) 
	["8hb"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 180 --note: 180-220 edam
						and LifeDrainMinDamage >= 7 --note: 7-10 lifestolen
				)
		},	
--]]
-----------------------------------------------------
--[[ Kuko Shakaku (Cedar Bow)
	["8lb"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 150 --note: 150-180 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Endlesshail (Double Bow)
	["8cb"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 180 --note: 180-220 edam
						and StrafeSkill >= 3 --note: 3-5 Strafe
				)
		},	
--]]
-----------------------------------------------------
--[[ Witchwild String (Short Siege Bow)
	["8s8"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 150 --note: 150-170 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Cliffkiller (Large Siege Bow)
	["8l8"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 190 --note: 190-230 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Magewrath (Rune Bow)
	["8sw"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 120 --note: 120-150 edam
						and ToHit >= 200 --note: 200-250 AR
						and MagicDamageReduction >= 9 --note: 9-13 magic redu
				)
		},	
--]]
-----------------------------------------------------
--[[ Goldstrike Arch (Gothic Bow)
	["8lw"] =  
		{
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 200 --note: 200-250 edam
						and DemonDamagePercent >= 100 --note: 100-200 dmg to demon
						and UndeadDamagepercent >= 100 --note: 100-200 dmg to undead
						and ToHitPercent >= 100 --note: 100-150 AR
				)
		},	
--]]
-----------------------------------------------------
-- ** Elite Bows ** --
-----------------------------------------------------
--[[ Eaglehorn (Crusader Bow)
	["6l7"] =  
		{
			priority = 5, identify = true,
			--note: all the same
		},
--]]
-----------------------------------------------------
---[[ Widowmaker (Ward Bow) 
	["6sw"] = 
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or GuidedArrowSkill >= 5 --note: 3-5 GuidedArrow
						and MaxDamagePercent >= 175 --note: 150-200 edam
				)
		},
--]]
-----------------------------------------------------
---[[ Windforce (Hydra Bow)
	["6lw"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or ManaDrainMinDamage >= 6 --note: 6-8 manastolen
				)
		},
--]]

-----------------------------------------------------
-- Crossbows ----------------------------------------
-----------------------------------------------------
-- ** Normal Crossbows ** --

--[[ Leadcrow (Light Crossbow)
	["lxb"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Ichorsting (Crossbow) 
	["mxb"] =  
		{
			priority = 2, identify = true,
			--note: all the same
		},	
--]]
-----------------------------------------------------
--[[ Hellcast (Heavy Crossbow)
	["hxb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 70 --note: 70-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Doomslinger (Repeating Crossbow)
	["rxb"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 60 --note: 60-100 edam
				)
		},	
--]]
-----------------------------------------------------
-- ** Exceptional Crossbows ** --
-----------------------------------------------------
--[[ Langer Briser (Arbalest)
	["8lx"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 170 --note: 170-200 edam
						and MagicFind >= 30 --note: 30-60 mf
				)
		},	
--]]
-----------------------------------------------------
--[[ Pus Spitter (Siege Crossbow) 
	["8mx"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 150 --note: 150-220 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Buriza-Do Kyanon (Ballista) 
	["8hx"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 195 --note: 150-200 edam
						--and ArmorClass >= 75 --note: 75-150 armor def
				)
		},	
--]]
-----------------------------------------------------
--[[ Demon Machine (Chu-Ko-Nu)
	["8rx"] = 
		{
			priority = 3, identify = true,
			--note: all the same
		},	
--]]
----------------------------------------------------- 
-- ** Elite Crossbows ** --
-----------------------------------------------------
--[[ Hellrack (Colossus Crossbow)
	["6hx"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 180 --note: 180-230 edam
					    --and ToHitPercent >= 100 --note: 100-150 AR
				)
		},
--]]
-----------------------------------------------------
--[[ Gut Siphon (Demon Crossbow)
	["6rx"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 160 --note: 160-220 edam
					    --and LifeDrainMinDamage >= 12 --note: 12-18 lifestolen
				)
		},
--]]
 
-----------------------------------------------------
-- Daggers ------------------------------------------
-----------------------------------------------------
-- ** Normal Daggers ** --

--[[ Gull (Dagger)
	["dgr"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ The Diggler (Dirk)
	["dir"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ The Jade Tan do (Kris)
	["kri"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and ToHit >= 100 --note: 100-150 AR
					or not Ethereal
						and ToHit >= 100 --note: 100-150 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Spectral Shard (Blade)
	["bld"] = 
		{
			priority = 2, identify = true,
			--note: all the same, unless ethereal
		},	
--]]
----------------------------------------------------- 
-- ** Exceptional Daggers ** --
-----------------------------------------------------
--[[ Spineripper (Poignard)
	["9dg"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 200 --note: 200-240 edam
					or not Ethereal
						and MaxDamagePercent >= 200 --note: 200-240 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Heart Carver (Rondel)
	["9di"] = 
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 190 --note: 190-240 edam
					or not Ethereal
						and MaxDamagePercent >= 190 --note: 190-240 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Blackbog's Sharp (Cinquedeas)
	["9kr"] =  
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Stormspike (Stiletto)
	["9bl"] =  
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
----------------------------------------------------- 
-- ** Elite Daggers ** --
-----------------------------------------------------
--[[ Wizardspike (Bone Knife) (Indestructible)
	["7dg"] =
		{
			priority = 5, identify = true,
			--note: all the same
		},
--]]
-----------------------------------------------------
---[[ Fleshripper (Fanged Knife)
	["7kr"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and MaxDamagePercent >= 200 --note: 200-300 edam
					or not Ethereal
						and MaxDamagePercent >= 275 --note: 200-300 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Ghostflame (Legend Spike) (Indestructible) (Ethereal)
	["7bl"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 210 --note: 190-240 edam
						and ManaDrainMinDamage >= 14 --note: 10-15 manastolen
				)
		},
--]] 

-----------------------------------------------------
-- Maces --------------------------------------------
-----------------------------------------------------
-- ** Normal Maces ** --

--[[ Felloak (Club)
	["clb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 70 --note: 70-80 edam
					or not Ethereal
						and MaxDamagePercent >= 70 --note: 70-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Stoutnail (Spiked Club)
	["spc"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and AttackerTakesDamage >= 3 --note: 3-10 dam to enemy
					or not Ethereal
						and AttackerTakesDamage >= 3 --note: 3-10 dam to enemy
				)
		},	
--]]
-----------------------------------------------------
--[[ Crushflange (Mace)
	["mac"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 50 --note: 50-60 edam
					or not Ethereal
						and MaxDamagePercent >= 50 --note: 50-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Bloodrise (Morning Star)
	["mst"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ The General's Tan Do Li Ga (Flail) 
	["fla"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 50 --note: 50-60 edam
					or not Ethereal
						and MaxDamagePercent >= 50 --note: 50-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Ironstone (War Hammer)
	["whm"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 100 --note: 100-150 edam
						and ToHit >= 100 --note: 100-150 AR
					or not Ethereal
						and MaxDamagePercent >= 100 --note: 100-150 edam
						and ToHit >= 100 --note: 100-150 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Bonesnap (Maul)
	["mau"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 200 --note: 200-300 edam
					or not Ethereal
						and MaxDamagePercent >= 200 --note: 200-300 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Steeldriver (Great Maul)
	["gma"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 150 --note: 150-250 edam
					or not Ethereal
						and MaxDamagePercent >= 150 --note: 150-250 edam
				)
		},	
--]]
----------------------------------------------------- 
-- ** Exceptional Maces ** --
-----------------------------------------------------
--[[ Dark Clan Crusher (Cudgel)
	["9cl"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and ToHitPercent >= 20 --note: 20-25 AR
					or not Ethereal
						and ToHitPercent >= 20 --note: 20-25 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Fleshrender (Barbed Club)
	["9sp"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 130 --note: 130-200 edam
					or not Ethereal
					    and MaxDamagePercent >= 130 --note: 130-200 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Sureshrill Frost (Flanged Mace) 
	["9ma"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 150 --note: 150-180 edam
					or not Ethereal
					    and MaxDamagePercent >= 150 --note: 150-180 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Moonfall (Jagged Star)
	["9mt"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 120 --note: 120-150 edam
					    and MagicDamageReduction >= 9 --note: 9-12 magic redu
					or not Ethereal
					    and MaxDamagePercent >= 120 --note: 120-150 edam
					    and MagicDamageReduction >= 9 --note: 9-12 magic redu
				)
		},	
--]]
-----------------------------------------------------
--[[ Baezil's Vortex (Knout)
	["9fl"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 160 --note: 160-200 edam
					or not Ethereal
					    and MaxDamagePercent >= 160 --note: 160-200 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Earthshaker (Battle Hammer)
	["9wh"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Bloodtree Stump (War Club)
	["9m9"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 180 --note: 180-220 edam
					or not Ethereal
					    and MaxDamagePercent >= 180 --note: 180-220 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Gavel Of Pain (Martel de Fer) (Indestructible)
	["9gm"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 130 --note: 130-160 edam
				)
		},	
--]]
----------------------------------------------------- 
-- ** Elite Maces ** --
-----------------------------------------------------
--[[ Nord's Tenderizer (Truncheon)
	["7cl"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MaxDamagePercent >= 270 --note: 270-330 edam
						--and Freeze >= 2 --note: 2-4 freeze target
						--and AbsorbColdPercent >= 5 --note: 5-15 cold absorb
						--and ToHitPercent >= 150 --note: 150-180 AR
				    or not Ethereal
						and MaxDamagePercent >= 300 --note: 270-330 edam
						--and Freeze >= 2 --note: 2-4 freeze target
						and AbsorbColdPercent >= 12 --note: 5-15 cold absorb
						--and ToHitPercent >= 150 --note: 150-180 AR
				)
		},
--]]
-----------------------------------------------------
---[[ Demon Limb (Tyrant Club) (Repairs 1 Durability in 20 Seconds)
	["7sp"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
						--and MaxDamagePercent >= 180 --note: 180-230 edam
						--and LifeDrainMinDamage >= 7 --note: 7-13 lifestolen
						--and FireResist >= 15 --note: 15-20 fire res
				    --or not Ethereal
						--and MaxDamagePercent >= 210 --note: 180-230 edam
						--and LifeDrainMinDamage >= 11 --note: 7-13 lifestolen
						--and FireResist >= 15 --note: 15-20 fire res
				)
		},
--]]
-----------------------------------------------------
--[[ Baranar's Star (Devil Star)
	["7mt"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    --or not Ethereal
			--note: all the same, unless ethereal
				)
		},
--]]
-----------------------------------------------------
---[[ Horizon's Tornado or Stormlash (Scourge) 
	["7fl"] = 
		{
			priority = 8, identify = true,
			goodItem =
				( Unidentified
					or UniqueIndex == 360 and Ethereal -- Stormlash
					    --and MaxDamagePercent >= 240 --note: 240-300 edam
					    --and AbsorbLight >= 3 --note: 3-9 light absorb
					or UniqueIndex == 360 and not Ethereal -- Stormlash
					    and MaxDamagePercent >= 260 --note: 240-300 edam
					    --and AbsorbLight >= 3 --note: 3-9 light absorb
					    
					--or UniqueIndex == 306 and Ethereal-- Horizon's Tornado
					    --and MaxDamagePercent >= 230 --note: 230-280 edam
					--or UniqueIndex == 306 and not Ethereal-- Horizon's Tornado
					   -- and MaxDamagePercent >= 255 --note: 230-280 edam
				)
		},
--]]
-----------------------------------------------------
---[[ Stone Crusher or Schaefer's Hammer (Legendary Mallet) (SH is Indestructible)
	["7wh"] =
		{
			priority = 8, identify = true,
			goodItem =
				( Unidentified
					--or UniqueIndex == 307 and Ethereal-- Stone Crusher
					    --and MaxDamagePercent >= 280 --note: 280-320 edam
					    --and Strength >= 20 --note: 20-30 str
					--or UniqueIndex == 307 and not Ethereal-- Stone Crusher
					    --and MaxDamagePercent >= 300 --note: 280-320 edam
					    --and Strength >= 20 --note: 20-30 str
					    
					or UniqueIndex == 257 -- Schaefer's Hammer
					    --and MaxDamagePercent >= 100 --note: 100-130 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Windhammer (Ogre Maul) 
	["7m7"] =
	    {
			priority = 6, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
						--and MaxDamagePercent >= 180 --note: 180-230 edam
				    or not Ethereal
					    and MaxDamagePercent >= 220 --note: 180-230 edam
				)
		},
--]]
-----------------------------------------------------
---[[ Earth Shifter or The Cranium Basher (Thunder Maul) (CB is Indestructible)
	["7gm"] =
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or UniqueIndex == 385 and Ethereal-- Earth Shifter
					    --and MaxDamagePercent >= 250 --note: 250-300 edam
					or UniqueIndex == 385 and not Ethereal-- Earth Shifter
					    --and MaxDamagePercent >= 250 --note: 250-300 edam
					    
					--or UniqueIndex == 258 -- The Cranium Basher
					    --and MaxDamagePercent >= 200 --note: 200-240 edam
				)
		},
--]]
 
-----------------------------------------------------
-- Polearms -----------------------------------------
-----------------------------------------------------
-- ** Normal Polearms ** --

--[[ Dimoak's Hew (Bardiche)
	["bar"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Steelgoad (Voulge)
	["vou"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 60 --note: 60-80 edam
				    or not Ethereal
						and MaxDamagePercent >= 60 --note: 60-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Soul Harvest (Scythe)
	["scy"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 50 --note: 50-90 edam
				    or not Ethereal
						and MaxDamagePercent >= 50 --note: 50-90 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Battlebranch (Poleaxe)
	["pax"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 50 --note: 50-70 edam
						and ToHit >= 50 --note: 50-100 AR
				    or not Ethereal
						and MaxDamagePercent >= 50 --note: 50-70 edam
						and ToHit >= 50 --note: 50-100 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Woestave (Halberd)
	["hal"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 20 --note: 20-40 edam
				    or not Ethereal
						and MaxDamagePercent >= 20 --note: 20-40 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Grim Reaper (War Scythe)
	["wsc"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
-- ** Exceptional Polearms ** --
-----------------------------------------------------
--[[ The Meat Scraper (Lochaber Axe)
	["9b7"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 150 --note: 150-200 edam
					or not Ethereal
					    and MaxDamagePercent >= 150 --note: 150-200 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Blackleach Blade (Bill)
	["9vo"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 100 --note: 100-140 edam
					or not Ethereal
					    and MaxDamagePercent >= 100 --note: 100-140 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Athena's Wrath (Battle Scythe)
	["9s8"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 150 --note: 150-180 edam
					    and ClassSkillsBonus >= 1 --note: 1-3 Druid skill
					or not Ethereal
					    and MaxDamagePercent >= 150 --note: 150-180 edam
					    and ClassSkillsBonus >= 1 --note: 1-3 Druid skill
				)
		},	
--]]
-----------------------------------------------------
--[[ Pierre Tombale Couant (Partizan)
	["9pa"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 160 --note: 160-220 edam
					    and ToHit >= 100 --note: 100-200 AR
					or not Ethereal
					    and MaxDamagePercent >= 160 --note: 160-220 edam
					    and ToHit >= 100 --note: 100-200 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Husoldal Evo (Bec-De-Corbin)
	["9h9"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 160 --note: 160-200 edam
					    and ToHit >= 200 --note: 200-250 AR
					or not Ethereal
					    and MaxDamagePercent >= 160 --note: 160-200 edam
					    and ToHit >= 200 --note: 200-250 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Grim's Burning Dead (Grim Scythe)
	["9wc"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 140 --note: 140-180 edam
					    and ToHit >= 200 --note: 200-250 AR
					or not Ethereal
					    and MaxDamagePercent >= 140 --note: 140-180 edam
					    and ToHit >= 200 --note: 200-250 AR
				)
		},	
--]]
----------------------------------------------------- 
-- ** Elite Polearms ** --
-----------------------------------------------------
---[[ Bonehew (Ogre Axe)
	["7o7"] =
	    {
			priority = 6, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				        --and MaxDamagePercent >= 270 --note: 270-320 edam
				    --or not Ethereal
					    --and MaxDamagePercent >= 295 --note: 270-320 edam

				)
		},
--]]
-----------------------------------------------------
---[[ The Reaper's Toll (Thresher) 
	["7s8"] = 
	    {
			priority = 6, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				        --and MaxDamagePercent >= 190 --note: 190-240 edam
					    --and LifeDrainMinDamage >= 11 --note: 11-15 lifestolen
				    --or not Ethereal
					    --and MaxDamagePercent >= 215 --note: 190-240 edam
					    --and LifeDrainMinDamage >= 13 --note: 11-15 lifestolen
				)
		},
--]]
-----------------------------------------------------
---[[ Tomb Reaver (Cryptic Axe)
	["7pa"] =  
	    {
			priority = 8, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				        --and MaxDamagePercent >= 200 --note: 200-280 edam
					    --and UndeadDamagepercent >= 150 --note: 150-230 dmg to undead
					    --and UndeadToHit >= 250 --note: 250-350 AR to undead
					    --and AllResist >= 30 --note: 30-50 all res
					    --and HealAfterKill >= 10 --note: 10-14 life after kill
					    --and MagicFind >= 50 --note: 50-80 mf
					    --and Sockets >= 1 --note: 1-3 sock
				    or not Ethereal
					    --and MaxDamagePercent >= 200 --note: 200-280 edam
					    --and UndeadDamagepercent >= 150 --note: 150-230 dmg to undead
					    --and UndeadToHit >= 250 --note: 250-350 AR to undead
					    --and AllResist >= 30 --note: 30-50 all res
					    --and HealAfterKill >= 10 --note: 10-14 life after kill
					    --and MagicFind >= 50 --note: 50-80 mf
					    and Sockets >= 3 --note: 1-3 sock
				)
		},
--]]
-----------------------------------------------------
--[[ Stormspire (Giant Thresher) (Indestructible)
	["7wc"] =
	    {
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 200 --note: 150-250 edam
				)
		},
--]]

-----------------------------------------------------
-- Scepters -----------------------------------------
-----------------------------------------------------
-- ** Normal Scepters ** --

--[[ Knell Striker (Scepter)
	["scp"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 70 --note: 70-80 edam
				    or not Ethereal
						and MaxDamagePercent >= 70 --note: 70-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Rusthandle (Grand Scepter)
	["gsc"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 50 --note: 50-60 edam
						and UndeadDamagepercent >= 100 -- 100-110 dmg to undead
						and VengeanceSkill >= 1 --note: 1-3 Vengeance
				    or not Ethereal
					    and MaxDamagePercent >= 50 --note: 50-60 edam
						and UndeadDamagepercent >= 100 -- 100-110 dmg to undead
						and VengeanceSkill >= 1 --note: 1-3 Vengeance
				)
		},	
--]]
-----------------------------------------------------
--[[ Stormeye (War Scepter)
	["wsp"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 80 --note: 80-120 edam
						and ResistLightningSkill >= 3 --note: 3-5 ResistLightning
				    or not Ethereal
					    and MaxDamagePercent >= 80 --note: 80-120 edam
						and ResistLightningSkill >= 3 --note: 3-5 ResistLightning
				)
		},	
--]]
-----------------------------------------------------
-- ** Exceptional Scepters ** --
-----------------------------------------------------
--[[ Zakarum's Hand (Rune Scepter)
	["9sc"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 180 --note: 180-220 edam
				    or not Ethereal
					    and MaxDamagePercent >= 180 --note: 180-220 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Fetid Sprinkler (Holy Water Sprinkler)
	["9qs"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 160 --note: 160-190 edam
						and ToHit >= 150 --note: 150-200 AR
				    or not Ethereal
					    and MaxDamagePercent >= 160 --note: 160-190 edam
						and ToHit >= 150 --note: 150-200 AR
				)
		},	
--]]
-----------------------------------------------------
--[[ Hand of Blessed Light (Divine Scepter)
	["9ws"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 130 --note: 130-160 edam
				    or not Ethereal
					    and MaxDamagePercent >= 130 --note: 130-160 edam
				)
		},	
--]]
-----------------------------------------------------
-- ** Elite Scepters ** --
-----------------------------------------------------
---[[ Heaven's Light or The Redeemer (Mighty Scepter)
	["7sc"] = 
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or UniqueIndex == 371 and Ethereal -- Heaven's Light
					    --and MaxDamagePercent >= 250 --note: 250-300 edam
						--and ClassSkillsBonus >= 2 --note: 2-3 Paladin skill
						--and Sockets >= 1 --note: 1-2 sock
						--and HealAfterDemonKill >= 15 --note: 15-20 life after demon
					or UniqueIndex == 371 and not Ethereal -- Heaven's Light
					    --and MaxDamagePercent >= 250 --note: 250-300 edam
						--and ClassSkillsBonus >= 2 --note: 2-3 Paladin skill
						--and Sockets >= 1 --note: 1-2 sock
						--and HealAfterDemonKill >= 15 --note: 15-20 life after demon
						and (ClassSkillsBonus >= 3 or Sockets >= 2)
						
					or UniqueIndex == 389 and Ethereal  -- The Redeemer
					    --and MaxDamagePercent >= 250 --note: 250-300 edam
					    --and DemonDamagePercent >= 200 --note: 200-250 dam to demon
					    --and RedemptionSkill >= 2 --note: 2-4 Redemption
					    --and HolyBoltSkill >= 2 --note: 2-4 HolyBolt
					or UniqueIndex == 389 and not Ethereal  -- The Redeemer
					    and MaxDamagePercent >= 275 --note: 250-300 edam
					    --and DemonDamagePercent >= 200 --note: 200-250 dam to demon
					    --and RedemptionSkill >= 2 --note: 2-4 Redemption
					    --and HolyBoltSkill >= 2 --note: 2-4 HolyBolt
				)
		},
--]]
-----------------------------------------------------
---[[ Astreon's Iron Ward (Caduceus)
	["7ws"] =  
		{
			priority = 8, identify = false,
			goodItem =
				( Unidentified
					or Ethereal
					    --and SkillTabBonus >= 2 --note: 2-4 Paladin Combat Skill
						--and MaxDamagePercent >= 240 --note: 240-280 edam
						--and DamageReduction >= 4 --note: 4-7 dam redu
						--and ToHitPercent >= 150 --note: 150-200 AR
				    or not Ethereal
					    --and SkillTabBonus >= 2 --note: 2-4 Paladin Combat Skill
						--and MaxDamagePercent >= 240 --note: 240-280 edam
						--and DamageReduction >= 4 --note: 4-7 dam redu
						--and ToHitPercent >= 150 --note: 150-200 AR
				)
		},
--]]

-----------------------------------------------------
-- Spears -------------------------------------------
-----------------------------------------------------
-- ** Normal Spears ** --

--[[ The Dragon Chang (Spear)
	["spr"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Razortine (Trident)
	["tri"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 30 --note: 30-50 edam
				    or not Ethereal
					    and MaxDamagePercent >= 30 --note: 30-50 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Bloodthief (Brandistock) 
	["brn"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 50 --note: 50-70 edam
						and LifeDrainMinDamage >= 8 --note: 8-12 lifestolen
				    or not Ethereal
					    and MaxDamagePercent >= 50 --note: 50-70 edam
						and LifeDrainMinDamage >= 8 --note: 8-12 lifestolen
				)
		},	
--]]
-----------------------------------------------------
--[[ Lance of Yaggai (Spetum)
	["spt"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ The Tannr Gorerod (Pike)
	["pik"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 80 --note: 80-100 edam
				    or not Ethereal
					    and MaxDamagePercent >= 80 --note: 80-100 edam
				)
		},	
--]]
-----------------------------------------------------
-- ** Exceptional Spears ** --
-----------------------------------------------------
--[[ The Impaler (War Spear)
	["9sr"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 140 --note: 140-170 edam
					or not Ethereal
					    and MaxDamagePercent >= 140 --note: 140-170 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Kelpie Snare (Fuscina)
	["9tr"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 140 --note: 140-180 edam
					or not Ethereal
					    and MaxDamagePercent >= 140 --note: 140-180 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Soulfeast Tine (War Fork)
	["9br"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 150 --note: 150-190 edam
					    and ToHit >= 150 --note: 150-250 AR
					or not Ethereal
					    and MaxDamagePercent >= 150 --note: 150-190 edam
					    and ToHit >= 150 --note: 150-250 AR
				)
		},	
--]]
-----------------------------------------------------
---[[ Hone Sundan (Yari) (Repairs 1 Durability In 10 Seconds)
	["9st"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 190 --note: 160-200 edam
					--or not Ethereal
					    --and MaxDamagePercent >= 160 --note: 160-200 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Spire of Honor (Lance) 
	["9p9"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 150 --note: 150-200 edam
					or not Ethereal
					    and MaxDamagePercent >= 150 --note: 150-200 edam
				)
		},	
--]]
----------------------------------------------------- 
-- ** Elite Spears ** --
-----------------------------------------------------
---[[ Arioc's Needle (Hyperion Spear)
	["7sr"] = 
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
						--and AllSkillsBonus >= 2 --note: 2-4 all skill
						--and MaxDamagePercent >= 180 --note: 180-230 edam
					or not Ethereal
						and AllSkillsBonus >= 4 --note: 2-4 all skill
						and MaxDamagePercent >= 205 --note: 180-230 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Viperfork (Mancatcher)
	["7br"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MaxDamagePercent >= 190 --note: 190-240 edam
						--and ToHit >= 200 --note: 200-250 AR
						--and PoisonResist >= 30 --note: 30-50 pois res
					or not Ethereal
						and MaxDamagePercent >= 215 --note: 190-240 edam
						and ToHit >= 225 --note: 200-250 AR
						--and PoisonResist >= 30 --note: 30-50 pois res
				)
		},
--]]
-----------------------------------------------------
---[[ Steel Pillar (War Pike) (Indestructible)
	["7p7"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 235 --note: 210-260 edam
						--and ArmorClass >= 50 --note: 50-80 armor def
				)
		},
--]]
 
-----------------------------------------------------
-- Staves -------------------------------------------
-----------------------------------------------------
-- ** Normal Staves ** --

--[[ Bane Ash (Short Staff)
	["sst"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 50 --note: 50-60 edam
				    or not Ethereal
						and MaxDamagePercent >= 50 --note: 50-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Serpent Lord (Long Staff)
	["lst"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 30 --note: 30-40 edam
				    or not Ethereal
						and MaxDamagePercent >= 30 --note: 30-40 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Spire of Lazarus (Gnarled Staff)
	["cst"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ The Salamander (Battle Staff)
	["bst"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ The Iron Jang Bong (War Staff)
	["wst"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
----------------------------------------------------- 
-- ** Exceptional Staves ** --
-----------------------------------------------------
--[[ Razorswitch (Jo Staff)
	["8ss"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
---[[ Ribcracker (Quarterstaff)
	["8ls"] = 
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 280 --note: 200-300 edam
					--or not Ethereal
					    --and MaxDamagePercent >= 200 --note: 200-300 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Chromatic Ire (Cedar Staff)
	["8cs"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxLifePercent >= 20 --note: 20-25 increase maxlife
						and AllResist >= 20 --note: 20-40 all res
				    or not Ethereal
						and MaxLifePercent >= 20 --note: 20-25 increase maxlife
						and AllResist >= 20 --note: 20-40 all res
				)
		},	
--]]
-----------------------------------------------------
--[[ Warpspear (Gothic Staff)
	["8bs"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Skull Collector (Rune Staff)
	["8ws"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
----------------------------------------------------- 
-- ** Elite Staves ** --
-----------------------------------------------------
---[[ Ondal's Wisdom (Elder Staff)
	["6cs"] = 
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and AllSkillsBonus >= 2 --note: 2-4 all skill
						--and ArmorClass >= 450 --note: 450-500 armor def
						--and Energy >= 40 --note: 40-50 energy
						--and MagicDamageReduction >= 5 --note: 5-8 magic redu
				    or not Ethereal
						and AllSkillsBonus >= 3 --note: 2-4 all skill
						--and ArmorClass >= 450 --note: 450-500 armor def
						--and Energy >= 40 --note: 40-50 energy
						--and MagicDamageReduction >= 5 --note: 5-8 magic redu
				)
		},
--]]
-----------------------------------------------------
---[[ Mang Song's Lesson (Archon Staff)
	["6ws"] =
		{
			priority = 5, identify = false,
			goodItem =
				( Unidentified
					or Ethereal
						--and PassiveLightningPierce >= 7 --note: 7-15 -light res
					    --and PassiveColdPierce >= 7 --note: 7-15 -cold res
					    --and PassiveFirePierce >= 7 --note: 7-15 -fire res
				    or not Ethereal
						--and PassiveLightningPierce >= 7 --note: 7-15 -light res
					    --and PassiveColdPierce >= 7 --note: 7-15 -cold res
					    --and PassiveFirePierce >= 7 --note: 7-15 -fire res
				)
		},
--]]

-----------------------------------------------------
-- Swords -------------------------------------------
-----------------------------------------------------
-- ** Normal Swords ** --

--[[ Rixot's Keen (Short Sword)
	["ssd"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Blood Crescent (Scimitar)
	["scm"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 60 --note: 60-80 edam
				    or not Ethereal
						and MaxDamagePercent >= 60 --note: 60-80 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Skewer of Krintiz (Sabre)
	["sbr"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Gleamscythe (Falchion) 
	["flc"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 60 --note: 60-100 edam
				    or not Ethereal
						and MaxDamagePercent >= 60 --note: 60-100 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Griswold's Edge (Broad Sword)
	["bsd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 80 --note: 80-120 edam
						and FireMaxDamage >= 15 --note: 15-25 max fire dam
				    or not Ethereal
						and MaxDamagePercent >= 80 --note: 80-120 edam
						and FireMaxDamage >= 15 --note: 15-25 max fire dam
				)
		},	
--]]
-----------------------------------------------------
--[[ Hellplague (Long Sword)
	["lsd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 70 --note: 70-80 edam
				    or not Ethereal
						and MaxDamagePercent >= 70 --note: 70-80 edam
				)
		},		
--]]
-----------------------------------------------------
--[[ Culwen's Point (War Sword)
	["wsd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 70 --note: 70-80 edam
				    or not Ethereal
						and MaxDamagePercent >= 70 --note: 70-80 edam
				)
		},		
--]]
-----------------------------------------------------
--[[ Shadowfang (Two-Handed Sword)
	["2hs"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},		
--]]
-----------------------------------------------------
--[[ Soulflay (Claymore) 
	["clm"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 70 --note: 70-100 edam
						and ManaDrainMinDamage >= 4 --note: 4-10 manastolen
				    or not Ethereal
						and MaxDamagePercent >= 70 --note: 70-100 edam
						and ManaDrainMinDamage >= 4 --note: 4-10 manastolen
				)
		},		
--]]
-----------------------------------------------------
--[[ Kinemil's Awl (Giant Sword)
	["gis"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 80 --note: 80-100 edam
						and FireMaxDamage >= 20 --note: 20-40 max fire dam
				    or not Ethereal
						and MaxDamagePercent >= 80 --note: 80-100 edam
						and FireMaxDamage >= 20 --note: 20-40 max fire dam
				)
		},	
--]]
-----------------------------------------------------
--[[ Blacktongue (Bastard Sword) 
	["bsw"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 50 --note: 50-60 edam
				    or not Ethereal
						and MaxDamagePercent >= 50 --note: 50-60 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Ripsaw (Flamberge)
	["flb"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 80 --note: 80-100 edam
				    or not Ethereal
						and MaxDamagePercent >= 80 --note: 80-100 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Patriarch (Great Sword)
	["gsd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxDamagePercent >= 100 --note: 100-120 edam
				    or not Ethereal
						and MaxDamagePercent >= 100 --note: 100-120 edam
				)
		},	
--]]
-----------------------------------------------------
-- ** Exceptional Swords ** --
-----------------------------------------------------
--[[ Bloodletter (Gladius)
	["9ss"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and WhirlwindSkill >= 1 --note: 1-3 Whirlwind
					    --and SwordMasterySkill >= 2 --note: 2-4 SwordMastery
					or not Ethereal
					    --and WhirlwindSkill >= 1 --note: 1-3 Whirlwind
					    --and SwordMasterySkill >= 2 --note: 2-4 SwordMastery
				)
		},	
--]]
-----------------------------------------------------
--[[ Coldsteel Eye (Cutlass)
	["9sm"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 200 --note: 200-250 edam
					or not Ethereal
					    --and MaxDamagePercent >= 200 --note: 200-250 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Hexfire (Shamshir)
	["9sb"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 140 --note: 140-160 edam
					or not Ethereal
					    --and MaxDamagePercent >= 140 --note: 140-160 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Blade Of Ali Baba (Tulwar)
	["9fc"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 60 --note: 60-120 edam
					    --and Dexterity >= 5 --note: 5-15 dex
					--or not Ethereal
					    --and MaxDamagePercent >= 60 --note: 60-120 edam
					    --and Dexterity >= 5 --note: 5-15 dex
				)
		},	
--]]
-----------------------------------------------------
--[[ Ginther's Rift (Dimensional Blade) (Repairs 1 Durability in 5 Seconds)
	["9cr"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 100 --note: 100-150 edam
					    --and MagicDamageReduction >= 7 --note: 7-12 magic redu
					--or not Ethereal
					    --and MaxDamagePercent >= 100 --note: 100-150 edam
					    --and MagicDamageReduction >= 7 --note: 7-12 magic redu
				)
		},	
--]] 
-----------------------------------------------------
--[[ Headstriker (Battle Sword)
	["9bs"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Plague Bearer (Rune Sword)
	["9ls"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ The Atlantean (Ancient Sword) 
	["9wd"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 200 --note: 200-250 edam
					or not Ethereal
					    and MaxDamagePercent >= 200 --note: 200-250 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Crainte Vomir (Espandon)
	["92h"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 160 --note: 160-200 edam
					or not Ethereal
					    and MaxDamagePercent >= 160 --note: 160-200 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Bing Sz Wang (Dacian Falx)
	["9cm"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 130 --note: 130-160 edam
					or not Ethereal
					    and MaxDamagePercent >= 130 --note: 130-160 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ The Vile Husk (Tusk Sword)
	["9gs"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 150 --note: 150-200 edam
					or not Ethereal
					    and MaxDamagePercent >= 150 --note: 150-200 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Cloudcrack (Gothic Sword)
	["9b9"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 150 --note: 150-200 edam
					or not Ethereal
					    and MaxDamagePercent >= 150 --note: 150-200 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Todesfaelle Flamme (Zweihander)
	["9fb"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 120 --note: 120-160 edam
					or not Ethereal
					    and MaxDamagePercent >= 120 --note: 120-160 edam
				)
		},	
--]]
-----------------------------------------------------
--[[ Swordguard (Executioner Sword)
	["9gd"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 170 --note: 170-180 edam
					    and AllResist >= 10 --note: 10-20 all res
					or not Ethereal
					    and MaxDamagePercent >= 170 --note: 170-180 edam
					    and AllResist >= 10 --note: 10-20 all res
				)
		},	
--]]
-----------------------------------------------------
-- ** Elite Swords ** --
-----------------------------------------------------
--[[ Djinn Slayer (Ataghan)
	["7sm"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MaxDamagePercent >= 190 --note: 190-240 edam
						--and DemonDamagePercent >= 100 --note: 100-150 dam to demon
						--and DemonToHit >= 200 --note: 200-300 AR to demon
						--and ManaDrainMinDamage >= 3 --note: 3-6 manastolen
						--and AbsorbLight >= 3 --note: 3-7 light absorb
						--and Sockets >= 1 --note: 1-2 sock
					or not Ethereal
						and MaxDamagePercent >= 215 --note: 190-240 edam
						--and DemonDamagePercent >= 100 --note: 100-150 dam to demon
						--and DemonToHit >= 200 --note: 200-300 AR to demon
						--and ManaDrainMinDamage >= 3 --note: 3-6 manastolen
						--and AbsorbLight >= 3 --note: 3-7 light absorb
						and Sockets >= 2 --note: 1-2 sock
				)
		},
--]]
-----------------------------------------------------
--[[ Bloodmoon (Elegant Blade)
	["7sb"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MaxDamagePercent >= 210 --note: 210-260 edam
						--and LifeDrainMinDamage >= 10 --note: 10-15 lifestolen
						--and HealAfterKill >= 7 --note: 7-13 life after kill
					or not Ethereal
						and MaxDamagePercent >= 235 --note: 210-260 edam
						and LifeDrainMinDamage >= 14 --note: 10-15 lifestolen
						--and HealAfterKill >= 7 --note: 7-13 life after kill
				)
		},
--]]
-----------------------------------------------------
---[[ Lightsabre or Azurewrath (Phase Blade) (Both Indestructible)
	["7cr"] =  
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or UniqueIndex == 301 -- Azurewrath
					    and MaxDamagePercent >= 250 --note: 230-270 edam
					    --and Aura >= 10 --note: 10-13 Sanctuary aura
					--note: Removed StatPoints check; doesn't work.
					
					or UniqueIndex == 259 -- Lightsabre
					    and MaxDamagePercent >= 175 --note: 150-200 edam
					    --and ManaDrainMinDamage >= 5 --note: 5-7 manastolen
					    --and SkillOnAttack >= 14 --note: 14-20 ChainLightning
				)
		},
--]]
-----------------------------------------------------
--[[ Frostwind (Cryptic Sword)
	["7ls"] = 
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MaxDamagePercent >= 180 --note: 180-230 edam
						--and ArcticBlastSkill >= 7 --note: 7-14 ArcticBlast
						--and AbsorbColdPercent >= 7 --note: 7-15 cold absorb
					or not Ethereal
						and MaxDamagePercent >= 205 --note: 180-230 edam
						and ArcticBlastSkill >= 10 --note: 7-14 ArcticBlast
						and AbsorbColdPercent >= 10 --note: 7-15 cold absorb
				)
		},
--]]
-----------------------------------------------------
--[[ Flamebellow (Balrog Blade)
	["7gs"] = 
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MaxDamagePercent >= 170 --note: 170-240 edam
						--and InfernoSkill >= 12 --note: 12-18 Inferno
						--and Strength >= 10 --note: 10-20 str
						--and Vitality >= 5 --note: 5-10 vit
						--and AbsorbFirePercent >= 7 --note: 7-15 fire absorb
					or not Ethereal
						and MaxDamagePercent >= 210 --note: 170-240 edam
						--and InfernoSkill >= 12 --note: 12-18 Inferno
						and Strength >= 15 --note: 10-20 str
						--and Vitality >= 5 --note: 5-10 vit
						--and AbsorbFirePercent >= 7 --note: 7-15 fire absorb
				)
		},
--]]
-----------------------------------------------------
---[[ Doombringer (Champion Sword) (Indestructible)
	["7b7"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 215 --note: 180-250 edam
						--and LifeDrainMinDamage >= 5 --note: 5-7 lifestolen
				)
		},
--]]
-----------------------------------------------------
---[[ The Grandfather (Colossus Blade) (Indestructible)
	["7gd"] = 
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 200 --note: 150-250 edam
				)
		},
--]]

-----------------------------------------------------
-- Wands --------------------------------------------
-----------------------------------------------------
-- ** Normal Wands ** --

--[[ Torch of Iro (Wand)
	["wnd"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Maelstrom (Yew Wand)
	["ywn"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and IronMaidenSkill >= 1 --note: 1-3 IronMaiden
						and AmplifyDamageSkill >= 1 --note: 1-3 AmpDamage
						and TerrorSkill >= 1 --note: 1-3 Terror
						and CorpseExplosionSkill >= 1 --note: 1-3 CorpExplosion
				    or not Ethereal
						and IronMaidenSkill >= 1 --note: 1-3 IronMaiden
						and AmplifyDamageSkill >= 1 --note: 1-3 AmpDamage
						and TerrorSkill >= 1 --note: 1-3 Terror
						and CorpseExplosionSkill >= 1 --note: 1-3 CorpExplosion
				)
		},	
--]]
-----------------------------------------------------
--[[ Gravenspine (Bone Wand)
	["bwn"] = 
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and MaxMana >= 25 --note: 25-50 mana
				    or not Ethereal
						and MaxMana >= 25 --note: 25-50 mana
				)
		},	
--]]
-----------------------------------------------------
--[[ Ume's Lament (Grim Wand)
	["gwn"] =  
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
----------------------------------------------------- 
-- ** Exceptional Wands ** --
-----------------------------------------------------
--[[ Suicide Branch (Burnt Wand)
	["9wn"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Carin Shard (Petrified Wand)
	["9yw"] = 
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Arm of King Leoric (Tomb Wand)
	["9bw"] = 
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    --or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ Blackhand Key (Grave Wand)
	["9gw"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
				    --or not Ethereal
			--note: all the same, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
-- ** Elite Wands ** --
-----------------------------------------------------
---[[ Boneshade (Lich Wand)
	["7bw"] =  
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and BoneSpiritSkill >= 2 --note: 1-2 BoneSpirit
						and BoneSpearSkill >= 3 --note: 2-3 BoneSpear
						--and BoneWallSkill >= 2 --note: 2-3 BoneWall
						--and BoneArmorSkill >= 4 --note: 4-5 BoneArmor
						--and TeethSkill >= 4 --note: 4-5 Teeth
				    or not Ethereal
						and BoneSpiritSkill >= 2 --note: 1-2 BoneSpirit
						and BoneSpearSkill >= 3 --note: 2-3 BoneSpear
						--and BoneWallSkill >= 2 --note: 2-3 BoneWall
						--and BoneArmorSkill >= 4 --note: 4-5 BoneArmor
						--and TeethSkill >= 4 --note: 4-5 Teeth
				)
		},
--]]
-----------------------------------------------------
---[[ Death's Web (Unearthed Wand)
	["7gw"] =  
		{
			priority = 10, identify = false,
			goodItem =
				( Unidentified
					or Ethereal
						--and PassivePoisonPierce >= 40 --note: 40-50 -poison res
					    --and ManaAfterKill >= 7 --note: 7-12 mana ea kill
					    --and HealAfterKill >= 7 --note: 7-12 life ea kill
					    --and PoisonAndBoneTab >= 1 --note: 1-2 Poison/Bone skills
				    or not Ethereal
						--and PassivePoisonPierce >= 40 --note: 40-50 -poison res
					    --and ManaAfterKill >= 7 --note: 7-12 mana ea kill
					    --and HealAfterKill >= 7 --note: 7-12 life ea kill
					    --and PoisonAndBoneTab >= 1 --note: 1-2 Poison/Bone skills
				)
		},
--]]

-----------------------------------------------------
-- Throwing Weapons ---------------------------------
-----------------------------------------------------
-- ** Exceptional Throwing Weapons ** --

--[[ Deathbit (Battle Dart) (Replenishes Quantity 1 in 4 sec.)
	["9tk"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 155 --note: 130-180 edam
					    --and ToHit >= 200 --note: 200-450 AR
					    --and LifeDrainMinDamage >= 7 --note: 7-9 lifestolen
						--and ManaDrainMinDamage >= 4 --note: 4-6 manastolen 
					--or not Ethereal
					    --and MaxDamagePercent >= 130 --note: 130-180 edam
					    --and ToHit >= 200 --note: 200-450 AR
					    --and LifeDrainMinDamage >= 7 --note: 7-9 lifestolen
						--and ManaDrainMinDamage >= 4 --note: 4-6 manastolen
				)
		},	
--]]
-----------------------------------------------------
--[[ The Scalper (Francisca) (Replenishes Quantity 1 in 3 sec.)
	["9ta"] =  
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    and MaxDamagePercent >= 175 --note: 150-200 edam
					    --and LifeDrainMinDamage >= 4 --note: 4-6 lifestolen
					--or not Ethereal
					    --and MaxDamagePercent >= 150 --note: 150-200 edam
					    --and LifeDrainMinDamage >= 4 --note: 4-6 lifestolen
				)
		},	
--]]
-----------------------------------------------------
-- ** Elite Throwing Weapons ** --
-----------------------------------------------------
---[[ Warshrike (Winged Knife) (Replenishes Quantity 1 in 3 sec.)
	["7bk"] = 
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 200 --note: 200-250 edam
					--or not Ethereal
					    --and MaxDamagePercent >= 225 --note: 200-250 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Gimmershred (Flying Axe)
	["7ta"] =
	    {
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and MaxDamagePercent >= 160 --note: 160-210 edam
				    or not Ethereal
						--and MaxDamagePercent >= 160 --note: 160-210 edam
				)
		},
--]]
-----------------------------------------------------
---[[ Lacerator (Winged Axe) (Replenishes Quantity 1 in 4 sec.)
	["7b8"] = 
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 150 --note: 150-210 edam
					--or not Ethereal
					   --and MaxDamagePercent >= 185 --note: 150-210 edam
				)
		},
--]]

-----------------------------------------------------
-- Javelins -----------------------------------------
-----------------------------------------------------
-- ** Elite Javelins ** --

--[[ Demon's Arch (Balrog Spear) (Replenishes Quantity 1 in 3 sec.)
	["7s7"] = 
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 160 --note: 160-210 edam
						--and LifeDrainMinDamage >= 6 --note: 6-12 lifestolen
					or not Ethereal
					    and MaxDamagePercent >= 190 --note: 160-210 edam
						and LifeDrainMinDamage >= 10 --note: 6-12 lifestolen
				)
		},
--]]
-----------------------------------------------------
--[[ Wraith Flight (Ghost Glaive) (Replenishes Quantity 1 in 2 sec.) (Ethereal)
	["7gl"] =
	    {
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 170 --note: 150-190 edam
						and LifeDrainMinDamage >= 11 --note: 9-13 lifestolen
				)
		},
--]]
-----------------------------------------------------
--[[ Gargoyle's Bite (Winged Harpoon) (Replenishes Quantity 1 in 3 sec.)
	["7ts"] = 
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 180 --note: 180-230 edam
						--and LifeDrainMinDamage >= 9 --note: 9-15 lifestolen
					or not Ethereal
					    and MaxDamagePercent >= 205 --note: 180-230 edam
						and LifeDrainMinDamage >= 12 --note: 9-15 lifestolen
				)
		},
--]]

-----------------------------------------------------
-- Amazon Weapons -----------------------------------
-----------------------------------------------------

--[[ Lycander's Flank (Ceremonial Pike)
	["am9"] =
		{
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 150 --note: 150-200 edam
						--and LifeDrainMinDamage >= 5 --note: 5-9 lifestolen
					or not Ethereal
					    and MaxDamagePercent >= 175 --note: 150-200 edam
						and LifeDrainMinDamage >= 7 --note: 5-9 lifestolen
				)
		},
--]]
-----------------------------------------------------
--[[ Stoneraven (Matriarchal Spear)
	["amd"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					    --and MaxDamagePercent >= 230 --note: 230-280 edam
						--and JavelinAndSpearTab >= 1 --note: 1-3 Jav/Spear Skills
						--and ArmorClass >= 400 --note: 400-600 armor def
						--and AllResist >= 30 --note: 30-50 all res
					or not Ethereal
					    and MaxDamagePercent >= 255 --note: 230-280 edam
						and JavelinAndSpearTab >= 2 --note: 1-3 Jav/Spear Skills
						--and ArmorClass >= 400 --note: 400-600 armor def
						and AllResist >= 40 --note: 30-50 all res
				)
		},
--]]
-----------------------------------------------------
---[[ Titan's Revenge (Ceremonial Javelin) (Replenishes Quantity 1 in 3 sec.)
	["ama"] = 
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and MaxDamagePercent >= 150 --note: 150-200 edam
						--and LifeDrainMinDamage >= 5 --note: 5-9 lifestolen
					--or not Ethereal
						--and MaxDamagePercent >= 175 --note: 150-200 edam
						--and LifeDrainMinDamage >= 7 --note: 5-9 lifestolen
				)
		},
--]]
-----------------------------------------------------
---[[ Thunderstroke (Matriarchal Javelin)
	["amf"] = 
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and JavelinAndSpearTab >= 2 --note: 2-4 Jav/Spear Skills
						--and MaxDamagePercent >= 150 --note: 150-200 edam
					--or not Ethereal
						--and JavelinAndSpearTab >= 3 --note: 2-4 Jav/Spear Skills
						--and MaxDamagePercent >= 175 --note: 150-200 edam
				)
		},
--]]
-----------------------------------------------------
--[[ Lycander's Aim (Ceremonial Bow)
	["am7"] =
		{
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 175 --note: 150-200 edam
						and ManaDrainMinDamage >= 7 --note: 5-8 manastolen
				)
		},
--]]
-----------------------------------------------------
--[[ Blood Raven's Charge (Matriarchal Bow)
	["amb"] =  
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or BowAndCrossBowTab >= 3 --note: 2-4 Bow/Crossbow Skills
						--and ToHitPercent >= 200 --note: 200-300 AR
						and MaxDamagePercent >= 205 --note: 180-230 edam
				)
		},
--]]

-----------------------------------------------------
-- Assassin Weapons ---------------------------------
-----------------------------------------------------

---[[ Bartuc's Cut-Throat (Greater Talons)
	["9tw"] = 
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MaxDamagePercent >= 150 --note: 150-200 edam
						--and LifeDrainMinDamage >= 5 --note: 5-9 lifestolen
				    or not Ethereal
						and MaxDamagePercent >= 185 --note: 150-200 edam
						and LifeDrainMinDamage >= 7 --note: 5-9 lifestolen
				)
		},
--]]
-----------------------------------------------------
---[[ Jade Talon (Wrist Sword)
	["7wb"] = 
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MartialArtsTab >= 1 --note: 1-2 MartialArts
						--and ShadowDisciplinesTab >= 1 --note: 1-2 ShadowDisc
						--and MaxDamagePercent >= 190 --note: 190-240 edam
						--and ManaDrainMinDamage >= 10 --note: 10-15 manastolen
						--and AllResist >= 40 --note: 40-50 all res
				    or not Ethereal
						--and MartialArtsTab >= 1 --note: 1-2 MartialArts
						--and ShadowDisciplinesTab >= 1 --note: 1-2 ShadowDisc
						and MaxDamagePercent >= 215 --note: 190-240 edam
						--and ManaDrainMinDamage >= 10 --note: 10-15 manastolen
						and AllResist >= 45 --note: 40-50 all res
				)
		},
--]]
-----------------------------------------------------
--[[ Shadow Killer (Battle Cestus) (Indestructible) (Ethereal)
	["7cs"] =
		{
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or MaxDamagePercent >= 195 --note: 170-220 edam
					    and ManaAfterKill >= 13 --note: 10-15 mana ea kill
				)
		},
--]]
-----------------------------------------------------
--[[ Firelizard's Talons (Feral Claws) 
	["7lw"] =
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and MartialArtsTab >= 1 --note: 1-3 MartialArts
						--and WakeOfInfernoSkill >= 1 --note: 1-2 WakeOfInferno
						--and WakeOfFireSkill >= 1 --note: 1-2 WakeOfFire
						--and MaxDamagePercent >= 200 --note: 200-270 edam
						--and FireResist >= 40 --note: 40-70 fire res
				    or not Ethereal
						and MartialArtsTab >= 2 --note: 1-3 MartialArts
						--and WakeOfInfernoSkill >= 1 --note: 1-2 WakeOfInferno
						--and WakeOfFireSkill >= 1 --note: 1-2 WakeOfFire
						and MaxDamagePercent >= 235 --note: 200-270 edam
						--and FireResist >= 40 --note: 40-70 fire res
				)
		},
--]]

-----------------------------------------------------
-- Sorceress Weapons --------------------------------
-----------------------------------------------------

---[[ The Oculus (Swirling Crystal)
	["oba"] = 
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    or not Ethereal
			--note: all the same, unless ethereal
			    )
		},
--]]
-----------------------------------------------------
---[[ Eschuta's Temper (Eldritch Orb)
	["obc"] =  
		{
			priority = 8, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and ClassSkillsBonus >= 3 --note: 1-3 Sorceress Skill
						--and PassiveFireMastery >= 10 --note: 10-20 fireskill dam
						--and PassiveLightningMastery >= 10 --note: 10-20 lightskill dam
						--and Energy >= 20 --note: 20-30 energy
					or not Ethereal
						and ClassSkillsBonus >= 3 --note: 1-3 Sorceress Skill
						--and PassiveFireMastery >= 10 --note: 10-20 fireskill dam
						--and PassiveLightningMastery >= 10 --note: 10-20 lightskill dam
						--and Energy >= 20 --note: 20-30 energy
				)
		},
--]]
-----------------------------------------------------
---[[ Death's Fathom (Dimensional Shard)
	["obf"] = 
		{
			priority = 10, identify = false,
			goodItem =
				( Unidentified
					or Ethereal
						--and PassiveColdMastery >= 15 --note: 15-30 coldskill dam
					    --and LightResist >= 25 --note: 25-40 light res
					    --and FireResist >= 25 --note: 25-40 fire res
					or not Ethereal
						--and PassiveColdMastery >= 15 --note: 15-30 coldskill dam
					    --and LightResist >= 25 --note: 25-40 light res
					    --and FireResist >= 25 --note: 25-40 fire res
				)
		},
--]]
}