-- expansion\unique\gloves.lua "moderate"

unique.gloves =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- ** Normal Gloves ** --
-----------------------------------------------------
--[[ The Hand of Broc (Leather Gloves)
	["lgl"] =
		{
			priority = 2, identify = true,
			goodItem =
   			( not Ethereal and
				( Unidentified
					or DefensePercent >= 15 --note: 10-20 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Bloodfist (Heavy Gloves)
	["vgl"] =
		{
			priority = 2, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 15 --note: 10-20 ed
				)
			)
		},
--]]
-----------------------------------------------------
---[[ Chance Guards (Chain Gloves)
	["mgl"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or MagicFind >= 38 --note: 25-40 mf
						--and DefensePercent >= 20 --note: 20-30 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Magefist (Light Gauntlets)
	["tgl"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 25 --note: 20-30 ed
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Frostburn (Gauntlets)
	["hgl"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 15 --note: 10-20 ed
				)
			)
		},
--]]
-----------------------------------------------------
-- ** Exceptional Gloves ** --
-----------------------------------------------------
--[[ Venom Grip (Demonhide Gloves)
	["xlg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or TotalDefense >= 108 --note: 97-118 def
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Gravepalm (Sharkskin Gloves)
	["xvg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 140 --note: 140-180 ed
					    and UndeadDamagepercent >= 150 --note: 100-200 dam undead
					    --and UndeadToHit >= 100 --note: 100-200 AR to undead
				)
			)
		},
--]]
-----------------------------------------------------
--[[ Ghoulhide (Heavy Bracers)
	["xmg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 150 --note: 150-190 ed
					    and ManaDrainMinDamage >= 5 --note: 4-5 manastolen
				)
			)
		},
--]]
-----------------------------------------------------
---[[ Lava Gout (Battle Gauntlets)
	["xtg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 190 --note: 150-200 ed
				)
			)
		},
--]]
-----------------------------------------------------
---[[ Hellmouth (War Gauntlets)
	["xhg"] =
		{
			priority = 5, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or DefensePercent >= 190 --note: 150-200 ed
				)
			)
		},
--]]
-----------------------------------------------------
-- ** Elite Gloves ** --
-----------------------------------------------------
---[[ Dracul's Grasp (Vampirebone Gloves)
	["uvg"] =
		{
			priority = 7, identify = false,
			goodItem =
			( not Ethereal and
				( Unidentified
					or Strength >= 13 --note: 10-15 str
						--and LifeDrainMinDamage >= 7 --note: 7-10 lifestolen
						--and HealAfterKill >= 5 --note: 5-10 life after kill
						--and DefensePercent >= 90 --note: 90-120 ed
				)
			)
		},
--]]
-----------------------------------------------------
---[[ Soul Drainer (Vambraces)
	["umg"] =
		{
			priority = 6, identify = true,
			goodItem =
			( not Ethereal and
				( Unidentified
					or LifeDrainMinDamage >= 6 --note: 4-7 lifestolen
						and ManaDrainMinDamage >= 6 --note: 4-7 manastolen
						--and DefensePercent >= 90 --note: 90-120 ed
				)
			)
		},
--]]
-----------------------------------------------------
---[[ Steelrend (Ogre Gauntlets)
	["uhg"] =
		{
			priority = 8, identify = false,
			goodItem =
			( not Ethereal and
				( Unidentified
					or TotalDefense >= 232 --note: 232-281 def
						and MaxDamagePercent >= 45 --note: 30-60 edam
						and Strength >= 15 --note: 15-20 str
				)
			)
		}
--]]
-----------------------------------------------------
}