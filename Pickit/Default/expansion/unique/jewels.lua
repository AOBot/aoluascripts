-- expansion\unique\jewels.lua "moderate"

local minSum = 8 -- Mastery+Pierce (6-10)

unique.jewels =
{ checkStats = _CheckUniqueItemStats,
	["jew"] = ------ Rainbow Facet (Jewel)
		{ priority = 8, identify = true,
			goodItem =
				( Unidentified or
					--SkillOnDeath ~= 0 and
					( (PassiveFireMastery + PassiveFirePierce) >= minSum
						or (PassiveColdMastery + PassiveColdPierce) >= minSum
						or (PassiveLightningMastery + PassiveLightningPierce) >= minSum
						or (PassivePoisonMastery + PassivePoisonPierce) >= minSum
					)
				)
		}
}