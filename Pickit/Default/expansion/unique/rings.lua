--- expansion\unique\rings.lua "moderate"

unique.rings =
{ checkStats = _CheckUniqueItemStats,
	["rin"] = ------ Ring
		{ priority = 8, identify = true,
			goodItem =
				( Unidentified
-----------------------------------------------------
-- This part keeps rings regardless of stats
-- Note: this part overrides the next part
-----------------------------------------------------
					--or UniqueIndex == 120 -- Nagelring
					--or UniqueIndex == 121 -- Manald Heal
					or UniqueIndex == 122 -- The Stone of Jordan
					or UniqueIndex == 268 -- Bul-Kathos' Wedding Band
					--or UniqueIndex == 274 -- Dwarf Star
					--or UniqueIndex == 275 -- Raven Frost
					--or UniqueIndex == 300 -- Nature's Peace
					or UniqueIndex == 319 -- Wisp Projector
					--or UniqueIndex == 378 -- Carrion Wind
					
-----------------------------------------------------
-- This part keeps rings with specific stats
-----------------------------------------------------
---[[ Nagelring
					or UniqueIndex == 120 -- Nagelring
						and MagicFind >= 30 --note: 15-30 MF
						--and ToHit >= 50 --note: 50-75 AR
--]]
-----------------------------------------------------
---[[ Manald Heal
	                or UniqueIndex == 121 -- Manald Heal
	                    and ManaDrainMinDamage >= 7 --note: 4-7 manastolen
	                    --and LifeRegen >= 5 --note: 5-8 replen life
--]]
-----------------------------------------------------	                    
	--The Stone of Jordan (all the same / perfect) (see UniqueIndex list above)
-----------------------------------------------------	
--[[ Dwarf Star
	                or UniqueIndex == 274 -- Dwarf Star
	                    and MagicDamageReduction >= 14 --note: 12-15 magic dam redu
--]]
-----------------------------------------------------	                    
---[[ Raven Frost
					or UniqueIndex == 275 -- Raven Frost
						and Dexterity >= 20 --note: 15-20 dex
						--and ToHit >= 150 --note: 150-250 AR
--]]
-----------------------------------------------------						
--[[ Bul-Kathos' Wedding Band 
	                or UniqueIndex == 268 -- Bul-Kathos' Wedding Band
	                    and LifeDrainMinDamage >= 3 --note: 3-5 lifestolen
--]]
-----------------------------------------------------	                    
---[[ Carrion Wind
					or UniqueIndex == 378 -- Carrion Wind
						and LifeDrainMinDamage >= 8 --note: 6-9 lifestolen
						--and ArmorClassVsMissile >= 100 --note: 100-160 def vs missle
--]]
-----------------------------------------------------						
---[[ Nature's Peace
	                or UniqueIndex == 300 -- Nature's Peace
	                    and PoisonResist >= 25 --note: 20-30 pois res
	                    and DamageReduction >= 10 --note: 7-11 dmg red
--]]
-----------------------------------------------------	                    
--[[ Wisp Projector 
	                or UniqueIndex == 319 -- Wisp Projector
	                    and AbsorbLightningPercent >= 10 --note: 10-20 light absorb
	                    and MagicFind >= 10 --note: 10-20 MF
--]]
-----------------------------------------------------
				)
		}
}