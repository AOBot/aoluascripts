-- expansion\unique\shields.lua "moderate"

unique.shields =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- ** Normal Shields ** --
-----------------------------------------------------
--[[ Pelta Lunata (Buckler)
	["buc"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and DefensePercent >= 30 --note: 30-40 ed
					or not Ethereal
						and DefensePercent >= 35 --note: 30-40 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Umbral Disk (Small Shield)
	["sml"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and DefensePercent >= 40 --note: 40-50 ed
					or not Ethereal
						and DefensePercent >= 45 --note: 40-50 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Stormguild (Large Shield)
	["lrg"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and DefensePercent >= 50 --note: 50-60 ed
					or not Ethereal
						and DefensePercent >= 55 --note: 50-60 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Steelclash (Kite Shield)
	["kit"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and DefensePercent >= 60 --note: 60-100 ed
					or not Ethereal
						and DefensePercent >= 80 --note: 60-100 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Swordback Hold (Spiked Shield)
	["spk"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and DefensePercent >= 30 --note: 30-60 ed
					or not Ethereal
						and DefensePercent >= 45 --note: 30-60 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Bverrit Keep (Tower Shield)
	["tow"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and DefensePercent >= 80 --note: 80-120 ed
					or not Ethereal
						and DefensePercent >= 100 --note: 80-120 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Wall of the Eyeless (Bone Shield)
	["bsh"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and DefensePercent >= 30 --note: 30-40 ed
					or not Ethereal
						and DefensePercent >= 35 --note: 30-40 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ The Ward (Gothic Shield)
	["gts"] = 
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and AllResist >= 45 --note: 30-50 all res
					or not Ethereal
						and AllResist >= 50 --note: 30-50 all res
				)
		},	
--]]
-----------------------------------------------------
-- ** Exceptional Shields ** --
-----------------------------------------------------
--[[ Visceratuant (Defender)
	["xuc"] = 
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 100 --note: 100-150 ed
					or not Ethereal
						and DefensePercent >= 125 --note: 100-150 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Moser's Blessed Circle (Round Shield)
	["xml"] = 
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 180 --note: 180-220 ed
					or not Ethereal
						and DefensePercent >= 200 --note: 180-220 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Stormchaser (Scutum)
	["xrg"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 160 --note: 160-220 ed
					or not Ethereal
						and DefensePercent >= 190 --note: 160-220 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Tiamat's Rebuke (Dragon Shield)
	["xit"] = 
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 140 --note: 140-200 ed
						--and AllResist >= 25 --note: 25-35 all res
					or not Ethereal
						and DefensePercent >= 170 --note: 140-200 ed
						and AllResist >= 30 --note: 25-35 all res
				)
		},	
--]]
-----------------------------------------------------
--[[ Lance Guard (Barbed Shield)
	["xpk"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 70 --note: 70-120 ed
					or not Ethereal
						and DefensePercent >= 95 --note: 70-120 ed
				)
		},	
--]]
-----------------------------------------------------
--[[ Gerke's Sanctuary (Pavise)
	["xow"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 180 --note: 180-240 ed
						--and AllResist >= 20 --note: 20-30 all res
					    --and DamageReduction >= 11 --note: 11-16 dam redu
					    --and MagicDamageReduction >= 14 --note: 14-18 mdr
					or not Ethereal
						--and DefensePercent >= 210 --note: 180-240 ed
						and AllResist >= 26 --note: 20-30 all res
					    and DamageReduction >= 14 --note: 11-16 dam redu
					    and MagicDamageReduction >= 16 --note: 14-18 mdr
				)
		},	
--]]
-----------------------------------------------------
---[[ Lidless Wall (Grim Shield) 
	["xsh"] =  
		{ 
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 80 --note: 80-130 ed
						--and ManaAfterKill >= 3 --note: 3-5 mana ea kill
					or not Ethereal
						and DefensePercent >= 105 --note: 80-130 ed
						--and ManaAfterKill >= 4 --note: 3-5 mana ea kill
				)
		},
--]]
-----------------------------------------------------
--[[ Radament's Sphere (Ancient Shield)
	["xts"] = 
		{ 
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 160 --note: 160-200 ed
					or not Ethereal
						and DefensePercent >= 180 --note: 160-200 ed
				)
		},	
--]]
-----------------------------------------------------
-- ** Elite Shields ** --
-----------------------------------------------------
--[[ Blackoak Shield (Luna)
	["uml"] = 
		{ 
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 160 --note: 160-200 ed
					or not Ethereal
						and DefensePercent >= 180 --note: 160-200 ed
				)
		},	
--]]
-----------------------------------------------------
---[[ Stormshield (Monarch) (Indestructible)
	["uit"] = 
		{ 
			priority = 6, identify = true,
			goodItem = 
				( Unidentified			
					or BaseDefense >= 133 --note: 133-148 base def
				)
		},
--]]
-----------------------------------------------------
---[[ Spike Thorn (Blade Barrier)
	["upk"] = 
		{ 
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 120 --note: 120-150 ed
						--and DamageResist >= 15 --note: 15-20 dam redu
					or not Ethereal
						--and DefensePercent >= 135 --note: 120-150 ed
						and DamageResist >= 18 --note: 15-20 dam redu
				)
		},	
--]]
-----------------------------------------------------
---[[ Medusa's Gaze (Aegis) 
	["uow"] = 
		{ 
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 150 --note: 150-180 ed
						--and LifeDrainMinDamage >= 5 --note: 5-9 lifestolen
						--and ColdResist >= 40 --note: 40-80 cold res
					or not Ethereal
						--and DefensePercent >= 165 --note: 150-180 ed
						and LifeDrainMinDamage >= 8 --note: 5-9 lifestolen
						and ColdResist >= 65 --note: 40-80 cold res
				)
		},	
--]]
-----------------------------------------------------
---[[ Head Hunter's Glory (Troll Nest)
	["ush"] = 
		{ 
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and ArmorClass >= 320 --note: 320-420 ed
						--and ArmorClassVsMissile >= 300 -- 300-350 def vs mis
						--and FireResist >= 20 --note: 20-30 fire res
						--and PoisonResist >= 30 --note: 30-40 pois res
						--and HealAfterKill >= 5 --note: 5-7 life after kill
						--and Sockets >= 1 --note: 1-3 sock
					or not Ethereal
						--and ArmorClass >= 380 --note: 320-420 ed
						--and ArmorClassVsMissile >= 325 -- 300-350 def vs mis
						--and FireResist >= 25 --note: 20-30 fire res
						--and PoisonResist >= 35 --note: 30-40 pois res
						--and HealAfterKill >= 7 --note: 5-7 life after kill
						and Sockets >= 3 --note: 1-3 sock
				)
		},	
--]]
-----------------------------------------------------
---[[ Spirit Ward (Ward)
	["uts"] = 
		{ 
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 130 --note: 130-180 ed
						--and ToBlock >= 20 --note: 20-30 chance of block
						--and AllResist >= 30 --note: 30-40 all res
						--and AbsorbCold >= 6 --note: 6-11 cold absorb
					or not Ethereal
						--and DefensePercent >= 150 --note: 130-180 ed
						--and ToBlock >= 20 --note: 20-30 chance of block
						and AllResist >= 35 --note: 30-40 all res
						--and AbsorbCold >= 8 --note: 6-11 cold absorb
				)
		},	
--]]
-----------------------------------------------------
-- ** Necromancer Shields ** --
-----------------------------------------------------
---[[ Homunculus (Hierophant Trophy)
	["nea"] = 
		{ 
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 150 --note: 150-200 ed
					or not Ethereal
						and DefensePercent >= 175 --note: 150-200 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Darkforce Spawn (Bloodlord Skull)
	["nef"] = 
		{ 
			priority = 8, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 140 --note: 140-180 ed
					    --and (NecroSummoningTab + PoisonAndBoneTab) >= 3
						--and NecroSummoningTab >= 1 --note: 1-3 NecSummon
						--and PoisonAndBoneTab >= 1 --note: 1-3 PnB Skill
						--and CursesTab >= 1 --note: 1-3 Curses
					or not Ethereal
						--and DefensePercent >= 160 --note: 140-180 ed
					    and (NecroSummoningTab + PoisonAndBoneTab) >= 3
						--and NecroSummoningTab >= 2 --note: 1-3 NecSummon
						--and PoisonAndBoneTab >= 2 --note: 1-3 PnB Skill
						--and CursesTab >= 1 --note: 1-3 Curses
				)
		},
--]]
-----------------------------------------------------
---[[ Boneflame (Succubus Skull)
	["nee"] = 
		{ 
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and ClassSkillsBonus >= 2 --note: 2-3 Necro Skill
						--and AllResist >= 20 --note: 20-30 all res
						--and DefensePercent >= 120 --note: 120-150 ed
					or not Ethereal
						--and ClassSkillsBonus >= 2 --note: 2-3 Necro Skill
						--and AllResist >= 25 --note: 20-30 all res
						--and DefensePercent >= 135 --note: 120-150 ed
				)
		},
--]]
-----------------------------------------------------
-- ** Paladin Shields ** --
-----------------------------------------------------
---[[ Alma Negra (Sacred Rondache)
	["pac"] = 
		{ 
			priority = 5, identify = true,
			goodItem =
				( Unidentified 
					or Ethereal
						--and ClassSkillsBonus >= 1 --note: 1-2 Paladin Skill
						--and DamagePercent >= 40 --note: 40-75 edam
						--and ToHitPercent >= 40 --note: 40-75 bonus attack
						--and DefensePercent >= 180 --note: 180-210 ed
					    --and MagicDamageReduction >= 5 --note: 5-9 mdr
					or not Ethereal
						and ClassSkillsBonus >= 2 --note: 1-2 Paladin Skill
						--and DamagePercent >= 58 --note: 40-75 edam
						--and ToHitPercent >= 58 --note: 40-75 bonus attack
						--and DefensePercent >= 205 --note: 180-210 ed
					    --and MagicDamageReduction >= 7 --note: 5-9 mdr
				)
		},
--]]
-----------------------------------------------------
---[[ Herald Of Zakarum (Gilded Shield)
	["pa9"] = 
		{ 
			priority = 9, identify = false,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 150 --note: 150-200 ed
					or not Ethereal
						--and DefensePercent >= 175 --note: 150-200 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Dragonscale (Zakarum Shield)
	["pae"] = 
		{ 
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 150 --note: 150-200 ed
						--and Strength >= 15 --note: 15-25 str
						--and AbsorbFirePercent >= 10 --note: 10-20 fire absorb
					or not Ethereal
						--and DefensePercent >= 175 --note: 150-200 ed
						--and Strength >= 20 --note: 15-25 str
						--and AbsorbFirePercent >= 15 --note: 10-20 fire absorb
				)
		},
--]]
-----------------------------------------------------
}