-- expansion\unique\helms.lua "moderate"

unique.helms =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- ** Normal Helms ** --
-----------------------------------------------------
--[[ Biggin's Bonnet (Cap)
	["cap"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and TotalDefense >= 17 --note: 17-19 total def
					or not Ethereal
						and TotalDefense >= 18 --note: 17-19 total def
				)
		},	
--]]
-----------------------------------------------------
--[[ Tarnhelm (Skull Cap)
	["skp"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and BaseDefense >= 8 --note: 8-11 base def
					    and MagicFind >= 50 --note: 25-50 mf
					or not Ethereal
						and BaseDefense >= 8 --note: 8-11 base def
					    and MagicFind >= 50 --note: 25-50 mf
				)
		},	
--]]
-----------------------------------------------------
--[[ Coif of Glory (Helm)
	["hlm"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and TotalDefense >= 25 --note: 25-28 total def
					or not Ethereal
						and TotalDefense >= 27 --note: 25-28 total def
				)
		},
--]]
-----------------------------------------------------
--[[ Duskdeep (Full Helm)
	["fhl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and TotalDefense >= 45 --note: 45-60 total def
					or not Ethereal
						and TotalDefense >= 53 --note: 45-60 total def
				)
		},	
--]]
-----------------------------------------------------
--[[ Howltusk (Great Helm)
	["ghm"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
					or not Ethereal
			--note: all the same stats, unless ethereal
				)
		},	
--]]
-----------------------------------------------------
--[[ The Face of Horror (Mask)
	["msk"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and TotalDefense >= 34 --note: 34-52 total def
					or not Ethereal
						and TotalDefense >= 43 --note: 34-52 total def
				)
		},	
--]]
-----------------------------------------------------
--[[ Undead Crown (Crown)
	["crn"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and TotalDefense >= 99 --note: 99-113 total def
					    and UndeadToHit >= 50 --note: 50-100 AR to undead
					or not Ethereal
						and TotalDefense >= 99 --note: 99-113 total def
					    and UndeadToHit >= 100 --note: 50-100 AR to undead
				)
		},	
--]]
-----------------------------------------------------
--[[ Wormskull (Bone Helm)
	["bhm"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and BaseDefense >= 33 --note: 33-36 base def
					or not Ethereal
						and BaseDefense >= 35 --note: 33-36 base def
				)
		},	
--]]
-----------------------------------------------------
-- ** Exceptional Helms ** --
-----------------------------------------------------
--[[ Peasant Crown (War Hat)
	["xap"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and LifeRegen >= 6 --note: 6-12 replen life
					or not Ethereal
						and LifeRegen >= 9 --note: 6-12 replen life
				)
		},	
--]]
-----------------------------------------------------
---[[ Rockstopper (Sallet)
	["xkp"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				    	--and DefensePercent >= 160 --note: 160-220 ed
					    and TotalResist >= 100 --note: 60-130 total res
						--and ColdResist >= 20 --note: 20-40 cold res
						--and FireResist >= 20 --note: 20-50 fire res
						--and LightResist >= 20 --note: 20-40 light res
					--or not Ethereal
						--and DefensePercent >= 160 --note: 160-220 ed
					    --and TotalResist >= 95 --note: 60-130 total res
						--and ColdResist >= 30 --note: 20-40 cold res
						--and FireResist >= 35 --note: 20-50 fire res
						--and LightResist >= 30 --note: 20-40 light res
				)
		},	
--]]
-----------------------------------------------------
--[[ Stealskull (Casque)
	["xlm"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 200 --note: 200-240 ed
					    --and MagicFind >= 30 --note: 30-50 mf
					or not Ethereal
						--and DefensePercent >= 220 --note: 200-240 ed
					    and MagicFind >= 40 --note: 30-50 mf
				)
		},
--]]
-----------------------------------------------------
--[[ Darksight Helm (Basinet)
	["xhl"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and FireResist >= 20 --note: 20-40 fire res
					or not Ethereal
						--and FireResist >= 30 --note: 20-40 fire res
				)
		},	
--]]
-----------------------------------------------------
--[[ Valkyrie Wing (Winged Helm)
	["xhm"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and ClassSkillsBonus >= 1 --note: 1-2 Amazon skill
						--and DefensePercent >= 150 --note: 150-200 ed
						--and ManaAfterKill >= 2 --note: 2-4 mana after kill
					or not Ethereal
						and ClassSkillsBonus >= 2 --note: 1-2 Amazon skill
						and DefensePercent >= 175 --note: 150-200 ed
						--and ManaAfterKill >= 3 --note: 2-4 mana after kill
				)
		},
--]]
-----------------------------------------------------
---[[ Blackhorn's Face (Death Mask)
	["xsk"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and DefensePercent >= 200 --note: 180-220 ed
					--or not Ethereal
						--and DefensePercent >= 200 --note: 180-220 ed
				)
		},	
--]]
-----------------------------------------------------
---[[ Crown of Thieves (Grand Crown)
	["xrn"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						and LifeDrainMinDamage >= 11 --note: 9-12 lifestolen
					    --and DefensePercent >= 160 --note: 160-200 ed
						--and GoldFind >= 80 --note: 80-100 extra gold
					--or not Ethereal
						--and LifeDrainMinDamage >= 11 --note: 9-12 lifestolen
					    --and DefensePercent >= 180 --note: 160-200 ed
						--and GoldFind >= 90 --note: 80-100 extra gold
				)
		},
--]]
-----------------------------------------------------
---[[ Vampire Gaze (Grim Helm)
	["xh9"] =
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DamageResist >= 15 --note: 15-20 dam reduc
						--and LifeDrainMinDamage >= 6 --note: 6-8 lifestolen
						--and ManaDrainMinDamage >= 6 --note: 6-8 manastolen
						--and MagicDamageReduction >= 10 --note: 10-15 magic redu
					or not Ethereal
						and DamageResist >= 17 --note: 15-20 dam reduc
						and LifeDrainMinDamage >= 8 --note: 6-8 lifestolen
						--and ManaDrainMinDamage >= 7 --note: 6-8 manastolen
						--and MagicDamageReduction >= 13 --note: 10-15 magic redu
				)
		},
--]]
-----------------------------------------------------
-- ** Elite Helms ** --
-----------------------------------------------------
---[[ Harlequin Crest (Shako)
	["uap"] =
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
					or not Ethereal
						--and BaseDefense >= 110 --note: 98-141 base def
					or Ethereal
						and BaseDefense >= 179 --note: 147-211 eth base def?
				)
		},
--]]
-----------------------------------------------------
--[[ Steel Shade (Armet)
	["ulm"] =
		{
			priority = 4, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 100 --note: 100-130 ed
						--and LifeRegen >= 10 --note: 10-18 replen life
						--and ManaDrainMinDamage >= 4 --note: 4-8 manastolen
						--and AbsorbFire >= 5 --note: 5-11 fire absorb
					or not Ethereal
						--and DefensePercent >= 115 --note: 100-130 ed
						--and LifeRegen >= 14 --note: 10-18 replen life
						--and ManaDrainMinDamage >= 6 --note: 4-8 manastolen
						--and AbsorbFire >= 8 --note: 5-11 fire absorb
				)
		},	
--]]
-----------------------------------------------------
---[[ Veil of Steel or Nightwing's Veil (Spired Helm)
	["uhm"] =
		{
			priority = 8, identify = true,
			goodItem =
				( Unidentified
					or UniqueIndex == 343 and Ethereal-- Nightwing's Veil
						--and PassiveColdMastery >= 8 --note: 8-15 coldskill dam
						--and DefensePercent >= 90 --note: 90-120 ed
						--and Dexterity >= 10 --note: 10-20 dex
						--and AbsorbCold >= 5 --note: 5-9 cold absorb
					or UniqueIndex == 343 and not Ethereal-- Nightwing's Veil
						--and PassiveColdMastery >= 12 --note: 8-15 coldskill dam
						--and DefensePercent >= 105 --note: 90-120 ed
						--and Dexterity >= 15 --note: 10-20 dex
						--and AbsorbCold >= 8 --note: 5-9 cold absorb
						and (PassiveColdMastery >= 12 or Dexterity >= 15)
						
					or UniqueIndex == 249 and Ethereal-- Veil of Steel
					or UniqueIndex == 249 and not Ethereal-- Veil of Steel
                    --note: all the same stats, unless ethereal
				)
		},
--]]
-----------------------------------------------------
---[[ Andariel's Visage (Demonhead)
	["usk"] =
		{
			priority = 8, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 100 --note: 100-150 ed
						--and (LifeDrainMinDamage + Strength) >= 33 --note: 33-40
						--and Strength >= 25 --note: 25-30 str
						--and LifeDrainMinDamage >= 8 --note: 8-10 lifestolen
					or not Ethereal
						--and DefensePercent >= 125 --note: 100-150 ed
						and (LifeDrainMinDamage + Strength) >= 36 --note: 33-40
						--and Strength >= 27 --note: 25-30 str
						--and LifeDrainMinDamage >= 9 --note: 8-10 lifestolen
				)
		},
--]]
-----------------------------------------------------
---[[ Crown of Ages (Corona) (Indestructible)
	["urn"] =
		{
			priority = 8, identify = false,
			goodItem =
				( Unidentified
					or TotalDefense >= 349 --note: 349-399 total def
						--and DamageResist >= 13 --note: 10-15 dam reduc
						--and AllResist >= 25 --note: 20-30 all res
						--and Sockets >= 2 --note: 1-2 sock
				)
		},	
--]]
-----------------------------------------------------
---[[ Giant Skull (Bone Visage)
	["uh9"] =
		{
			priority = 7, identify = true,
			goodItem =
				( Unidentified
				    or Ethereal
				        --and Sockets >= 1 --note: 1-2 sock
				        --and Strength >= 25 --note: 25-35 str
				        --and TotalDefense >= 525 --note: 525-715 eth total def?
				    or not Ethereal
						and Sockets >= 2
						--and Strength >= 30 --note: 25-35 str
						--and TotalDefense >= 425 --note: 350-477 total def
				)
		},
--]]
-----------------------------------------------------
-- ** Circlets ** --
-----------------------------------------------------
---[[ Kira's Guardian (Tiara)
	["ci2"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and AllResist >= 50 --note: 50-70 all res
						--and TotalDefense >= 90 --note: 90-170 total def
					or not Ethereal
						and AllResist >= 60 --note: 50-70 all res
						--and TotalDefense >= 130 --note: 90-170 total def
				)
		},
--]]
-----------------------------------------------------
---[[ Griffon's Eye (Diadem)
	["ci3"] =
	    {
			priority = 10, identify = false,
			goodItem =
				( Unidentified
					or Ethereal
						--and TotalDefense >= 150 --note: 150-260 total def
						--and PassiveLightningPierce >= 15 --note: 15-20 -lightres
						--and PassiveLightningMastery >= 10 --note: 10-15 light dam
					or not Ethereal
						--and TotalDefense >= 205 --note: 150-260 total def
						--and PassiveLightningPierce >= 17 --note: 15-20 -lightres
						--and PassiveLightningMastery >= 13 --note: 10-15 light dam
				)
		},
--]]
-----------------------------------------------------
-- ** Barbarian Helms ** --
-----------------------------------------------------
---[[ Arreat's Face (Slayer Guard)
	["baa"] =
		{
			priority = 6, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 150 --note: 150-200 ed
						--and LifeDrainMinDamage >= 3 --note: 3-6 lifestolen
					or not Ethereal
						and DefensePercent >= 175 --note: 150-200 ed
						and LifeDrainMinDamage >= 5 --note: 3-6 lifestolen
				)
		},
--]]
-----------------------------------------------------
---[[ Wolfhowl (Fury Visor)
	["bac"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and WarcriesTab >= 2 --note: 2-3 Warcries
					    --and DefensePercent >= 120 --note: 120-150 ed
						--and FeralRageSkill >= 3 --note: 3-6 FeralRage
						--and ShapeShiftingSkill >= 3 --note: 3-6 Lycan
						--and WerewolfSkill >= 3 --note: 3-6 Werewolf
						--and Strength >= 8 --note: 8-15 str
						--and Dexterity >= 8 --note: 8-15 dex
						--and Vitality >= 8 --note: 8-15 vit
					or not Ethereal
						and WarcriesTab >= 3 --note: 2-3 Warcries
					    --and DefensePercent >= 135 --note: 120-150 ed
						--and FeralRageSkill >= 5 --note: 3-6 FeralRage
						--and ShapeShiftingSkill >= 5 --note: 3-6 Lycan
						--and WerewolfSkill >= 5 --note: 3-6 Werewolf
						--and Strength >= 12 --note: 8-15 str
						--and Dexterity >= 12 --note: 8-15 dex
						--and Vitality >= 11 --note: 8-15 vit
				)
		},
--]]
-----------------------------------------------------
---[[ Demonhorn's Edge (Destroyer Helm)
	["bad"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 120 --note: 120-160 ed
						--and LifeDrainMinDamage >= 3 --note: 3-6 lifestolen
						--and AttackerTakesDamage >= 55 --note: 55-77 attacker takes
						--and WarcriesTab >= 1 --note: 1-3 Warcries
						--and MasteriesTab >= 1 --note: 1-3 Masteries
						--and BarbarianCombatTab >= 1 --note: 1-3 BarbCombat
					or not Ethereal
						--and DefensePercent >= 140 --note: 120-160 ed
						--and LifeDrainMinDamage >= 5 --note: 3-6 lifestolen
						--and AttackerTakesDamage >= 66 --note: 55-77 attacker takes
						and WarcriesTab >= 2 --note: 1-3 Warcries
						--and MasteriesTab >= 2 --note: 1-3 Masteries
						and BarbarianCombatTab >= 2 --note: 1-3 BarbCombat
				)
		},
--]]
-----------------------------------------------------
---[[ Halaberd's Reign (Conqueror Crown)
	["bae"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 140 --note: 140-170 ed
					    --and LifeRegen >= 15 --note: 15-23 replen life
					    --and BattleCommandSkill >= 1 --note: 1-2 BattleCommand
					    --and BattleOrdersSkill >= 1 --note: 1-2 BattleOrders
					or not Ethereal
						--and DefensePercent >= 155 --note: 140-170 ed
					    --and LifeRegen >= 19 --note: 15-23 replen life
					    --and BattleCommandSkill >= 2 --note: 1-2 BattleCommand
					    and BattleOrdersSkill >= 2 --note: 1-2 BattleOrders
				)
		},
--]]
-----------------------------------------------------
-- ** Druid Pelts ** --
-----------------------------------------------------
---[[ Jalal's Mane (Totemic Mask)
	["dra"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 150 --note: 150-200 ed
					or not Ethereal
						and DefensePercent >= 175 --note: 150-200 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Cerebus' Bite (Blood Spirit)
	["drb"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and ShapeShiftingTab >= 2 --note: 2-4 ShapeShift
					    --and FeralRageSkill >= 1 --note: 1-2 FeralRage
					    --and DefensePercent >= 130 --note: 130-140 ed
						--and ToHitPercent >= 60 --note: 60-120 AR
						--and LifeDrainMinDamage >= 7 --note: 7-10 lifestolen
					or not Ethereal
						and ShapeShiftingTab >= 3 --note: 2-4 ShapeShift
					    --and FeralRageSkill >= 2 --note: 1-2 FeralRage
					    --and DefensePercent >= 135 --note: 130-140 ed
						--and ToHitPercent >= 90 --note: 60-120 AR
						and LifeDrainMinDamage >= 9 --note: 7-10 lifestolen
				)
		},
--]]
-----------------------------------------------------
---[[ Ravenlore (Sky Spirit)
	["dre"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and PassiveFirePierce >= 10 --note: 10-20 -fireres
						--and AllResist >= 15 --note: 15-25 all res
						--and Energy >= 20 --note: 20-30 energy
						--and DefensePercent >= 120 --note: 120-150 ed
					or not Ethereal
						and PassiveFirePierce >= 16 --note: 10-20 -fireres
						--and AllResist >= 20 --note: 15-25 all res
						--and Energy >= 25 --note: 20-30 energy
						--and DefensePercent >= 135 --note: 120-150 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Spirit Keeper (Earth Spirit)
	["drd"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or Ethereal
						--and DefensePercent >= 170 --note: 170-190 ed
					    --and ClassSkillsBonus >= 1 --note: 1-2 Druid skill
					    --and FireResist >= 30 --note: 30-40 fire res
					    --and AbsorbLight >= 9 --note: 9-14 light absorb
					    --and AbsorbColdPercent >= 15 --note: 15-25 cold absorb
					or not Ethereal
						--and DefensePercent >= 170 --note: 170-190 ed
					    and ClassSkillsBonus >= 2 --note: 1-2 Druid skill
					    --and FireResist >= 35 --note: 30-40 fire res
					    --and AbsorbLight >= 12 --note: 9-14 light absorb
					    --and AbsorbColdPercent >= 20 --note: 15-25 cold absorb
				)
		},
--]]
-----------------------------------------------------
}