-- expansion\unique\amulets.lua "moderate"

unique.amulets =
{ checkStats = _CheckUniqueItemStats,
	["amu"] = ------ Amulet
		{ priority = 8, identify = true,
			goodItem =
				( Unidentified
-----------------------------------------------------
-- This part keeps amulets regardless of stats
-- Note: this part overrides the next part
-----------------------------------------------------
					--or UniqueIndex == 117 -- Nokozan Relic
					--or UniqueIndex == 118 -- The Eye of Etlich
					--or UniqueIndex == 119 -- The Mahim-Oak Curio
					--or UniqueIndex == 269 -- The Cat's Eye
					--or UniqueIndex == 270 -- The Rising Sun
					--or UniqueIndex == 271 -- Crescent Moon
					or UniqueIndex == 272 -- Mara's Kaleidoscope
					--or UniqueIndex == 273 -- Atma's Scarab
					or UniqueIndex == 276 -- Highlord's Wrath
					--or UniqueIndex == 277 -- Saracen's Chance
					--or UniqueIndex == 302 -- Seraph's Hymn
					or UniqueIndex == 375 -- Metalgrid
					
-----------------------------------------------------
-- This part keeps amulets with specific stats
-----------------------------------------------------
	-- Nokozan Relic (all the same) (see UniqueIndex list above)
-----------------------------------------------------	
--[[ The Eye of Etlich 
	                or UniqueIndex == 118 -- The Eye of Etlich
	                    --and LightRadius >= 1 -- 1-5 light rad
	                    and LifeDrainMinDamage >= 5 -- 3-7 lifestolen
	                    --and ColdMinDamage >= 1 -- 1-2 min cold dmg
	                    --and ColdMaxDamage >= 3 -- 3-5 max cold dmg
	                    and ArmorClassVsMissile >= 10 -- 10-40 def vs mis
--]]
-----------------------------------------------------	                    
	-- The Mahim-Oak Curio (all the same) (see UniqueIndex list above)
-----------------------------------------------------	
---[[ Saracen's Chance 
					or UniqueIndex == 277 -- Saracen's Chance
						and AllResist >= 25 -- 15-25 all res
--]]
-----------------------------------------------------						
	-- The Cat's Eye (all the same) (see UniqueIndex list above)
-----------------------------------------------------	
---[[ Crescent Moon 
					or UniqueIndex == 271 -- Crescent Moon
						and LifeDrainMinDamage >= 6 -- 3-6 lifestolen
						and ManaDrainMinDamage >= 13 -- 11-15 manastolen
--]]
-----------------------------------------------------						
	-- Atma's Scarab (all the same) (see UniqueIndex list above)
-----------------------------------------------------	
---[[ The Rising Sun 
	                or UniqueIndex == 270 -- The Rising Sun
	                    and SkillOnGetHit >= 19 -- 13-19 meteor when struck
--]]
-----------------------------------------------------	                    
	-- Highlord's Wrath (all the same) (see UniqueIndex list above)
-----------------------------------------------------	
--[[ Mara's Kaleidoscope 
	                or UniqueIndex == 272 -- Mara's Kaleidoscope
	                    and AllResist >= 20 -- 20-30 all res
--]]
-----------------------------------------------------	                    
---[[ Seraph's Hymn 
	                or UniqueIndex == 302 -- Seraph's Hymn
	                    and DefensiveAurasTab >= 2 -- 1-2 DefAura skill (pal only)
	                    --and DemonDamagePercent >= 20 -- 20-50 dmg to demon
	                    --and DemonToHit >= 150 -- 150-250 AR to demon
	                    --and UndeadDamagepercent >= 20 -- 20-50 dmg to undead
	                    --and UndeadToHit >= 150 -- 150-250 AR to undead
--]]
-----------------------------------------------------	                    
--[[ Metalgrid 
	                or UniqueIndex == 375 -- Metalgrid
	                    and ToHit >= 400 -- 400-450 AR
	                    and AllResist >= 25 -- 25-35 all res
	                    and ArmorClass >= 300 -- 300-350 def
--]]
-----------------------------------------------------
				)
		}
}