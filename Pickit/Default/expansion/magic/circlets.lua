-- expansion\magic\circlets.lua "moderate"

local isGoodMagicCirclet =
function(item)
-- Good Skill Amount Settings
	local minSkill = 2 -- Class Skills
	local minTab = 3 -- Skill Tab
------------------------------------------------------------------------------
	local prefix, casterSkill, meleeSkill, suffix = false, false, false, false
	local fcr, frw = false, false

	prefix =
		( ClassSkillsBonus >= minSkill
			or SkillTabBonus >= minTab
			or Sockets >= 3
		)
		
	casterSkill =
		( PaladinSkills >= minSkill
			or SorceressSkills >= minSkill
			or DruidSkills >= minSkill
			or AssassinSkills >= minSkill
			or NecromancerSkills >= minSkill

			or PaladinCombatTab >= minTab
	      	or LightningTab >= minTab
			or OffensiveAurasTab >= minTab
	        --or DefensiveAurasTab >= minTab
	        or FireTab >= minTab
	        or ColdTab >= minTab
	        --or CursesTab >= minTab
	        or PoisonAndBoneTab >= minTab
	        or NecroSummoningTab >= minTab
	        or WarcriesTab >= minTab
	        or ElementalTab >= minTab
		)
			
	meleeSkill =
	    ( BarbarianSkills >= minSkill
	        or AmazonSkills >= minSkill
			or PaladinSkills >= minSkill
			or DruidSkills >= minSkill
			or AssassinSkills >= minSkill
			
			or PaladinCombatTab >= minTab
	        or BowAndCrossBowTab >= minTab
	        or PassiveAndMagicTab >= minTab
	        or JavelinAndSpearTab >= minTab
	        or BarbarianCombatTab >= minTab
	        or MasteriesTab >= minTab
	        or ShapeShiftingTab >= minTab
	        or ShadowDisciplinesTab >= minTab
	        or MartialArtsTab >= minTab
		)
		
	suffix =
		( MaxLife >= 61
			or Strength >= 21
			or Dexterity >= 21
			--or Energy >= 21
			or MagicFind >= 26
			--or DamageReduction >= 18
			or LifePerLevel ~= 0
			--or ManaPerLevel ~= 0
			--or PoisonLengthReduction >= 75
			--or MaxDamage >= 10
			--or MinDamage >= 10
		)

	fcr = FasterCastRate >= 20
	
	frw = FasterMoveVelocity >= 30	
	
	return
		( Unidentified
			or (casterSkill or Sockets >= 3) 
				and (fcr or suffix)
			or (meleeSkill or Sockets >= 3) 
				and (frw or suffix)
			--or prefix 
				--and (suffix or fcr or frw)
		)
end

magic.circlets =
{ checkStats = true,
	["Circlet"] = -- Circlets
		{ priority = 3, identify = true,
			isGoodItem = isGoodMagicCirclet
		}
}