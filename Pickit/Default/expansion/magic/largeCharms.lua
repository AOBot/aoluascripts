-- expansion\magic\largeCharms.lua "moderate"

-- Large Charms For Reroll Settings
local minItemLvl = 91 --note: forRerolling
local keepRerollUnid = (ItemLevel < minItemLvl)
local forRerolling = ItemLevel >= minItemLvl

local isGoodLargeCharm =
function(item)
	local greatMod, prefix, suffix = false, false, false
	greatMod =
		( MaxDamage >= 6
				and ToHit >= 38
			or MaxDamage >= 7
			or ColdMaxDamage >= 30 -- 30-42
			or FireMaxDamage >= 43 -- 40-60
			or LightMaxDamage >= 90 -- 85-128
			or PoisonMaxDamage >= 175 -- 200-...
			or AllResist >= 8
			or SingleResist >= 15
			)
	prefix =
		( AllResist >= 6
			or SingleResist >= 14
			or ToHit >= 70
			or ArmorClass >= 53
			--or MaxMana >= 30
			or MaxDamage >= 4
			--or MinDamage >= 2
			--or ColdMaxDamage >= 26
			--or FireMaxDamage >= 36
			--or LightMaxDamage >= 79
			or PoisonMaxDamage >= 175
		)
	suffix =
		( MaxLife >= 30
			or Strength >= 4
			or Dexterity >= 4
			or FasterHitRecovery ~= 0
			or FasterMoveVelocity ~= 0
		)
	return
		( Unidentified
		    --or forRerolling
			or greatMod
			or prefix and suffix
			--or prefix or suffix
		)
end

magic.largeCharms =
{ checkStats = true,
	["cm2"] = -- Large Charm
		{ priority = 3, identify = true,
			isGoodItem = isGoodLargeCharm
		}
}

-----------------------------------------------------------------------------
-- Large Charm Good Affix Reference
-----------------------------------------------------------------------------
-- Alvl:   Lowest Ilvl that affix can spawn
-- LvlReq: Level Req item will have if that affix spawns.
-- Freq:   Affix's chance to appear.
-- Formula: Sum of All Group (Prefix or Suffix) Freqs / That Affix Freq
-- Prefix Freqs @ Ilvl 99 = 186 (Prefix/Suffix groups calc'd separately).
-- Suffix Freqs @ Ilvl 99 = 84
-- Ex: Stalwart: (186/4) = 1:46.5 or 2.15% chance.
-----------------------------------------------------------------------------
-- General Prefixes
-----------------------------------------------------------------------------
-- Name         Alvl    LvlReq  Freq  Stats
-- Stalwart     45      37      4     45-60 Defense
-- Sanguinary   60      52      4     2 MinDmg
-- Steel        52      44      4     65-77 AR
-- Sharp        28      21      4     21-48 AR, +4-6 MaxDmg
-- Serpent's    49      41      4     30-34 Mana
-- Shimmering   34      26      1     6-8% All Resist
-- Sapphire     35      27      2     13-15% Cold Res
-- Ruby         35      27      2     13-15% Fire Res
-- Amber        35      27      2     13-15% Light Res
-- Emerald      35      27      2     13-15% Poison Res
-----------------------------------------------------------------------------
-- General Suffixes
-----------------------------------------------------------------------------
-- Balance      19      14      4     8% FHR
-- Vita         58      50      4     26-30 Life
-- Vita         74      66      4     31-35 Life
-- Strength     18      13      4     4-5 Str
-- Dexterity    18      13      4     4-5 Dex
-- Inertia      24      18      4     5% FRW
-----------------------------------------------------------------------------
-- Elemental Affixes - Or, why these sometimes don't suck.
-----------------------------------------------------------------------------
-- Elemental Damage Prefixes = Only real reason to keep a LC.
-----------------------------------------------------------------------------
-- Hibernal     69      61      1     11-15 to 21-30 Cold Dmg/1 Sec
-- Flaming      67      59      1     10-27 to 28-43 Fire Dmg
-- Arcing       48      40      1     1 to 27-58 Lightning Dmg
-- Shocking     68      60      1     1 to 59-90 Lightning Dmg
-- Toxic        46      38      1     90 Poison Dmg/5 Sec.
-- Pestilent    66      58      1     175 Poison Dmg/6 Sec.
-----------------------------------------------------------------------------
-- Elemental Damage Suffixes. Crappy unless paired w/same-element prefix.
-----------------------------------------------------------------------------
-- Winter       35      27      1     4-6 to 9-12 Cold Dmg/1 Sec.
-- Incineration 34      26      1     6-10 to 12-17 Fire Dmg
-- Storms       34      26      1     1 to 26-38 Lightning Dmg
-- Anthrax      33      25      1     50 Poison Dmg/6 Sec.
-----------------------------------------------------------------------------
-- Ilvl for GoodEleDmg=58-69 -- AllEleDmg=33-69 -- AllGoodMods==33-74
-----------------------------------------------------------------------------
-- Elemental ranges & StatTypes
-- Cold: ColdMinDamage? ColdMaxDamage? ColdMaxDamageByTime?
-- Fire: FireMinDamage
-- Lite: Min always 1, so LightMaxDamage?
-- Psn : PoisonMinDamage 16-225 CAN'T be right! SC = 100,175,313,377,451!
-- All this work for a crap-ass item. Jeez...