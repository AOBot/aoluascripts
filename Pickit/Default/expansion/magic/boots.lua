-- expansion\magic\boots.lua "moderate"

local magicCraftBoots =
{ priority = 3, identify = false,
   goodItem = ItemLevel >= 91
}

magic.boots =
{ checkStats = true,
	--["mbt"] = magicCraftBoots, -- Chain Boots / Hit Power
	["xmb"] = magicCraftBoots, -- Mesh Boots / Hit Power
	["umb"] = magicCraftBoots, -- Boneweave Boots / Hit Power
	--["tbt"] = magicCraftBoots, -- Light Plate Boots / Blood
	["xlb"] = magicCraftBoots, -- Battle Boots / Blood
	["ulb"] = magicCraftBoots, -- Mirrored Boots / Blood
	--["lbt"] = magicCraftBoots, -- Boots / Caster
	["xlb"] = magicCraftBoots, -- Demonhide Boots / Caster
	["ulb"] = magicCraftBoots, -- Wyrmhide Boots / Caster
	--["hbt"] = magicCraftBoots, -- Greaves / Safety
	["xhb"] = magicCraftBoots, -- War Boots / Safety
	["uhb"] = magicCraftBoots, -- Myrmidon Boots / Safety
}
