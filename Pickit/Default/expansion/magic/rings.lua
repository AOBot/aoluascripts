-- expansion\magic\rings.lua "moderate"

-- Rings For Craft Settings
local minItemLvl = 91 --note: forCrafting
local keepCraftUnid = (ItemLevel < minItemLvl)
local forCrafting = ItemLevel >= minItemLvl


local isGoodMagicRing =
function(item)
	local goodMod = false
	
	goodMod =
		( AllResist >= 10
				and (MagicFind >= 16 or FasterCastRate ~= 0)
			or AllResist >= 14
			or MagicFind >= 30
			or MinDamage >= 10
				and (ToHit >= 61 or AllResist >= 10)
		)

	return
		( Unidentified
		    --or forCrafting
			or goodMod
		)
end
magic.rings =
{ checkStats = true,
	["rin"] = -- Ring
		{ priority = 0, identify = true,
			isGoodItem = isGoodMagicRing
		},
}