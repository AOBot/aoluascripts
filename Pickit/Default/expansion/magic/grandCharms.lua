-- expansion\magic\grandCharms.lua "moderate"

-- Grand Charms For Reroll Settings
local minItemLvl = 99 --note: forRerolling
local keepRerollUnid = (ItemLevel < minItemLvl)
local forRerolling = ItemLevel >= minItemLvl

local isGoodGrandCharm =
function(item)
	local greatMod, prefix, suffix = false, false, false
	local greatSkill, goodSkill = false, false
	
	greatMod =
		( AllResist >= 15
			or MaxDamage >= 9
				and ToHit >= 61
			or ToHit >= 110
				and MaxLife >= 36
			or ToHit >= 125
			or MaxDamage >= 10
		)
		
	prefix =
		( AllResist >= 12
			--or SingleResist >= 28
			--or ToHit >= 103
			or MaxDamage >= 8 and ToHit >= 59
			--or MaxMana >= 53
		)
		
	suffix =
		( MaxLife >= 34
			or FasterHitRecovery ~= 0
			or FasterMoveVelocity ~= 0
			or Strength >= 5
			or Dexterity >= 5
		)
		
	greatSkill =
	    ( PaladinCombatTab ~= 0
	      	or LightningTab ~= 0
			or OffensiveAurasTab ~= 0
	        or JavelinAndSpearTab ~= 0
	        or FireTab ~= 0
	        or ColdTab ~= 0
	        or PoisonAndBoneTab ~= 0
	        or NecroSummoningTab ~= 0
	        or WarcriesTab ~= 0
	        or ElementalTab ~= 0
	        or TrapsTab ~= 0
	    )

	goodSkill =
	    ( DefensiveAurasTab ~= 0
	        or BowAndCrossBowTab ~= 0
	        or PassiveAndMagicTab ~= 0
	        or CursesTab ~= 0
	        or BarbarianCombatTab ~= 0
	        or MasteriesTab ~= 0
	        or DruidSummoningTab ~= 0
	        or ShapeShiftingTab ~= 0
	        or ShadowDisciplinesTab ~= 0
	        or MartialArtsTab ~= 0
		)
		
	return
		( Unidentified
		    --or forRerolling
			or greatMod
			--or SkillTabBonus ~= 0
			or greatSkill
			or goodSkill
				and suffix
			or prefix and suffix
		)
		
end

magic.grandCharms =
{ checkStats = true,
	["cm3"] = -- Grand Charm
		{ priority = 4, identify = keepRerollUnid,
			isGoodItem = isGoodGrandCharm
		},
}