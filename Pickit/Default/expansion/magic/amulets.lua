local isGoodMagicAmulet = function(item)
	local prefix = (
		--ToHit >= 20 or -- 10-20
		MaxMana >= 91 or -- 1-120
		--LightRadius >= 1 or -- 1-2
		--FireResist >= 31 or -- 5-40
		--ColdResist >= 31 or -- 5-40
		--LightResist >= 31 or -- 5-40
		--PoisonResist >= 31 or -- 5-40
		AllResist >= 21 or -- 3-30
		AmazonSkills >= 2 or -- 1-2
		BarbarianSkills >= 2 or -- 1-2
		PaladinSkills >= 2 or -- 1-2
		NecromancerSkills >= 2 or -- 1-2
		SorceressSkills >= 2 or -- 1-2
		DruidSkills >= 2 or -- 1-2
		AssassinSkills >= 2 or -- 1-2
		PassiveAndMagicTab >= 3 or -- 1-3
		FireTab >= 3 or -- 1-3
		LightningTab >= 3 or -- 1-3
		ColdTab >= 3 or -- 1-3
		CursesTab >= 3 or -- 1-3
		PoisonAndBoneTab >= 3 or -- 1-3
		NecroSummoningTab >= 3 or -- 1-3
		PaladinCombatTab >= 3 or -- 1-3
		OffensiveAurasTab >= 3 or -- 1-3
		DefensiveAurasTab >= 3 or -- 1-3
		BarbarianCombatTab >= 3 or -- 1-3
		WarcriesTab >= 3 or -- 1-3
		DruidSummoningTab >= 3 or -- 1-3
		ShapeshiftingTab >= 3 or -- 1-3
		ElementalTab >= 3 or -- 1-3
		TrapsTab >= 3 or -- 1-3
		ShadowDisciplinesTab >= 3 or -- 1-3
		MartialArtsTab >= 3 or -- 1-3
		--DamageToMana >= 10 or -- 7-12
		--MagicFind >= 11 or -- 5-15
		--MaxStamina >= 15 -- 5-20
	)
	
	local suffix = (
		Strength >= 21 or -- 1-30
		Dexterity >= 21 or -- 1-30
		Energy >= 21 or -- 1-30
		MaxLife >= 81 or -- 1-100
		--LightRadius >= 5 and ToHitPercent >= 5 -- 1/15AR or 3/30AR or 5/5%AR
		--MinDamage >= 9 or -- 1-14
		--MaxDamage >= 3 or -- 1-12
		--FireMinDamage >= 1 and FireMaxDamage >= 6 or -- 1-(2-6)
		--ColdMinDamage >= 1 and ColdMaxDamage >= 2 and ColdLength >= 2 or -- 1-(1-2) @ 1-2 sec
		--LightMinDamage >= 1 and LightMaxDamage >= 23 or -- 1-(6-23)
		--PoisonMinDamage >= 1 and PoisonMaxDamage >= 50 and PoisonLength < 3 or -- 1-50 @ 2-3 sec
		--PoisonLengthReduction >= 75 or -- 25/50/75
		--LifeDrainMinDamage >= 6 or -- 2-6
		--ManaDrainMinDamage >= 7 or -- 3-8
		LifeRegen >= 11 or -- 3-15
		DamageReduction >= 10 or -- 1-25
		--MagicDamageReduction >= 4 or -- 1-6
		GoldFind >= 41 or -- 25-80
		MagicFind >= 26 or -- 5-35
		FasterCastRate >= 10 or -- 10
		--HalfFreezeDuration ~= 0 or -- ???
		LifePerLevel ~= 0 or 
		ManaPerLevel ~= 0 
	)
	
	return (
		Unidentified or
		ItemLevel > 91 or -- crafting
		prefix and suffix
	)
end

magic.amulets = {
	checkStats = true,
	["amu"] = {
		priority = 2,
		identify = true,
		isGoodItem = isGoodMagicAmulet
	}
}
