-- expansion\magic\helms.lua "moderate"

local minItemLvl = 91 --note: forCrafting
local keepCraftUnid = (ItemLevel < minItemLvl)
local forCrafting = ItemLevel >= minItemLvl

local goodMagicHelm =
{ priority = 2, identify = true,
	goodItem =
		( Unidentified
			or (ToHitPerLevel ~= 0 or Sockets == 3)
				and MaxLife >= 21
		)
}

local anyGoodMagicHelm =
{ priority = 2, identify = true,
	goodItem =
		( Unidentified
			or (ToHitPerLevel ~= 0 or Sockets >= 2)
				and MaxLife >= 21
		)
}

local magicCraftHelm =
{ priority = 2, identify = false,
   goodItem = forCrafting
}

local goodMagicOrCraftHelm =
{ priority = 2, identify = keepCraftUnid,
	goodItem =
		( Unidentified
		    or forCrafting
			or (ToHitPerLevel ~= 0 or Sockets == 3)
				and MaxLife >= 21
		)
}

magic.helms =
{ checkStats = true,
--[[ Normal Helms
	["cap"] = anyGoodMagicHelm, -- Cap
	["skp"] = anyGoodMagicHelm, -- Skull Cap
	["hlm"] = anyGoodMagicHelm, -- Helm / Blood
	["fhl"] = anyGoodMagicHelm, -- Full Helm / Hit Power
	["ghm"] = anyGoodMagicHelm, -- Great Helm
	["msk"] = anyGoodMagicHelm, -- Mask / Caster
	["crn"] = anyGoodMagicHelm, -- Crown / Safety
	["bhm"] = anyGoodMagicHelm, -- Bone Helm
--]]

--[[ Exceptional Helms
	["xap"] = anyGoodMagicHelm, -- War Hat
	["xkp"] = anyGoodMagicHelm, -- Sallet
	["xlm"] = anyGoodMagicHelm, -- Casque / Blood
	["xhl"] = anyGoodMagicHelm, -- Basinet / Hit Power
	["xhm"] = anyGoodMagicHelm, -- Winged Helm
	["xsk"] = anyGoodMagicHelm, -- Death Mask / Caster
	["xrn"] = anyGoodMagicHelm, -- Grand Crown / Safety
	["xh9"] = anyGoodMagicHelm, -- Grim Helm
--]]

---[[ Elite Helms
	["uap"] = anyGoodMagicHelm, -- Shako
	["ukp"] = anyGoodMagicHelm, -- Hydraskull
	["ulm"] = anyGoodMagicHelm, -- Armet / Blood
	["uhl"] = anyGoodMagicHelm, -- Giant Conch / Hit Power
	["uhm"] = goodMagicHelm, -- Spired Helm
	["usk"] = goodMagicHelm, -- Demonhead / Caster
	["urn"] = goodMagicHelm, -- Corona / Safety
	["uh9"] = goodMagicHelm, -- Bone Visage
--]]
}