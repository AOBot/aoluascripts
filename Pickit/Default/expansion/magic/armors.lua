-- expansion\magic\armors.lua "moderate"

local minItemLvl = 91 --note: forCrafting
local keepCraftUnid = (ItemLevel < minItemLvl)
local forCrafting = ItemLevel >= minItemLvl

local goodMagicArmor =
{ priority = 2, identify = true,
	goodItem =
		( Unidentified
			or (Sockets == 4 or ItemCode == "xtp" and Sockets == 3)
				and (MaxLife >= 61 or FasterHitRecovery >= 17)
				-- Jewelers Armor of the Whale/Stability
		)
}

local anyGoodMagicArmor =
{ priority = 2, identify = true,
	goodItem =
		( Unidentified
			or Sockets >= 2
				and (MaxLife >= 61 or FasterHitRecovery >= 17)
		)
}

local magicCraftArmor =
{ priority = 2, identify = false,
   goodItem = forCrafting
}

local goodMagicOrCraftArmor =
{ priority = 2, identify = keepCraftUnid,
	goodItem =
		( Unidentified
		    or forCrafting
			or (Sockets == 4 or ItemCode == "xtp" and Sockets == 3)
				and (MaxLife >= 61 or FasterHitRecovery >= 17)
				-- Jewelers Armor of the Whale/Stability
		)
}

magic.armors =
{ checkStats = true,
--[[ Normal Armors
	["qui"] = anyGoodMagicArmor, -- Quilted Armor
	["lea"] = anyGoodMagicArmor, -- Leather Armor
	["hla"] = anyGoodMagicArmor, -- Hard Leather Armor
	["stu"] = anyGoodMagicArmor, -- Studded Leather
	["rng"] = anyGoodMagicArmor, -- Ring Mail
	["scl"] = anyGoodMagicArmor, -- Scale Mail
	["brs"] = anyGoodMagicArmor, -- Breast Plate / Safety
	["chn"] = anyGoodMagicArmor, -- Chain Mail
	["spl"] = anyGoodMagicArmor, -- Splint Mail
	["ltp"] = anyGoodMagicArmor, -- Light Plate / Caster
	["plt"] = anyGoodMagicArmor, -- Plate Mail / Blood
	["fld"] = anyGoodMagicArmor, -- Field Plate / Hit Power
	["gth"] = anyGoodMagicArmor, -- Gothic Plate
	["ful"] = anyGoodMagicArmor, -- Full Plate Mail
	["aar"] = anyGoodMagicArmor, -- Ancient Armor
--]]

---[[ Exceptional Armors
	--["xui"] = anyGoodMagicArmor, -- Ghost Armor
	--["xea"] = anyGoodMagicArmor, -- Serpentskin Armor
	--["xla"] = anyGoodMagicArmor, -- Demonhide Armor
	--["xtu"] = anyGoodMagicArmor, -- Trellised Armor
	--["xcl"] = anyGoodMagicArmor, -- Tigulated Mail
	--["xng"] = anyGoodMagicArmor, -- Linked Mail
	--["xrs"] = anyGoodMagicArmor, -- Cuirass / Safety
	--["xhn"] = anyGoodMagicArmor, -- Mesh Armor
	--["xpl"] = anyGoodMagicArmor, -- Russet Armor
	["xtp"] = goodMagicArmor, -- Mage Plate / Caster
	--["xlt"] = anyGoodMagicArmor, -- Templar Coat / Blood
	--["xld"] = anyGoodMagicArmor, -- Sharktooth Armor / Hit Power
	--["xth"] = anyGoodMagicArmor, -- Embossed Plate
	--["xul"] = anyGoodMagicArmor, -- Chaos Armor
	--["xar"] = anyGoodMagicArmor, -- Ornate Plate
--]]
	
---[[ Elite Armors	
	["uui"] = goodMagicArmor, -- Dusk Shroud
	--["uea"] = goodMagicArmor, -- Wyrmhide
	--["ula"] = goodMagicArmor, -- Scarab Husk
	--["utu"] = goodMagicArmor, -- Wire Fleece
	--["ucl"] = goodMagicArmor, -- Loricated Mail
	--["ung"] = goodMagicArmor, -- Diamond Mail
	--["urs"] = goodMagicArmor, -- Great Hauberk / Safety
	--["uhn"] = goodMagicArmor, -- Boneweave
	["upl"] = goodMagicArmor, -- Balrog Skin
	["utp"] = goodMagicArmor, -- Archon Plate / Caster
	["ult"] = goodMagicArmor, -- Hellforge Plate / Blood
	["uld"] = goodMagicArmor, -- Kraken Shell / Hit Power
	["uth"] = goodMagicArmor, -- Lacquered Plate
	["uul"] = goodMagicArmor, -- Shadow Plate
	["uar"] = goodMagicArmor, -- Sacred Armor
--]]
}