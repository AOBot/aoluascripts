-- expansion\magic\gloves.lua "moderate"

-- Gloves For Craft Settings
local minItemLvl = 91 --note: forCrafting
local keepCraftUnid = (ItemLevel < minItemLvl)
local forCrafting =
	( ItemLevel >= minItemLvl
		and isItemInList
		{
			--"mgl", -- Chain Gloves / Hit Power
			"xmg", -- Heavy Bracers / Hit Power
			"umg", -- Vambraces / Hit Power
			--"vgl", -- Heavy Gloves / Blood
			"xvg", -- Sharkskin Gloves / Blood
			"uvg", -- Vampirebone Gloves / Blood
			--"lgl", -- Leather Gloves / Caster
			"xlg", -- Demonhide Gloves / Caster
			"ulg", -- Bramble Mitts / Caster
			--"hgl", -- Gauntlets / Safety
			"xhg", -- War Gauntlets / Safety
			"uhg" -- Ogre Gauntlets / Safety
		}
	)

local isGoodMagicGloves =
function(item)
	if Ethereal and Identified and ReplenishDurability == 0
	then
		return false -- not keeping eth unless repairs or is forCrafting
	end
	local skill, ias, stat, other = false, false, false, false
	skill =
		( JavelinAndSpearTab >= 3
			or PassiveAndMagicTab >= 3
			or BowAndCrossBowTab >= 3
			or MartialArtsTab >= 3
		)
	ias = FasterAttackRate >= 20
	stat =
		( Dexterity >= 16
			or Strength >= 12
		)
	other =
		( FasterHitRecovery >= 10
			or LifeDrainMinDamage >= 3
			or ManaDrainMinDamage >= 3
			--or MagicFind >= 15
		)


	return
		( Unidentified
		    --or forCrafting
		    or skill and
				(ias
					or stat
					--or other
				)
		)
end

magic.gloves =
{ checkStats = true,
	["Gloves"] =
		{ priority = 3, identify = true,
			isGoodItem = isGoodMagicGloves
		}
}
