-- expansion\magic\smallCharms.lua "moderate"

-- Small Charms For Reroll Settings
local minItemLvl = 99 --note: forRerolling
local keepRerollUnid = (ItemLevel < minItemLvl)
local forRerolling = ItemLevel >= minItemLvl


local isGoodSmallCharm =
function(item)
	local greatMod, prefix, suffix = false, false, false
	greatMod =
			( AllResist >= 5
				or MaxLife >= 20
				or MagicFind >= 7
				or PoisonMaxDamage >= 100
				or ColdMaxDamage >= 20
				or FireMaxDamage >= 29
				or LightMaxDamage >= 71
				or MaxDamage >= 3
					and ToHit >= 20
				or MaxDamage >= 2
					and ToHit >= 10
					and MaxLife >= 15
				or MaxDamage >= 4
				--or SingleResist >= 11
			)
	prefix =
			( AllResist >= 4
				or SingleResist >= 10
				or ToHit >= 30
				or MaxDamage >= 2 and ToHit >= 15
				--or MaxMana >= 15
				or ArmorClass >= 25
				--or PoisonMaxDamage >= 50
				--or ColdMaxDamage >= 17
				--or FireMaxDamage >= 25
				--or LightMaxDamage >= 60
			)
	suffix =
			( MaxLife >= 16
				or Strength >= 2
				or Dexterity >= 2
				or MagicFind >= 6
				--or GoldFind >= 9
				or FasterHitRecovery ~= 0
				or FasterMoveVelocity ~= 0
			)
	return
		( Unidentified
		    --or forRerolling
			or greatMod
			or prefix and suffix
			--or prefix or suffix
		)
end

magic.smallCharms =
{ checkStats = true,
	["cm1"] = -- Small Charm
		{ priority = 5, identify = true,
			isGoodItem = isGoodSmallCharm
		}
}