-- expansion\magic\weapons.lua "moderate"

-------------------- Settings ----------------------

-- Magic Items For Craft Settings
local minItemLvl = 91 --note: forCrafting
local keepCraftUnid = (ItemLevel < minItemLvl)
local forCrafting = ItemLevel >= minItemLvl
local foolsMod = MaxDamagePerLevel ~= 0 and ToHitPerLevel ~= 0

-- Minimum Skill Level Setting
local minSkillLevel = 5

----------------- Sorceress Settings --------------------
local isGoodMagicSorcWeap =
function(item)
	local sorcSkiller = false

	sorcSkiller = 
		(ClassSkillsBonus + FireTab + FireBallSkill)
		or (ClassSkillsBonus + FireTab + MeteorSkill)
		or (ClassSkillsBonus + FireTab + EnchantSkill)
		--or (ClassSkillsBonus + FireTab + FireBoltSkill)
		--or (ClassSkillsBonus + FireTab + WarmthSkill)
		--or (ClassSkillsBonus + FireTab + InfernoSkill)
		--or (ClassSkillsBonus + FireTab + BlazeSkill)
		--or (ClassSkillsBonus + FireTab + FireWallSkill)
		--or (ClassSkillsBonus + FireTab + HydraSkill)
		--or (ClassSkillsBonus + FireTab + FireMasterySkill)
		or (ClassSkillsBonus + ColdTab + BlizzardSkill)
		or (ClassSkillsBonus + ColdTab + FrozenOrbSkill)
		--or (ClassSkillsBonus + ColdTab + IceBoltSkill)
		--or (ClassSkillsBonus + ColdTab + FrozenArmorSkill)
		--or (ClassSkillsBonus + ColdTab + FrostNovaSkill)
		--or (ClassSkillsBonus + ColdTab + IceBlastSkill)
		--or (ClassSkillsBonus + ColdTab + ShiverArmorSkill)
		--or (ClassSkillsBonus + ColdTab + GlacialSpikeSkill)
		--or (ClassSkillsBonus + ColdTab + ChillingArmorSkill)
		--or (ClassSkillsBonus + ColdTab + ColdMasterySkill)
		or (ClassSkillsBonus + LightningTab + LightningSkill)
		--or (ClassSkillsBonus + LightningTab + ChainLightningSkill)
		--or (ClassSkillsBonus + LightningTab + ChargedBoltSkill)
		--or (ClassSkillsBonus + LightningTab + StaticFieldSkill)
		--or (ClassSkillsBonus + LightningTab + TelekinesisSkill)
		--or (ClassSkillsBonus + LightningTab + NovaSkill)
		--or (ClassSkillsBonus + LightningTab + ThunderStormSkill)
		--or (ClassSkillsBonus + LightningTab + EnergyShieldSkill)
		--or (ClassSkillsBonus + LightningTab + TeleportSkill)
		--or (ClassSkillsBonus + LightningTab + LightningMasterySkill)
		    
	return
		( Unidentified
			--or forCrafting and not item.baseItem.baseType.Type == ItemKind.Orb
			or sorcSkiller >= minSkillLevel
				--and FasterCastRate >= 10
			--or SkillTabBonus >= 3
			--or ClassSkillsBonus >= 2
		)
end

----------------- Assassin Settings --------------------
local isGoodMagicAssaWeap =
function(item)
	local sinSkiller = false
	
	sinSkiller = 
		(ClassSkillsBonus + TrapsTab + LightningSentrySkill) 
		--or (ClassSkillsBonus + TrapsTab + DeathSentrySkill)
		--or (ClassSkillsBonus + TrapsTab + FireBlastSkill)
		--or (ClassSkillsBonus + TrapsTab + ShockWebSkill)
		--or (ClassSkillsBonus + TrapsTab + BladeSentinelSkill)
		--or (ClassSkillsBonus + TrapsTab + ChargedBoltSentrySkill)
		--or (ClassSkillsBonus + TrapsTab + WakeOfFireSkill)
		--or (ClassSkillsBonus + TrapsTab + BladeFurySkill)
		--or (ClassSkillsBonus + TrapsTab + WakeOfInfernoSkill)
		--or (ClassSkillsBonus + TrapsTab + BladeShieldSkill)
		or (ClassSkillsBonus + ShadowDisciplinesTab + VenomSkill) 
		--or (ClassSkillsBonus + ShadowDisciplinesTab + ClawMasterySkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + PsychicHammerSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + BurstOfSpeedSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + WeaponBlockSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + CloakOfShadowsSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + FadeSkill) 
		--or (ClassSkillsBonus + ShadowDisciplinesTab + ShadowWarriorSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + MindBlastSkill)
		--or (ClassSkillsBonus + ShadowDisciplinesTab + ShadowMasterSkill)
		--or (ClassSkillsBonus + MartialArtsTab + DragonTalonSkill) 
		--or (ClassSkillsBonus + MartialArtsTab + PhoenixStrikeSkill) 
		--or (ClassSkillsBonus + MartialArtsTab + TigerStrikeSkill) 
		--or (ClassSkillsBonus + MartialArtsTab + FistsOfFireSkill)
		--or (ClassSkillsBonus + MartialArtsTab + DragonClawSkill)
		--or (ClassSkillsBonus + MartialArtsTab + CobraStrikeSkill)
		--or (ClassSkillsBonus + MartialArtsTab + ClawsOfThunderSkill)
		--or (ClassSkillsBonus + MartialArtsTab + DragonTailSkill) 
		--or (ClassSkillsBonus + MartialArtsTab + BladesOfIceSkill)
		--or (ClassSkillsBonus + MartialArtsTab + DragonFlightSkill)
			
	return
		( Unidentified
			or sinSkiller >= minSkillLevel 
				and ias 
			or LightningSentrySkill >= 3 
				and foolsMod 
				and FasterAttackRate >= 20 
          	--or SkillTabBonus >= 3
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Paladin Settings --------------------
local isGoodMagicPalWeap =
function(item)
	local pallySkiller = false

	pallySkiller = 
		(ClassSkillsBonus + PaladinCombatTab + BlessedHammerSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + SacrificeSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + SmiteSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + HolyBoltSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + ZealSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + ChargeSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + VengeanceSkill)
		--or (ClassSkillsBonus + PaladinCombatTab + ConversionSkill)
		--or (ClassSkillsBonus + PaladinCombatTab + HolyShieldSkill) 
		--or (ClassSkillsBonus + PaladinCombatTab + FistOfTheHeavensSkill)		
		--or (ClassSkillsBonus + OffensiveAurasTab + ConcentrationSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + FanaticismSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + ConvictionSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + MightSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + HolyFireSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + ThornsSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + BlessedAimSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + HolyFreezeSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + HolyShockSkill) 
		--or (ClassSkillsBonus + OffensiveAurasTab + SanctuarySkill) 		
		--or (ClassSkillsBonus + DefensiveAurasTab + RedemptionSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + PrayerSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + ResistFireSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + DefianceSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + ResistColdSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + CleansingSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + ResistLightningSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + VigorSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + MeditationSkill) 
		--or (ClassSkillsBonus + DefensiveAurasTab + SalvationSkill)

	return
		( Unidentified
			--or forCrafting 
				--and item.baseItem.code == "7ws" -- Caduceus
			or pallySkiller >= minSkillLevel
				--and FasterCastRate >= 10
			--or SkillTabBonus >= 3
			--or ClassSkillsBonus >= 2
		)
end

----------------- Necromancer Settings --------------------
local isGoodMagicNecWeap =
function(item)
	local necroSkiller = false
	
	necroSkiller = 
		(ClassSkillsBonus + PoisonAndBoneTab + BoneSpearSkill)
		or (ClassSkillsBonus + PoisonAndBoneTab + BoneSpiritSkill)
		or (ClassSkillsBonus + PoisonAndBoneTab + PoisonNovaSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + TeethSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BoneArmorSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + PoisonDaggerSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + CorpseExplosionSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BoneWallSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + PoisonExplosionSkill)
		--or (ClassSkillsBonus + PoisonAndBoneTab + BonePrisonSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + RaiseSkeletonSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + SkeletonMasterySkill)
		--or (ClassSkillsBonus + NecroSummoningTab + ClayGolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + GolemMasterySkill)
		--or (ClassSkillsBonus + NecroSummoningTab + RaiseSkeletalMageSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + BloodgolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + SummonResistSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + IronGolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + FiregolemSkill)
		--or (ClassSkillsBonus + NecroSummoningTab + ReviveSkill)
		--or (ClassSkillsBonus + CursesTab + LowerResistSkill)
		--or (ClassSkillsBonus + CursesTab + DecrepifySkill)
		--or (ClassSkillsBonus + CursesTab + AmplifyDamageSkill)
		--or (ClassSkillsBonus + CursesTab + DimVisionSkill)
		--or (ClassSkillsBonus + CursesTab + WeakenSkill)
		--or (ClassSkillsBonus + CursesTab + IronMaidenSkill)
		--or (ClassSkillsBonus + CursesTab + TerrorSkill)
		--or (ClassSkillsBonus + CursesTab + ConfuseSkill)
		--or (ClassSkillsBonus + CursesTab + LifeTapSkill)
		--or (ClassSkillsBonus + CursesTab + AttractSkill)

	return
		( Unidentified
			--or forCrafting
			or necroSkiller >= minSkillLevel
				--and FasterCastRate >= 10
			--or SkillTabBonus >= 3
			--or ClassSkillsBonus >= 2
		)
end

----------------- Other Settings --------------------
local magicCraftWeapon = 
{ priority = 3, identify = false,
	goodItem = forCrafting
}
local warcryWeapon = 
{ priority = 3, identify = true,
	goodItem = (Unidentified or WarcriesTab >= 3)
}

----------------- Magic Weapons --------------------
magic.weapons =
{ checkStats = true,
	["Orb"] = { priority = 3, identify = true, isGoodItem = isGoodMagicSorcWeap },
	--["Staff"] = { priority = 3, identify = true, isGoodItem = isGoodMagicSorcWeap },
	["HandToHand2"] = { priority = 3, identify = true, isGoodItem = isGoodMagicAssaWeap },
	--["HandToHand"] = { priority = 3, identify = true, isGoodItem = isGoodMagicAssaWeap },
	--["Scepter"] = { priority = 3, identify = true, isGoodItem = isGoodMagicPalWeap },
	--["Wand"] = { priority = 3, identify = true, isGoodItem = isGoodMagicNecWeap },
	--["AmazonJavelin"] = { priority = 3, identify = true, goodItem = magicCraftWeapon },	
	--["AmazonSpear"] = { priority = 3, identify = true, goodItem = magicCraftWeapon },	

-- Axes / Blood

--[[ Normal Axes
	["hax"] = magicCraftWeapon, -- Hand Axe
	["axe"] = magicCraftWeapon, -- Axe
	["2ax"] = magicCraftWeapon, -- Double Axe
	["mpi"] = magicCraftWeapon, -- Military Pick
	["wax"] = magicCraftWeapon, -- War Axe
	["lax"] = magicCraftWeapon, -- Large Axe
	["bax"] = magicCraftWeapon, -- Broad Axe
	["btx"] = magicCraftWeapon, -- Battle Axe
	["gax"] = magicCraftWeapon, -- Great Axe
	["gix"] = magicCraftWeapon, -- Giant Axe
--]]

--[[ Exceptional Axes
	["9ha"] = magicCraftWeapon, -- Hatchet
	["9ax"] = magicCraftWeapon, -- Cleaver
	["92a"] = magicCraftWeapon, -- Twin Axe
	["9mp"] = magicCraftWeapon, -- Crowbill
	["9wa"] = magicCraftWeapon, -- Naga
	["9la"] = magicCraftWeapon, -- Military Axe
	["9ba"] = magicCraftWeapon, -- Bearded Axe
	["9bt"] = magicCraftWeapon, -- Tabar
	["9ga"] = magicCraftWeapon, -- Gothic Axe
	["9gi"] = magicCraftWeapon, -- Ancient Axe
--]]

--[[ Elite Axes
	["7ha"] = magicCraftWeapon, -- Tomahalk
	["7ax"] = magicCraftWeapon, -- Small Crescent
	["72a"] = magicCraftWeapon, -- Ettin Axe
	["7mp"] = magicCraftWeapon, -- War Spike
	["7wa"] = magicCraftWeapon, -- Berserker Axe
	["7la"] = magicCraftWeapon, -- Feral Axe
	["7ba"] = magicCraftWeapon, -- Silver Edged Axe
	["7bt"] = magicCraftWeapon, -- Decapitator
	["7ga"] = magicCraftWeapon, -- Champion Axe
	["7gi"] = magicCraftWeapon, -- Glorious Axe
--]]

-- Maces / Hit Power

--[[ Normal Maces
	["clb"] = magicCraftWeapon, -- Club
	["spc"] = magicCraftWeapon, -- Spiked Club
	["mac"] = magicCraftWeapon, -- Mace
	["mst"] = magicCraftWeapon, -- Morning Star
	["fla"] = magicCraftWeapon, -- Flail
	["whm"] = magicCraftWeapon, -- War Hammer
	["mau"] = magicCraftWeapon, -- Maul
	["gma"] = magicCraftWeapon, -- Great Maul
--]]

--[[ Exceptional Maces
	["9cl"] = magicCraftWeapon, -- Cudgel
	["9sp"] = magicCraftWeapon, -- Barbed Club
	["9ma"] = magicCraftWeapon, -- Flanged Mace
	["9mt"] = magicCraftWeapon, -- Jagged Star
	["9fl"] = magicCraftWeapon, -- Knout
	["9wh"] = magicCraftWeapon, -- Battle Hammer
	["9m9"] = magicCraftWeapon, -- War Club
	["9gm"] = magicCraftWeapon, -- Martel de Fer
--]]

--[[ Elite Maces
    ["7cl"] = magicCraftWeapon, -- Truncheon
	["7sp"] = magicCraftWeapon, -- Tyrant Club
	["7ma"] = magicCraftWeapon, -- Reinforced Mace
	["7mf"] = magicCraftWeapon, -- Devil Star
	["7fl"] = magicCraftWeapon, -- Scourge
	["7wh"] = magicCraftWeapon, -- Legendary Mallet
	["7m7"] = magicCraftWeapon, -- Ogre Maul
	["7gm"] = magicCraftWeapon, -- Thunder Maul
--]]

-- Spears / Safety

--[[ Normal Spears
	["spr"] = magicCraftWeapon, -- Spear
	["tri"] = magicCraftWeapon, -- Trident
	["brn"] = magicCraftWeapon, -- Brandistock
	["spt"] = magicCraftWeapon, -- Spetum
	["pik"] = magicCraftWeapon, -- Pike
--]]

--[[ Exceptional Spears
	["9sr"] = magicCraftWeapon, -- War Spear
	["9tr"] = magicCraftWeapon, -- Fuscina
	["9br"] = magicCraftWeapon, -- War Fork
	["9st"] = magicCraftWeapon, -- Yari
	["9p9"] = magicCraftWeapon, -- Lance
--]]

--[[ Elite Spears
	["7sr"] = magicCraftWeapon, -- Hyperion Spear
	["7tr"] = magicCraftWeapon, -- Stygian Pike
	["7br"] = magicCraftWeapon, -- Mancatcher
	["7st"] = magicCraftWeapon, -- Ghost Spear
	["7p7"] = magicCraftWeapon, -- War Pike
--]]

-- Javelins / Safety

--[[ Normal Javelins
	["jav"] = warcryWeapon, -- Javelin 
	["pil"] = warcryWeapon, -- Pilum
	["ssp"] = warcryWeapon, -- Short Spear
	["glv"] = warcryWeapon, -- Glaive
	["tsp"] = warcryWeapon, -- Throwing Spear
--]]

--[[ Exceptional Javelins
	["9ja"] = warcryWeapon, -- War Javelin
	["9pi"] = warcryWeapon, -- Great Pilum
	["9s9"] = warcryWeapon, -- Simbilan
	["9gl"] = warcryWeapon, -- Spiculum
	["9ts"] = warcryWeapon, -- Harpoon
--]]

--[[ Elite Javelins
	["7ja"] = magicCraftWeapon, -- Hyperion Javelin
	["7pi"] = magicCraftWeapon, -- Stygian Pilum
	["7s7"] = magicCraftWeapon, -- Balrog Spear
	["7gl"] = magicCraftWeapon, -- Ghost Glaive
	["7ts"] = magicCraftWeapon, -- Winged Harpoon
--]]

-- Throwing Weapons

--[[ Normal Throwables
	["tkf"] = warcryWeapon, -- Throwing Knife 
	["tax"] = warcryWeapon, -- Throwing Axe
	["bkf"] = warcryWeapon, -- Balanced Knife
	["bal"] = warcryWeapon, -- Balanced Axe
--]]

--[[ Exceptional Throwables
	["9tk"] = warcryWeapon, -- Battle Dart
	["9ta"] = warcryWeapon, -- Francisca
	["9bk"] = warcryWeapon, -- War Dart
	["9b8"] = warcryWeapon, -- Hurlbat
--]]

--[[ Elite Throwables
	["7tk"] = magicCraftWeapon, -- Flying Knife
	["7ta"] = magicCraftWeapon, -- Flying Axe
	["7bk"] = magicCraftWeapon, -- Winged Knife
	["7b8"] = magicCraftWeapon, -- Winged Axe
--]]

}                                                   