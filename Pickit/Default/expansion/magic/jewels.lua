-- expansion\magic\jewels.lua "moderate"

local isGoodMagicJewel =
function(item)
	local greatMod, prefix, suffix = false, false, false
	
	greatMod =
			( AllResist >= 14
				or MaxDamagePercent >= 38
				or MaxDamage >= 14
			)
	prefix =
			( MaxDamagePercent >= 33
				or AllResist >= 12
				or SingleResist >= 28
				or MaxMana >= 18
				or ArmorClass >= 59
				or ToHit >= 85
				or DamageToMana ~= 0
			)
	suffix =
			( FasterAttackRate ~= 0
				or FasterHitRecovery ~= 0
				or LowerRequirementsPercent ~= 0
				or Strength >= 7
				or Dexterity >= 7
				or Energy >= 7
				or MaxLife >= 14
				--or LightMaxDamage >= 81
			)
	return
		( Unidentified
			or greatMod
			or prefix and suffix
		)
end

magic.jewels =
{ checkStats = true,
	["jew"] = -- Jewel
		{ priority = 3, identify = true,
			isGoodItem = isGoodMagicJewel
		}
}