-- classic\rare\belts.lua "moderate"

local isGoodRareBelt =
function(item)
	return
		( Unidentified
			or FasterHitRecovery >= 17
				and Strength >= 15
				and
					( MaxLife >= 31
						or TotalResist >= 50
						--or DefensePercent >= 66
					)
		)
end

rare.belts =
{ checkStats = true,
	["Belt"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareBelt
		}
}
