-- classic\rare\shields.lua "moderate"

local isGoodRareShield =
function(item)
	local fbr, sexyMods, goodMods = false, 0, 0

    fbr = FasterBlockRate >= 30
    
    sexyMods = -- mods with colors
        count{
			DefensePercent >= 66, -- 66-100 .. Dark Gold Color
			--LightRadius == 2, -- 2 .. Light Yellow Color
			--DamageToMana ~= 0, -- 7-12 .. Crystal Blue Color
			--AttackerTakesDamage >= 4, -- 4-6 .. Orange Color
			--PoisonLengthReduction >= 75, -- 75 .. Dark Red Color
			AllResist >= 18, -- 16-20 .. Purple Color
    		ClassSkillsBonus >= 2, -- 2 Pally Skill .. Green Color
		}
		    
    goodMods = 
    	count{
			FasterHitRecovery >= 10,
    		Strength >= 7,
			DoubleResist >= 32,
			TripleResist >= 40,
    	}
	return
		( Unidentified
			or fbr
			    and (sexyMods + goodMods) >= 2
			--or (sexyMods + goodMods) >= 3
			or sexyMods >= 2 and goodMods >= 1
		)
end

local goodRareShield =
{priority = 5, identify = true,
	isGoodItem = isGoodRareShield
}

local greaterRareShield =
{priority = 8, identify = true,
	isGoodItem = isGoodRareShield
}

rare.shields =
{ checkStats = true,
--[[ Normal Shields
	["buc"] = goodRareShield, -- Buckler
	["sml"] = goodRareShield, -- Small Shield
	["lrg"] = goodRareShield, -- Large Shield
	["kit"] = goodRareShield, -- Kite Shield
	["spk"] = goodRareShield, -- Spiked Shield
	["tow"] = goodRareShield, -- Tower Shield
	["bsh"] = goodRareShield, -- Bone Shield
	["gts"] = goodRareShield, -- Gothic Shield
--]]

---[[ Exceptional Shields
	["xuc"] = greaterRareShield, -- Defender
	["xml"] = greaterRareShield, -- Round Shield
	["xrg"] = greaterRareShield, -- Scutum
	["xit"] = greaterRareShield, -- Dragon Shield
	["xpk"] = greaterRareShield, -- Barbed Shield
	["xow"] = greaterRareShield, -- Pavise
	["xsh"] = greaterRareShield, -- Grim Shield
	["xts"] = greaterRareShield, -- Ancient Shield
--]]

}
