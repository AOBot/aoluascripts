-- classic\rare\armors.lua "moderate"

local isGoodRareArmor =
function(item)
	local sexyMods, goodMods = 0, 0
	
    sexyMods = -- mods with colors
        count{
			DefensePercent >= 66, -- 66-100 .. Dark Gold Color
			--LightRadius == 2, -- 2 .. Light Yellow Color
			MaxMana >= 31, -- 31-40 .. Crystal Blue Color
			--AttackerTakesDamage >= 4, -- 4-6 .. Orange Color
			FasterHitRecovery >= 17,
			MaxLife >= 31, -- 31-60 .. Crystal Red Color
			PoisonLengthReduction >= 75, -- 75 .. Dark Red Color
		}
		
	goodMods =
	    count{
			TotalResist >= 50,
			Strength >= 10,
			Dexterity >= 5,
            --LowerRequirementsPercent ~= 0,
            --ToHit ~= 0, -- +15 to Attack Rating
            --DamageReduction >= 4,
            --MagicDamageReduction >= 2,
	    }

	return
		( Unidentified
		    or (sexyMods + goodMods) >= 3
			--or sexyMods >= 2
			    --and goodMods >= 1
		)
end

local goodRareArmor =
{ priority = 4, identify = true,
	isGoodItem = isGoodRareArmor
}

local greaterRareArmor =
{ priority = 8, identify = true,
	isGoodItem = isGoodRareArmor
}

rare.armors =
{ checkStats = true,
--[[ Normal Armors
	["qui"] = goodRareArmor, -- Quilted Armor
	["lea"] = goodRareArmor, -- Leather Armor
	["hla"] = goodRareArmor, -- Hard Leather Armor
	["stu"] = goodRareArmor, -- Studded Leather
	["rng"] = goodRareArmor, -- Ring Mail
	["scl"] = goodRareArmor, -- Scale Mail
	["brs"] = goodRareArmor, -- Breast Plate
	["chn"] = goodRareArmor, -- Chain Mail
	["spl"] = goodRareArmor, -- Splint Mail
	["ltp"] = goodRareArmor, -- Light Plate
	["plt"] = goodRareArmor, -- Plate Mail
	["fld"] = goodRareArmor, -- Field Plate
	["gth"] = goodRareArmor, -- Gothic Plate
	["ful"] = goodRareArmor, -- Full Plate Mail
	["aar"] = goodRareArmor, -- Ancient Armor
--]]

---[[ Exceptional Armors
	["xui"] = greaterRareArmor, -- Ghost Armor
	["xea"] = greaterRareArmor, -- Serpentskin Armor
	["xla"] = greaterRareArmor, -- Demonhide Armor
	["xtu"] = greaterRareArmor, -- Trellised Armor
	["xcl"] = greaterRareArmor, -- Tigulated Mail
	["xng"] = greaterRareArmor, -- Linked Mail
	["xrs"] = greaterRareArmor, -- Cuirass
	["xhn"] = greaterRareArmor, -- Mesh Armor
	["xpl"] = greaterRareArmor, -- Russet Armor
	["xtp"] = greaterRareArmor, -- Mage Plate
	["xlt"] = greaterRareArmor, -- Templar Coat
	["xld"] = greaterRareArmor, -- Sharktooth Armor
	["xth"] = greaterRareArmor, -- Embossed Plate
	["xul"] = greaterRareArmor, -- Chaos Armor
	["xar"] = greaterRareArmor, -- Ornate Plate
--]]
	
}