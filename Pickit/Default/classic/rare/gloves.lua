-- classic\rare\gloves.lua "moderate"

local isGoodRareGloves =
function(item)
	local ias, stat, other = false, false, false

	ias = FasterAttackRate >= 10
	stat =
		( Strength >= 7
			or Dexterity >= 13
			--or MaxMana >= 31
		)
	other =
		( TotalResist >= 50
			or LifeDrainMinDamage >= 3 and ManaDrainMinDamage >= 3
			or ToHitPercent >= 5
			--or DefensePercent >= 66
		)
	return
		( Unidentified
			or ias and stat and other
			--or ias and (stat or other)
		)
end

rare.gloves =
{ checkStats = true,
	["Gloves"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareGloves
		}
}
