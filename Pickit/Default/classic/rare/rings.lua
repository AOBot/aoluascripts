-- classic\rare\rings.lua "moderate"

local isGoodRareRing =
function(item)
	local fcr, goodMods, statMods, meleeMods = false, 0, 0, 0
	
	fcr = FasterCastRate >= 10

	goodMods =
		count{
			TotalResist >= 50,
			MagicFind >= 15,
		}

	statMods =
		count{
			MaxLife >= 21,
			MaxMana >= 31,
			Strength >= 8,
			--Dexterity >= 8 and Energy >= 9,
			Dexterity >= 8,
			Energy >= 9,
		}

	meleeMods =
		count{
			ToHit >= 81,
			ToHitPercent ~= 0,
			LifeDrainMinDamage >= 4 and ManaDrainMinDamage >= 3,
			MinDamage >= 6 and MaxDamage >= 3,
		}

	return
		( Unidentified
			--or fcr
				--and goodMods >= 1
			or (goodMods + statMods) >= 2
			    and fcr
			or (statMods + meleeMods) >= 3
			or goodMods >= 1
				and meleeMods >= 2
		)
end

rare.rings =
{ checkStats = true,
	["rin"] =
		{ priority = 5, identify = true,
			isGoodItem = isGoodRareRing
		}
}
