-- classic\rare\weapons.lua "moderate"

----------------------------------------------------
-------------------- Settings ----------------------
----------------------------------------------------

-- Minimum Skill Level Setting
local minSkillLevel = 5

----------------- Sorceress Settings --------------------
local isGoodRareSorcWeap =
function(item)
	local sorcSkiller = false

	sorcSkiller = 
		(ClassSkillsBonus + FireBallSkill)
		or (ClassSkillsBonus + MeteorSkill)
		or (ClassSkillsBonus + EnchantSkill)
		--or (ClassSkillsBonus + FireBoltSkill)
		--or (ClassSkillsBonus + WarmthSkill)
		--or (ClassSkillsBonus + InfernoSkill)
		--or (ClassSkillsBonus + BlazeSkill)
		--or (ClassSkillsBonus + FireWallSkill)
		--or (ClassSkillsBonus + HydraSkill)
		--or (ClassSkillsBonus + FireMasterySkill)
		or (ClassSkillsBonus + BlizzardSkill)
		or (ClassSkillsBonus + FrozenOrbSkill)
		--or (ClassSkillsBonus + IceBoltSkill)
		--or (ClassSkillsBonus + FrozenArmorSkill)
		--or (ClassSkillsBonus + FrostNovaSkill)
		--or (ClassSkillsBonus + IceBlastSkill)
		--or (ClassSkillsBonus + ShiverArmorSkill)
		--or (ClassSkillsBonus + GlacialSpikeSkill)
		--or (ClassSkillsBonus + ChillingArmorSkill)
		--or (ClassSkillsBonus + ColdMasterySkill)
		or (ClassSkillsBonus + LightningSkill)
		--or (ClassSkillsBonus + ChainLightningSkill)
		--or (ClassSkillsBonus + ChargedBoltSkill)
		--or (ClassSkillsBonus + StaticFieldSkill)
		--or (ClassSkillsBonus + TelekinesisSkill)
		--or (ClassSkillsBonus + NovaSkill)
		--or (ClassSkillsBonus + ThunderStormSkill)
		--or (ClassSkillsBonus + EnergyShieldSkill)
		--or (ClassSkillsBonus + TeleportSkill)
		--or (ClassSkillsBonus + LightningMasterySkill)
		    
	return
		( Unidentified
			or sorcSkiller >= minSkillLevel
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Paladin Settings --------------------
local isGoodRarePalWeap =
function(item)
	local pallySkiller = false

	pallySkiller = 
		(ClassSkillsBonus + BlessedHammerSkill) 
		--or (ClassSkillsBonus + SacrificeSkill) 
		--or (ClassSkillsBonus + SmiteSkill) 
		--or (ClassSkillsBonus + HolyBoltSkill) 
		--or (ClassSkillsBonus + ZealSkill) 
		--or (ClassSkillsBonus + ChargeSkill) 
		--or (ClassSkillsBonus + VengeanceSkill)
		--or (ClassSkillsBonus + ConversionSkill)
		--or (ClassSkillsBonus + HolyShieldSkill) 
		--or (ClassSkillsBonus + FistOfTheHeavensSkill)		
		--or (ClassSkillsBonus + ConcentrationSkill) 
		--or (ClassSkillsBonus + FanaticismSkill) 
		--or (ClassSkillsBonus + ConvictionSkill) 
		--or (ClassSkillsBonus + MightSkill) 
		--or (ClassSkillsBonus + HolyFireSkill) 
		--or (ClassSkillsBonus + ThornsSkill) 
		--or (ClassSkillsBonus + BlessedAimSkill) 
		--or (ClassSkillsBonus + HolyFreezeSkill) 
		--or (ClassSkillsBonus + HolyShockSkill) 
		--or (ClassSkillsBonus + SanctuarySkill) 		
		--or (ClassSkillsBonus + RedemptionSkill) 
		--or (ClassSkillsBonus + PrayerSkill) 
		--or (ClassSkillsBonus + ResistFireSkill) 
		--or (ClassSkillsBonus + DefianceSkill) 
		--or (ClassSkillsBonus + ResistColdSkill) 
		--or (ClassSkillsBonus + CleansingSkill) 
		--or (ClassSkillsBonus + ResistLightningSkill) 
		--or (ClassSkillsBonus + VigorSkill) 
		--or (ClassSkillsBonus + MeditationSkill) 
		--or (ClassSkillsBonus + SalvationSkill)

	return
		( Unidentified
			or pallySkiller >= minSkillLevel
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Necromancer Settings --------------------
local isGoodRareNecWeap =
function(item)
	local necroSkiller = false

	necroSkiller = 
		(ClassSkillsBonus + BoneSpearSkill)
		or (ClassSkillsBonus + BoneSpiritSkill)
		or (ClassSkillsBonus + PoisonNovaSkill)
		--or (ClassSkillsBonus + TeethSkill)
		--or (ClassSkillsBonus + BoneArmorSkill)
		--or (ClassSkillsBonus + PoisonDaggerSkill)
		--or (ClassSkillsBonus + CorpseExplosionSkill)
		--or (ClassSkillsBonus + BoneWallSkill)
		--or (ClassSkillsBonus + PoisonExplosionSkill)
		--or (ClassSkillsBonus + BonePrisonSkill)
		--or (ClassSkillsBonus + RaiseSkeletonSkill)
		--or (ClassSkillsBonus + SkeletonMasterySkill)
		--or (ClassSkillsBonus + ClayGolemSkill)
		--or (ClassSkillsBonus + GolemMasterySkill)
		--or (ClassSkillsBonus + RaiseSkeletalMageSkill)
		--or (ClassSkillsBonus + BloodgolemSkill)
		--or (ClassSkillsBonus + SummonResistSkill)
		--or (ClassSkillsBonus + IronGolemSkill)
		--or (ClassSkillsBonus + FiregolemSkill)
		--or (ClassSkillsBonus + ReviveSkill)
		--or (ClassSkillsBonus + LowerResistSkill)
		--or (ClassSkillsBonus + DecrepifySkill)
		--or (ClassSkillsBonus + AmplifyDamageSkill)
		--or (ClassSkillsBonus + DimVisionSkill)
		--or (ClassSkillsBonus + WeakenSkill)
		--or (ClassSkillsBonus + IronMaidenSkill)
		--or (ClassSkillsBonus + TerrorSkill)
		--or (ClassSkillsBonus + ConfuseSkill)
		--or (ClassSkillsBonus + LifeTapSkill)
		--or (ClassSkillsBonus + AttractSkill)

	return
		( Unidentified
			or necroSkiller >= minSkillLevel
          	--or ClassSkillsBonus >= 2
		)
end

----------------- Non Class Settings --------------------
local isGoodRareWeap =
function(item)
	local greatMod, goodMod, ias = false, false, false
	local prefix, suffix = 0, 0

	greatMod = MaxDamagePercent >= 186
		
	goodMod =
		( MaxDamagePercent >= 100
			or MaxDamagePercent >= 88 and ToHit >= 101		
		)
	ias = FasterAttackRate >= 30 
	prefix =
		count{ 
			MaxDamagePercent >= 100, 
			MaxDamagePercent >= 88 and ToHit >= 101,
			--ClassSkillsBonus >= 2,
		}
	suffix =
		count{ 
			FasterAttackRate >= 30, 
			Strength >= 7,
			MinDamage >= 12, 
			MaxDamage >= 15, 
			LifeDrainMinDamage >= 6, 
			--ManaDrainMinDamage >= 6,
			--LowerRequirementsPercent ~= 0,
			--PreventHeal ~= 0,
            --Knockback ~= 0,
		}
	return
		( Unidentified
		    or greatMod
			or goodMod
				and ias
			--or (prefix + suffix) >= 3
			or BarbarianSkills == 2
		)
end	
----------------- Other Settings --------------------
-- goodRareWeapon checks Non Class Settings for good items

local goodRareWeapon =
{ priority = 4, identify = true,
	isGoodItem = isGoodRareWeap
}

----------------- Rare Weapons --------------------
rare.weapons =
{ checkStats = true,

---[[ Sorceress Staffs
	["Staff"] =
		{ priority = 4, identify = true,
   			isGoodItem = isGoodRareSorcWeap
		},
--]]

---[[ Scepters
	["Scepter"] =  
		{ priority = 4, identify = true,
			isGoodItem = isGoodRarePalWeap
		},
--]]

---[[ Wands
	["Wand"] = 
		{ priority = 4, identify = true,
   			isGoodItem = isGoodRareNecWeap
		},
--]]

----------------------------------------
-- Axes 
----------------------------------------
--[[ Normal Axes
	["hax"] = goodRareWeapon, -- Hand Axe
	["axe"] = goodRareWeapon, -- Axe
	["2ax"] = goodRareWeapon, -- Double Axe
	["mpi"] = goodRareWeapon, -- Military Pick
	["wax"] = goodRareWeapon, -- War Axe
	["lax"] = goodRareWeapon, -- Large Axe
	["bax"] = goodRareWeapon, -- Broad Axe
	["btx"] = goodRareWeapon, -- Battle Axe
	["gax"] = goodRareWeapon, -- Great Axe
	["gix"] = goodRareWeapon, -- Giant Axe
--]]

---[[ Exceptional Axes
	["9ha"] = goodRareWeapon, -- Hatchet
	["9ax"] = goodRareWeapon, -- Cleaver
	["92a"] = goodRareWeapon, -- Twin Axe
	["9mp"] = goodRareWeapon, -- Crowbill
	["9wa"] = goodRareWeapon, -- Naga
	["9la"] = goodRareWeapon, -- Military Axe
	["9ba"] = goodRareWeapon, -- Bearded Axe
	["9bt"] = goodRareWeapon, -- Tabar
	["9ga"] = goodRareWeapon, -- Gothic Axe
	["9gi"] = goodRareWeapon, -- Ancient Axe
--]]

----------------------------------------
-- Bows
----------------------------------------
--[[ Normal Bows
	["sbw"] = goodRareWeapon, -- Short Bow
	["hbw"] = goodRareWeapon, -- Hunter's Bow
	["lbw"] = goodRareWeapon, -- Long Bow
	["cbw"] = goodRareWeapon, -- Composite Bow
	["sbb"] = goodRareWeapon, -- Short Battle Bow
	["lbb"] = goodRareWeapon, -- Long Battle Bow
	["swb"] = goodRareWeapon, -- Short War Bow
	["lwb"] = goodRareWeapon, -- Long War Bow
--]]

---[[ Exceptional Bows
	["8sb"] = goodRareWeapon, -- Edge Bow
	["8hb"] = goodRareWeapon, -- Razor Bow
	["8lb"] = goodRareWeapon, -- Cedar Bow
	["8cb"] = goodRareWeapon, -- Double Bow
	["8s8"] = goodRareWeapon, -- Short Siege Bow
	["8l8"] = goodRareWeapon, -- Large Siege Bow
	["8sw"] = goodRareWeapon, -- Rune Bow
	["8lw"] = goodRareWeapon, -- Gothic Bow
--]]

----------------------------------------
-- Crossbows
----------------------------------------
--[[ Normal Crossbows
	["lxb"] = goodRareWeapon, -- Light Crossbow
	["mxb"] = goodRareWeapon, -- Crossbow
	["hxb"] = goodRareWeapon, -- Heavy Crossbow
	["rxb"] = goodRareWeapon, -- Repeating Crossbow
--]]

--[[ Exceptional Crossbows
	["8lx"] = goodRareWeapon, -- Arbalest
	["8mx"] = goodRareWeapon, -- Siege Crossbow
	["8hx"] = goodRareWeapon, -- Ballista
	["8rx"] = goodRareWeapon, -- Chu-Ko-Nu
--]]

----------------------------------------
-- Daggers
----------------------------------------
--[[ Normal Daggers
	["dgr"] = goodRareWeapon, -- Dagger
	["dir"] = goodRareWeapon, -- Dirk
	["kri"] = goodRareWeapon, -- Kris
	["bld"] = goodRareWeapon, -- Blade
--]]

---[[ Exceptional Daggers
	["9dg"] = goodRareWeapon, -- Poignard
	["9di"] = goodRareWeapon, -- Rondel
	["9kr"] = goodRareWeapon, -- Cinquedeas
	["9bl"] = goodRareWeapon, -- Stiletto
--]]

----------------------------------------
-- Maces 
----------------------------------------
--[[ Normal Maces
	["clb"] = goodRareWeapon, -- Club
	["spc"] = goodRareWeapon, -- Spiked Club
	["mac"] = goodRareWeapon, -- Mace
	["mst"] = goodRareWeapon, -- Morning Star
	["fla"] = goodRareWeapon, -- Flail
	["whm"] = goodRareWeapon, -- War Hammer
	["mau"] = goodRareWeapon, -- Maul
	["gma"] = goodRareWeapon, -- Great Maul
--]]

---[[ Exceptional Maces
	["9cl"] = goodRareWeapon, -- Cudgel
	["9sp"] = goodRareWeapon, -- Barbed Club
	["9ma"] = goodRareWeapon, -- Flanged Mace
	["9mt"] = goodRareWeapon, -- Jagged Star
	["9fl"] = goodRareWeapon, -- Knout
	["9wh"] = goodRareWeapon, -- Battle Hammer
	["9m9"] = goodRareWeapon, -- War Club
	["9gm"] = goodRareWeapon, -- Martel de Fer
--]]

----------------------------------------
-- Polearms
----------------------------------------
--[[ Normal Polearms
	["bar"] = goodRareWeapon, -- Bardiche
	["vou"] = goodRareWeapon, -- Voulge
	["scy"] = goodRareWeapon, -- Scythe
	["pax"] = goodRareWeapon, -- Poleaxe
	["hal"] = goodRareWeapon, -- Halberd
	["wsc"] = goodRareWeapon, -- War Scythe
--]]

---[[ Exceptional Polearms
	["9b7"] = goodRareWeapon, -- Lochaber Axe
	["9vo"] = goodRareWeapon, -- Bill
	["9s8"] = goodRareWeapon, -- Battle Scythe
	["9pa"] = goodRareWeapon, -- Partizan
	["9h9"] = goodRareWeapon, -- Bec-De-Corbin
	["9wc"] = goodRareWeapon, -- Grim Scythe
--]]

----------------------------------------
-- Spears 
----------------------------------------
--[[ Normal Spears
	["spr"] = goodRareWeapon, -- Spear
	["tri"] = goodRareWeapon, -- Trident
	["brn"] = goodRareWeapon, -- Brandistock
	["spt"] = goodRareWeapon, -- Spetum
	["pik"] = goodRareWeapon, -- Pike
--]]

---[[ Exceptional Spears
	["9sr"] = goodRareWeapon, -- War Spear
	["9tr"] = goodRareWeapon, -- Fuscina
	["9br"] = goodRareWeapon, -- War Fork
	["9st"] = goodRareWeapon, -- Yari
	["9p9"] = goodRareWeapon, -- Lance
--]]

----------------------------------------
-- Swords
----------------------------------------
--[[ Normal Swords
	["ssd"] = goodRareWeapon, -- Short Sword
	["scm"] = goodRareWeapon, -- Scimitar
	["sbr"] = goodRareWeapon, -- Sabre
	["flc"] = goodRareWeapon, -- Falchion
	["crs"] = goodRareWeapon, -- Crystal Sword
	["bsd"] = goodRareWeapon, -- Broad Sword
	["lsd"] = goodRareWeapon, -- Long Sword
	["wsd"] = goodRareWeapon, -- War Sword
	["2hs"] = goodRareWeapon, -- Two-Handed Sword
	["clm"] = goodRareWeapon, -- Claymore
	["gis"] = goodRareWeapon, -- Giant Sword
	["bsw"] = goodRareWeapon, -- Bastard Sword
	["flb"] = goodRareWeapon, -- Flamberge
	["gsd"] = goodRareWeapon, -- Great Sword
--]]

---[[ Exceptional Swords
	["9ss"] = goodRareWeapon, -- Gladius
	["9sm"] = goodRareWeapon, -- Cutlass
	["9sb"] = goodRareWeapon, -- Shamshir
	["9fc"] = goodRareWeapon, -- Tulwar
	["9cr"] = goodRareWeapon, -- Dimensional Blade
	["9bs"] = goodRareWeapon, -- Battle Sword
	["9ls"] = goodRareWeapon, -- Rune Sword
	["9wd"] = goodRareWeapon, -- Ancient Sword
	["92h"] = goodRareWeapon, -- Espandon
	["9cm"] = goodRareWeapon, -- Dacian Falx
	["9gs"] = goodRareWeapon, -- Tusk Sword
	["9b9"] = goodRareWeapon, -- Gothic Sword
	["9fb"] = goodRareWeapon, -- Zweihander
	["9gd"] = goodRareWeapon, -- Executioner Sword
--]]

----------------------------------------
-- Javelins 
----------------------------------------
--[[ Normal Javelins
	["jav"] = goodRareWeapon, -- Javelin
	["pil"] = goodRareWeapon, -- Pilum
	["ssp"] = goodRareWeapon, -- Short Spear
	["glv"] = goodRareWeapon, -- Glaive
	["tsp"] = goodRareWeapon, -- Throwing Spear
--]]

---[[ Exceptional Javelins
	["9ja"] = goodRareWeapon, -- War Javelin
	["9pi"] = goodRareWeapon, -- Great Pilum
	["9s9"] = goodRareWeapon, -- Simbilan
	["9gl"] = goodRareWeapon, -- Spiculum
	["9ts"] = goodRareWeapon, -- Harpoon
--]]

----------------------------------------
-- Throwing Weapons
----------------------------------------
--[[ Normal Throwables
	["tkf"] = goodRareWeapon, -- Throwing Knife
	["tax"] = goodRareWeapon, -- Throwing Axe
	["bkf"] = goodRareWeapon, -- Balanced Knife
	["bal"] = goodRareWeapon, -- Balanced Axe
--]]

---[[ Exceptional Throwables
	["9tk"] = goodRareWeapon, -- Battle Dart
	["9ta"] = goodRareWeapon, -- Francisca
	["9bk"] = goodRareWeapon, -- War Dart
	["9b8"] = goodRareWeapon, -- Hurlbat
--]]

}