-- classic\rare\boots.lua "moderate"

local isGoodRareBoots =
function(item)
	local frw, stat, other = false, false, false
	
	frw = FasterMoveVelocity >= 20
	stat =
		( Dexterity >= 7
			or MaxMana >= 31
		)
	other =
		( FasterHitRecovery >= 10
			or MagicFind >= 16
			or GoldFind >= 61
			--or DefensePercent >= 66
		)
	return
		( Unidentified
			or TripleResist >= 50
			or TotalResist >= 50 and frw
			or TotalResist >= 40 and frw and (stat or other)
			or TotalResist >= 32 and frw and stat and other
		)
end

rare.boots =
{ checkStats = true,
	["Boots"] =
		{ priority = 4, identify = true,
			isGoodItem = isGoodRareBoots
		}
}
