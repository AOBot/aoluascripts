-- classic\white\shields.lua "moderate"

local keepForImbue = Sockets == 0 -- "Charsi Quest"

local threeSocket =
{ priority = 3,
	goodItem =
		( Sockets == 3
			and DefensePercent >= 10
		--or Sockets == 0 -- keep weapon for imbue "Charsi Quest"
		)
}

white.shields =
{ checkStats = true,
---[[ Normal Shields
	--["sml"] = keepForImbue, -- Small Shield
	--["lrg"] = threeSocket, -- Large Shield
	--["kit"] = threeSocket, -- Kite Shield
	--["spk"] = keepForImbue, -- Spiked Shield
	["tow"] = threeSocket, -- Tower Shield
	--["bsh"] = keepForImbue, -- Bone Shield
	["gts"] = threeSocket, -- Gothic Shield
--]]

---[[ Exceptional Shields
	--["xml"] = keepForImbue, -- Round Shield
	["xrg"] = threeSocket, -- Scutum
	["xit"] = threeSocket, -- Dragon Shield
	--["xpk"] = keepForImbue, -- Barbed Shield
	["xow"] = threeSocket, -- Pavise
	--["xsh"] = keepForImbue, -- Grim Shield
	["xts"] = threeSocket, -- Ancient Shield
--]]

}