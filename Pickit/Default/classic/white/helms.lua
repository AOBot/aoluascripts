-- classic\white\helms.lua "moderate, strict"

local keepForImbue = Sockets == 0 -- "Charsi Quest"

local threeSocket =
{ priority = 3,
	goodItem =
		( Sockets == 3
			and DefensePercent >= 10
		--or Sockets == 0 -- keep weapon for imbue "Charsi Quest"
		)
}

white.helms =
{ checkStats = true,
--[[ Normal Helms
	["cap"] = keepForImbue, -- Cap
	["skp"] = keepForImbue, -- Skull Cap
	["hlm"] = keepForImbue, -- Helm
	["fhl"] = keepForImbue, -- Full Helm
	["ghm"] = threeSocket, -- Great Helm
	["msk"] = threeSocket, -- Mask
	["crn"] = threeSocket, -- Crown
	["bhm"] = keepForImbue, -- Bone Helm
--]]

---[[ Exceptional Helms
	--["xap"] = keepForImbue, -- War Hat
	--["xkp"] = keepForImbue, -- Sallet
	--["xlm"] = keepForImbue, -- Casque
	--["xhl"] = keepForImbue, -- Basinet
	["xhm"] = threeSocket, -- Winged Helm
	["xsk"] = threeSocket, -- Death Mask
	["xrn"] = threeSocket, -- Grand Crown
	--["xh9"] = keepForImbue, -- Grim Helm
--]]

}