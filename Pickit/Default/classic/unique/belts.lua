-- classic\unique\belts.lua "moderate"

unique.belts =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- Belts --------------------------------------------
-----------------------------------------------------
--[[ Lenymo (Sash)
	["lbl"] =
		{
			priority = 2, identify = true,
			--note: all the same
		},

--]]
-----------------------------------------------------
--[[ Snakecord (Light Belt)
	["vbl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 25 --note: 20-30 ed
				)
		},
	
--]]
-----------------------------------------------------
--[[ Nightsmoke (Belt)
	["mbl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 40 --note: 30-50 ed
				)
		},
	
--]]
-----------------------------------------------------
---[[ Goldwrap (Heavy Belt)
	["tbl"] =
		{
			priority = 3, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 40 --note: 40-60 ed
					    and GoldFind >= 65 --note: 50-80 extra gold
				)
		},
	
--]]
-----------------------------------------------------
---[[ Bladebuckle (Plated Belt)
	["hbl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 90 --note: 80-100 ed
				)
		},
	
--]]
-----------------------------------------------------
}