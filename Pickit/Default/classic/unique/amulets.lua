-- classic\unique\amulets.lua "moderate"

unique.amulets =
{ checkStats = _CheckUniqueItemStats,
	["amu"] = ------ Amulet
		{ priority = 8, identify = true,
			goodItem =
				( Unidentified
-----------------------------------------------------
-- This part keeps amulets regardless of stats
-- Note: this part overrides the next part
-----------------------------------------------------
					--or UniqueIndex == 117 -- Nokozan Relic
					--or UniqueIndex == 118 -- The Eye of Etlich
					or UniqueIndex == 119 -- The Mahim-Oak Curio
					
-----------------------------------------------------
-- This part keeps amulets with specific stats
-----------------------------------------------------	
---[[ The Eye of Etlich 
	                or UniqueIndex == 118 -- The Eye of Etlich
	                    --and LightRadius >= 1 -- 1-5 light rad
	                    and LifeDrainMinDamage >= 5 -- 3-7 lifestolen
	                    --and ColdMinDamage >= 1 -- 1-2 min cold dmg
	                    --and ColdMaxDamage >= 3 -- 3-5 max cold dmg
	                    and ArmorClassVsMissile >= 10 -- 10-40 def vs mis
--]]
-----------------------------------------------------
				)
		}
}