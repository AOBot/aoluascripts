-- classic\unique\gloves.lua "moderate"

unique.gloves =
{ checkStats = _CheckUniqueItemStats,
-----------------------------------------------------
-- Gloves -------------------------------------------
-----------------------------------------------------
--[[ The Hand of Broc (Leather Gloves)
	["lgl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 15 --note: 10-20 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Bloodfist (Heavy Gloves)
	["vgl"] =
		{
			priority = 2, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 15 --note: 10-20 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Chance Guards (Chain Gloves)
	["mgl"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or MagicFind >= 40 --note: 25-40 mf
						--and DefensePercent >= 20 --note: 20-30 ed
				)
		},
--]]
-----------------------------------------------------
---[[ Magefist (Light Gauntlets)
	["tgl"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 30 --note: 20-30 ed
				)
		},
--]]
-----------------------------------------------------
--[[ Frostburn (Gauntlets)
	["hgl"] =
		{
			priority = 5, identify = true,
			goodItem =
				( Unidentified
					or DefensePercent >= 15 --note: 10-20 ed
				)
		},
--]]
-----------------------------------------------------
}