-- classic\set\setItems.lua "moderate"

--[[ Note:
	Duplicate set items are located at the
	bottom of this file.
	Ex: Angelic Wings (Amulet) is located
		at the bottom of the file.
--]]
		
set.items =
{ checkStats = true,
--------------------------------------------------
---[[ Angelic Raiment
	["rng"] = {}, -- Angelic Mantle (Ring Mail)
	["sbr"] = {}, -- Angelic Sickle (Sabre)
	---------------- Angelic Halo (Ring) (Duplicate)
	---------------- Angelic Wings (Amulet) (Duplicate)
--]]
--------------------------------------------------
---[[ Arcanna's Tricks
	["skp"] = {}, -- Arcanna's Head (Skull Cap)
	["ltp"] = {}, -- Arcanna's Flesh (Light Plate)
	["wst"] = {}, -- Arcanna's Deathwand (War Staff)
	---------------- Arcanna's Sign (Amulet) (Duplicate)
--]]
--------------------------------------------------
---[[ Arctic Gear
	["qui"] = {}, -- Arctic Furs (Quilted Armor)
	["vbl"] = {}, -- Arctic Binding (Light Belt)
	---------------- Arctic Mitts (Light Gauntlets) (Duplicate)
	["swb"] = {}, -- Arctic Horn (Short War Bow)
--]]
--------------------------------------------------
--[[ Berserker's Arsenal
	["hlm"] = {}, -- Berserker's Headgear (Helm)
	["spl"] = {}, -- Berserker's Hauberk (Splint Mail)
	["2ax"] = {}, -- Berserker's Hatchet (Double Axe)
--]]
--------------------------------------------------
---[[ Cathan's Traps
	["msk"] = {}, -- Cathan's Visage (Mask)
	["chn"] = {}, -- Cathan's Mesh (Chain Mail)
	["bst"] = {}, -- Cathan's Rule (Battle Staff)
	---------------- Cathan's Sigil (Amulet) (Duplicate)
	---------------- Cathan's Seal (Ring) (Duplicate)
--]]
--------------------------------------------------
---[[ Civerb's Vestments
	["gsc"] = {}, -- Civerb's Cudgel (Grand Scepter)
	---------------- Civerb's Icon (Amulet) (Duplicate)
	["lrg"] = {}, -- Civerb's Ward (Large Shield)
--]]
--------------------------------------------------
--[[ Cleglaw's Brace
	["lsd"] = {}, -- Cleglaw's Tooth (Long Sword)
	["mgl"] = {}, -- Cleglaw's Pincers (Chain Gloves)
	["sml"] = {}, -- Cleglaw's Claw (Small Shield)
--]]
--------------------------------------------------
--[[ Death's Disguise
	["wsd"] = {}, -- Death's Touch (War Sword)
	["lgl"] = {}, -- Death's Hand (Leather Gloves)
	["lbl"] = {}, -- Death's Guard (Sash)
--]]
--------------------------------------------------
--[[ Hsarus' Defense
	["buc"] = {}, -- Hsarus' Iron Fist (Buckler)
	["mbl"] = {}, -- Hsarus' Iron Stay (Belt)
	["mbt"] = {}, -- Hsarus' Iron Heel (Chain Boots)
--]]
--------------------------------------------------
---[[ Infernal Tools
	["cap"] = {}, -- Infernal Cranium (Cap)
	---------------- Infernal Sign (Heavy Belt) (Duplicate)
	["gwn"] = {}, -- Infernal Torch (Grim Wand)
--]]
--------------------------------------------------
--[[ Iratha's Finery
	---------------- Iratha's Coil (Crown) (Duplicate)
	---------------- Iratha's Collar (Amulet) (Duplicate)
	---------------- Iratha's Cord (Heavy Belt) (Duplicate)
	---------------- Iratha's Cuff (Light Gauntlets) (Duplicate)
--]]
--------------------------------------------------
---[[ Isenhart's Armory
	["bsd"] = {}, -- Isenhart's Lightbrand (Broad Sword)
	["fhl"] = {}, -- Isenhart's Horns (Full Helm)
	["brs"] = {}, -- Isenhart's Case (Breast Plate)
	["gts"] = {}, -- Isenhart's Parry (Gothic Shield)
--]]
--------------------------------------------------
---[[ Milabrega's Regalia
	---------------- Milabrega's Diadem (Crown) (Duplicate)
	["aar"] = {}, -- Milabrega's Robe (Ancient Armor)
	["kit"] = {}, -- Milabrega's Orb (Kite Shield)
	["wsp"] = {}, -- Milabrega's Rod (War Scepter)
--]]
--------------------------------------------------
---[[ Sigon's Complete Steel
	["ghm"] = {}, -- Sigon's Visor (Great Helm)
	["gth"] = {}, -- Sigon's Shelter (Gothic Plate)
	["hbt"] = {}, -- Sigon's Sabot (Greaves)
	["tow"] = {}, -- Sigon's Guard (Tower Shield)
	["hbl"] = {}, -- Sigon's Wrap (Plated Belt)
	["hgl"] = {}, -- Sigon's Gage (Gauntlets)
--]]
--------------------------------------------------
---[[ Tancred's Battlegear
	["bhm"] = {}, -- Tancred's Skull (Bone Helm)
	["ful"] = {}, -- Tancred's Spine (Full Plate Mail)
	["lbt"] = {}, -- Tancred's Hobnails (Boots)
	["mpi"] = {}, -- Tancred's Crowbill (Military Pick)
	---------------- Tancred's Weird (Amulet) (Duplicate)
--]]
--------------------------------------------------
---[[ Vidala's Rig
	["lbb"] = {}, -- Vidala's Barb (Long Battle Bow)
	["lea"] = {}, -- Vidala's Ambush (Leather Armor)
	["tbt"] = {}, -- Vidala's Fetlock (Light Plated Boots)
	---------------- Vidala's Snare (Amulet) (Duplicate)
--]]

--------------------------------------------------
--------------- Duplicates -----------------------
--------------------------------------------------
---[[ Amulet
	["amu"] =
			{ identify = goodSetAmulet, priority = 4,
				goodItem =
					( Unidentified
						or SetIndex == 53 -- Angelic Wings
						or SetIndex == 58 -- Arcanna's Sign
						or SetIndex == 28 -- Cathan's Sigil
						or SetIndex == 1 -- Civerb's Icon
						or SetIndex == 9 -- Iratha's Collar
						or SetIndex == 33 -- Tancred's Weird
						or SetIndex == 20 -- Vidala's Snare
					)
			},
--]]
--------------------------------------------------
---[[ Crown
	["crn"] =
			{ identify = true,
				goodItem =
					( Unidentified
						or SetIndex == 11 -- Iratha's Coil
						or SetIndex == 23 -- Milabrega's Diadem
					)
			},
--]]
--------------------------------------------------
---[[ Heavy Belt
	["tbl"] =
			{ identify = true,
				goodItem =
					( Unidentified
						or SetIndex == 43 -- Infernal Sign
						or SetIndex == 12 -- Iratha's Cord
					)
			},
--]]
--------------------------------------------------
---[[ Light Gauntlets
	["tgl"] =
			{ identify = true,
				goodItem =
					( Unidentified
						or SetIndex == 57 -- Arctic Mitts
						or SetIndex == 10 -- Iratha's Cuff
					)
			},
--]]
--------------------------------------------------
---[[ Ring
	["rin"] =
			{ identify = true,
				goodItem =
					( Unidentified
						or SetIndex == 52 -- Angelic Halo
						or SetIndex == 29 -- Cathan's Seal
					)
			},
--]]
--------------------------------------------------
}