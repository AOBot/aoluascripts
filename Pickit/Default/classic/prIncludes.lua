--------------------------------------------------
-------------- Classic "Moderate" ----------------
--------------------------------------------------

unique.files = {
--------------------------------------------------
--------------- Unique Item Files ----------------
--------------------------------------------------
--["amulets.lua"] = {kind = "Amulet"},
["armors.lua"] = {kind = "Armor"},
["belts.lua"] = {kind = "Belt"},
--["boots.lua"] = {kind = "Boots"},
["gloves.lua"] = {kind = "Gloves"},
["helms.lua"] = {kind = "AnyHelm"},
["rings.lua"] = {kind = "Ring"},
--["shields.lua"] = {kind = "AnyShield"},
["weapons.lua"] = {kind = "AnyWeapon"},
--------------------------------------------------
}
rare.files = {
--------------------------------------------------
--------------- Rare Item Files ------------------
--------------------------------------------------
["amulets.lua"] = {kind = "Amulet"},
["armors.lua"] = {kind = "Armor"},
["belts.lua"] = {kind = "Belt"},
["boots.lua"] = {kind = "Boots"},
["gloves.lua"] = {kind = "Gloves"},
["helms.lua"] = {kind = "Helm"},
["shields.lua"] = {kind = "Shield"},
["rings.lua"] = {kind = "Ring"},
["weapons.lua"] = {kind = "AnyWeapon"},
--------------------------------------------------
}
set.files = {
--------------------------------------------------
--------------- Set Item Files -------------------
--------------------------------------------------
--["setItems.lua"] = {noLimiters = true}
--------------------------------------------------
}
magic.files = {
--------------------------------------------------
--------------- Magic Item Files -----------------
--------------------------------------------------
--["amulets.lua"] = {kind = "Amulet"},
--["armors.lua"] = {kind = "Armor"},
--["helms.lua"] = {kind = "Helm"},
--["rings.lua"] = {kind = "Ring"},
--["shields.lua"] = {kind = "Shield"},
["weapons.lua"] = {kind = "AnyWeapon"},
--------------------------------------------------
}
white.files = {
--------------------------------------------------
--------------- White Item Files -----------------
--------------------------------------------------
--["armors.lua"] = {kind = "Armor"},
--["gems.lua"] = {kind = "Gem"},
--["helms.lua"] = {kind = "AnyHelm"},
--["shields.lua"] = {kind = "AnyShield"},
--["weapons.lua"] = {kind = "AnyWeapon"},
--------------------------------------------------
}