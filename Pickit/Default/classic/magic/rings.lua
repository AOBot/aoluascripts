-- classic\magic\rings.lua "moderate"

magic.rings =
{ checkStats = true,
	["rin"] = -- Ring
		{ priority = 2, identify = true,
			goodItem = ( Unidentified or MagicFind >= 35)
		},
}