-- classic\magic\amulets.lua "moderate"

local isGoodMagicAmulet =
function(item)
	local greatMod, prefix, suffix = false, false, false
	
	greatMod = (MagicFind + AllResist) >= 32
	prefix =
		( AllResist >= 18
			or ClassSkillsBonus >= 2
		)
	suffix =
		( MaxLife >= 31
			or FasterCastRate >= 10
			or Strength >= 10
			or Dexterity >= 10
			or Energy >= 11
			or MagicFind >= 15
			--or PoisonLengthReduction >= 75
		)

	return
		( Unidentified
			or greatMod
			or (prefix and suffix)
			--or (prefix or suffix)
			--or prefix
		)
end

magic.amulets =
{ checkStats = true,
	["amu"] = -- Amulet
		{ priority = 2, identify = true,
			isGoodItem = isGoodMagicAmulet
		}
}