
function getStats(item)
	local newStatType, newItemQuality = {}, {}
	for k, v in pairs(StatType) do
		newStatType[v] = k
	end
	for k, v in pairs(ItemQuality) do
		newItemQuality[v] = k
	end
	local stats = {}
	local defaultStat = {__index = function(t, k) return 0 end}
	setmetatable(stats, defaultStat)
	local key
	stats.Identified = item.flags.Identified
	stats.Unidentified = not item.flags.Identified
	stats.Ethereal = item.flags.Ethereal
	stats.ItemLevel = item.level
	stats.ItemCode = item.baseItem.code
	stats.ItemQuality = newItemQuality[item.quality]
	if item.flags.Identified then
		if item.quality == ItemQuality.Unique then
			stats.UniqueIndex = item.uniqueItem.Index
		elseif item.quality == ItemQuality.Set then
			stats.SetIndex = item.setItem.Index
		elseif item.quality == ItemQuality.Magic then
			stats.PrefixIndex = item.prefix.Index
			stats.SuffixIndex = item.suffix.Index
		end
	end
	for i = 0, item.stats:size() - 1 do
		key = newStatType[item.stats[i].Stat.Type]
		stats[key] = item.stats[i].Value
	end
	-- replace types common to stats and mods
	if stats.ArmorClass > 0 then
		stats.BaseDefense = stats.ArmorClass
		stats.ArmorClass = nil
	end
	for i = 0, item.mods:size() - 1 do
		key = newStatType[item.mods[i].Stat.Type]
		stats[key] = item.mods[i].Value
	end
	return stats
end

-- getStats helper functions (get passed result of getStats)
function getDefense(s)
	if s.BaseDefense > 0 then
		s.TotalDefense =
			math.floor(s.BaseDefense*(1+s.DefensePercent/100)+s.ArmorClass)
	end
end

function getResists(s, poison)
	local nres, tot, maxp = 0, 0, 0
	local minp = 1000
	if s.FireResist > 0 then
		nres = nres + 1
		tot = tot + s.FireResist
		if s.FireResist < minp then minp = s.FireResist end
		if s.FireResist > maxp then maxp = s.FireResist end
	end
	if s.ColdResist > 0 then
		nres = nres + 1
		tot = tot + s.ColdResist
		if s.ColdResist < minp then minp = s.ColdResist end
		if s.ColdResist > maxp then maxp = s.ColdResist end
	end
	if s.LightResist > 0 then
		nres = nres + 1
		tot = tot + s.LightResist
		if s.LightResist < minp then minp = s.LightResist end
		if s.LightResist > maxp then maxp = s.LightResist end
	end
	if s.PoisonResist > 0 then
		if nres >= 3 then
			if s.PoisonResist < minp then s.AllResist = s.PoisonResist
			else s.AllResist = minp
			end
		end
		if poison then
			tot = tot + s.PoisonResist
			if s.PoisonResist < minp then minp = s.PoisonResist end
			if s.PoisonResist > maxp then maxp = s.PoisonResist end
		end
	end
	if nres >= 1 then s.SingleResist = minp
		if nres >= 2 then s.DoubleResist = minp end
		if nres >= 3 then s.TripleResist = minp end
		if tot > 0 then s.TotalResist = tot end
		if minp < 1000 then s.MinResist = minp end
		if maxp > 0 then s.MaxResist = maxp end
	end
end

function getElementalDamage(s, item)
	if s.ColdMinDamage > 0 or s.FireMinDamage > 0
	or s.LightMinDamage > 0 or s.PoisonMinDamage > 0 then
		local cmin, fmin, lmin, pmin = 0, 0, 0, 0
		local cmax, fmax, lmax, pmax = 0, 0, 0, 0
		for i = 0, item.mods:size() - 1 do
			if item.mods[i].Stat.Type == StatType.ColdMinDamage then
				cmin = cmin + item.mods[i].Value
				cmax = cmax + item.mods[i].Param
			elseif item.mods[i].Stat.Type == StatType.FireMinDamage then
				fmin = fmin + item.mods[i].Value
				fmax = fmax + item.mods[i].Param
			elseif item.mods[i].Stat.Type == StatType.LightMinDamage then
				lmin = lmin + item.mods[i].Value
				lmax = lmax + item.mods[i].Param
			elseif item.mods[i].Stat.Type == StatType.PoisonMinDamage then
				pmin = pmin + item.mods[i].Min
				pmax = pmax + item.mods[i].Max
			end
		end
		if cmin > 0 then s.ColdMinDamage = cmin ; s.ColdMaxDamage = cmax end
		if fmin > 0 then s.FireMinDamage = fmin ; s.FireMaxDamage = fmax end
		if lmin > 0 then s.LightMinDamage = lmin ; s.LightMaxDamage = lmax end
		if pmin > 0 then s.PoisonMinDamage = pmin ; s.PoisonMaxDamage = pmax end
	end
end

function getSkills(s, item)
-- Single Skills, Charged Skills,
-- Skill on Striking, Skill on get Hit,
-- NonClassSkill
-- note: BoneArmor, Fade, FindItem, Howl, MagicArrow, Pierce
-- are common to StatType (no idea what StatType.Howl, etc. is)
	if s.SingleSkill > 0 or s.ChargedSkill > 0
	or s.SkillOnStriking > 0 or s.SkillOnGetHit > 0
	or s.NonClassSkill > 0 then
		local skill
		local newSkillType = {}
		for k, v in pairs(SkillType) do
			newSkillType[v] = k
		end
		for i = 0, item.mods:size() - 1 do
			if item.mods[i].Stat.Type == StatType.SingleSkill
			or item.mods[i].Stat.Type == StatType.NonClassSkill then
				skill = newSkillType[item.mods[i].Skill]
				s[skill.."Skill"] = item.mods[i].Value
			elseif item.mods[i].Stat.Type == StatType.ChargedSkill then
				skill = newSkillType[item.mods[i].Skill]
				s[skill.."ChargesLevel"] = item.mods[i].Value
				s[skill.."ChargesMax"] = item.mods[i].MaxCharges
			elseif item.mods[i].Stat.Type == StatType.SkillOnStriking then
				skill = newSkillType[item.mods[i].Skill]
				s[skill.."OnStrikingLevel"] = item.mods[i].Value
				s[skill.."OnStrikingChance"] = item.mods[i].Chance
			elseif item.mods[i].Stat.Type == StatType.SkillOnGetHit then
				skill = newSkillType[item.mods[i].Skill]
				s[skill.."OnStruckLevel"] = item.mods[i].Value
				s[skill.."OnStruckChance"] = item.mods[i].Chance
			end
		end
	end
-- Class Skills
	if s.ClassSkillsBonus > 0 then
		local class
		local newCharacterClass = {}
		for k, v in pairs(CharacterClass) do
			newCharacterClass[v] = k
		end
		for i = 0, item.mods:size() - 1 do
			if item.mods[i].Stat.Type == StatType.ClassSkillsBonus then
				class = newCharacterClass[item.mods[i].Class]
				s[class.."Skills"] = item.mods[i].Value
			end
		end
	end
-- Tab Skills
	if s.SkillTabBonus > 0 then
		local tab
		local newSkillTab = {}
		for k, v in pairs(SkillTab) do
			newSkillTab[v] = k
		end
		for i = 0, item.mods:size() - 1 do
			if item.mods[i].Stat.Type == StatType.SkillTabBonus then
				tab = newSkillTab[item.mods[i].Tab]
				s[tab.."Tab"] = item.mods[i].Value
			end
		end
	end
end

-- _Stats is the global result of getStats (defined in gateway.lua:processItem)
function hasSockets(socs)
	if type(socs) == "string" then
		local itemSocs = _Stats.Sockets
		for i = 1, socs:len() do
			if (socs:byte(i) - 48) == itemSocs then return true end
		end
	end
	return false
end

function isItemInList(list)
	if type(list) == "table" then
		local code = _Stats.ItemCode
		for i = 1, #list do
			if list[i] == code then return true end
		end
	end
	return false
end

function count(list)
	if type(list) == "table" then
		local count = 0
		for i = 0, #list do
			if list[i] == true then count = count + 1 end
		end
		return count
	end
	return 0
end

-- debugging functions
function tableOut(t, s)
	io.output(io.open("logs\\PR_tableOut.txt", "a+"))
	io.write("----------------------------------------\n")
	if type(t) == "table" then
		io.write(tostring(t))
		if type(s) == "string" then io.write(" ("..s..")") end
		io.write("\n")
		for k, v in pairs(t) do
			io.write(tostring(k), " = ", tostring(v), "\n")
		end
	else
		io.write("Not table")
		if type(s) == "string" then io.write(" ("..s..")") end
		io.write("\n")
	end
	io.close()
end

function varOut(v, s)
	io.output(io.open("logs\\PR_varOut.txt", "a+"))
	if type(s) == "string" then io.write(s.." = ") end
	io.write(tostring(v), "\n")
	io.close()
end