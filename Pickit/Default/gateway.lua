------------------------- startup ------------------------------------
---- Common Settings ----
_CheckUniqueItemStats = true
_SellUnlistedItems = false
---- Log Settings -------
_UseItemLogging = true
---- Debug Settings -----
_TestAllFiles = false
_LogErrors = true
_LogWarnings = true
---------- new tables -------------
unique = {} ; unique.files = {}
rare = {} ; rare.files = {}
set = {} ; set.files = {}
magic = {} ; magic.files = {}
white = {} ; white.files = {}
_Stats = {}
_NewItemKind = {}
for k, v in pairs(ItemKind) do
	_NewItemKind[v] = k
end
---------- actions ----------------
_Pick = 1
_Ident = 2
_Sell = 3
_Dump = 4
---------- version setting --------
function setVersion(item)
	if item.version == ItemVersion.LoD110 then
		ProtectedLoadfile(_PRMainDir.."expansion\\prIncludes.lua")()
        	ProtectedLoadfile(_PRMainDir.."prCommon.lua")()
		unique.dir = _PRMainDir.."expansion\\unique\\"
		rare.dir = _PRMainDir.."expansion\\rare\\"
		set.dir = _PRMainDir.."expansion\\set\\"
		magic.dir = _PRMainDir.."expansion\\magic\\"
		white.dir = _PRMainDir.."expansion\\white\\"
	else
		ProtectedLoadfile(_PRMainDir.."classic\\prIncludes.lua")()
        	ProtectedLoadfile(_PRMainDir.."prCommon.lua")()
		unique.dir = _PRMainDir.."classic\\unique\\"
		rare.dir = _PRMainDir.."classic\\rare\\"
		set.dir = _PRMainDir.."classic\\set\\"
		magic.dir = _PRMainDir.."classic\\magic\\"
		white.dir = _PRMainDir.."classic\\white\\"
	end
end
---------- checkFileLimiters ------
-- located at bottom of file
---------- itemFile loader --------
function loadQualityFiles(item, qualityT, testFiles)
	local filename, loader
	if testFiles then
		for k, v in pairs(qualityT.files) do
			filename = qualityT.dir..k
			loader = ProtectedLoadfile(filename)
			--varOut("checked syntax: "..filename)
		end
	else
		local loadedCount = 0
		-- env: enables using "Strength >= 20", etc, in itemFiles
		local env = {}
		setmetatable(env, {__index =
			function(t, k)
				if _G[k] ~= nil then return _G[k]
				elseif _Stats[k] ~= nil then return _Stats[k]
				end
			end})
		for k, v in pairs(qualityT.files) do
			--varOut("checking limiters: "..qualityT.dir..k)
			if checkFileLimiters(item, v) then
				filename = qualityT.dir..k
				loader = ProtectedLoadfile(filename)
				setfenv(loader, env)
				loader()
				loadedCount = loadedCount + 1
				--varOut("loaded: "..filename)
			end
		end
		if loadedCount > 0 then return true end -- files loaded
	end
end
---------- all file testing -------
function testFiles()
	loadQualityFiles(item, unique, true)
	loadQualityFiles(item, rare, true)
	loadQualityFiles(item, set, true)
	loadQualityFiles(item, magic, true)
	loadQualityFiles(item, white, true)
	Log(2, "gateway", "testFiles : Finished syntax checking all itemFiles")
	io.close()
end
------------------------- item handling ------------------------------
---------- main handler driver ----
function processItem(item, action)
	if item.flags.Runeword then return false end
	-- getStats
	_Stats = getStats(item)
	getResists(_Stats)
	getDefense(_Stats)
	getSkills(_Stats, item)
	getElementalDamage(_Stats, item)
	--tableOut(_Stats, "_Stats")

	-- file testing
	if _TestAllFiles then testFiles() end
	-- drive main handler
	if item.quality == ItemQuality.Unique then
		return processItemTables(item, action, unique)
	elseif item.quality == ItemQuality.Rare then
		return processItemTables(item, action, rare)
	elseif item.quality == ItemQuality.Set then
		return processItemTables(item, action, set)
	elseif item.quality == ItemQuality.Magic then
		return processItemTables(item, action, magic)
	elseif item.quality ~= ItemQuality.Inferior then
		return processItemTables(item, action, white)
	end
end
---------- main handler -----------
-- processItemTables: handles qualityTables, ex: unique
-- note: loadQualityFiles determines the potential files to load,
--  so every itemTable in the qualityTable is a potential handler
function processItemTables(item, action, qualityT)
	local loaded = loadQualityFiles(item, qualityT)
	if not loaded then return end -- return nil (unhandled)
	-- don't need these two elements
	qualityT.files = nil
	qualityT.dir = nil
	--tableOut(qualityT, "qualityT (main handler)")
	local handled
	local handledCount = 0
	local handlerTables = {}
	for k, v in pairs(qualityT) do
		-- check the itemTable to see if it really does handle the item
		handled = handleItemTable(item, action, v)
		--varOut(handled, "handled")
		if handled ~= nil then
			-- multiple handle logging
			handledCount = handledCount + 1
			if _LogWarnings then handlerTables[k] = handled
			else return handled
			end
		end
	end
	--tableOut(handlerTables, "handlerTables")
	--[[
	if _LogWarnings and handledCount > 1 then
		logMultipleHandle(item, handlerTables)
	end
	]]--
	for k, v in pairs(handlerTables) do
		if v ~= nil then return v end
	end
end
---------- sub handler ------------
-- itemTable metatable (defaults)
defaultTable = {__index =
	function(t, k)
		if k == "checkStats" then return true
		elseif k == "dump" then return true
		elseif k == "pickToSell" then return false
		end
	end}
-- itemEntry metatable (defaults)
defaultEntry = {__index =
	function(t, k)
		if k == "priority" then return 1
		elseif k == "identify" then return false
		elseif k == "dump" then return true
		elseif k == "pickToSell" then return false
		elseif k == "pickLimit" then return 0
		elseif k == "isGoodItem" then
			if t.goodItem ~= nil then
				return (function(item) return t.goodItem end)
			else return (function(item) return true end)
			end
		end
	end}
-- handleItemTable: handles itemTables, ex: unique.amulets
function handleItemTable(item, action, itemT)
	local code = item.baseItem.code
	local kind = _NewItemKind[item.baseItem.baseType.Type]
	local entry
	if itemT[code] then entry = itemT[code]
	elseif itemT[kind] then entry = itemT[kind]
	end
	--tableOut(itemT, "itemT")
	--tableOut(entry, "entry")
	if entry then
		setmetatable(itemT, defaultTable)
		setmetatable(entry, defaultEntry)
		--don't dump pickToSell items
		if entry.pickToSell or itemT.pickToSell then entry.dump = false end
		--pickLimit setting
		local inLimit
		if entry.pickLimit > 0 then inLimit = isWithinPickLimit(item, entry.pickLimit) else inLimit = true	end

		local goodItem = (entry.isGoodItem(item) or not itemT.checkStats) and inLimit
		local identItem = entry.identify and itemT.checkStats
		local badItem = (not goodItem or itemT.pickToSell or entry.pickToSell or not inLimit)
		local dumpItem = badItem and itemT.dump and entry.dump

		if action == _Pick and goodItem then return entry.priority
		elseif action == _Ident then return identItem
		elseif action == _Sell then return badItem
		elseif action == _Buy then return goodItem
		
		elseif action == _Dump then return dumpItem
		else return false
		end
	end
end

function isWithinPickLimit(item, limit)
    if (Ao:getNumber("sellingDisabled") ~= nil and Ao:getNumber("sellingDisabled") > 0) then
        return true
    end
    local cl = Ao.me.cube:findByCode(item.baseItem.code)
    local sl = Ao.me.stash:findByCode(item.baseItem.code)
    local il = Ao.me.inventory:findByCode(item.baseItem.code)
    local count = 0
 
    for i = 0, cl:size() - 1 do
        if cl[i].quality == item.quality then count = count + 1 end
    end
    for a = 0, il:size() - 1 do
        if il[a].quality == item.quality then count = count + 1 end 
	end
	for e = 0, sl:size() - 1 do
	    if sl[e].quality == item.quality then count = count + 1 end
    end
	if count < limit then
	    Log(2, "gateway", "isWithinPickLimit : Item passed picklimit : "..item.baseItem.code.." ("..item.quality..") = "..count.."/"..limit)
        return true
    else
        Log(2, "gateway", "isWithin PickLimit : Limit reached for : "..item.baseItem.code.." ("..item.quality..") = "..count.."/"..limit, true)
        return false
	end
end
function serialize(o)
	if type(o) == "number" then
		io.write(o)
	elseif type(o) == "string" then
		io.write(string.format("%q", o))
	elseif type(o) == "table" then
		io.write("{\n")
		for k,v in pairs(o) do
			io.write("   [")
			serialize(k)
			io.write("] = ")
			serialize(v)
			io.write(",\n")
		end
		io.write("}\n")
	end
end
------------------------- checkFileLimiters --------------------------
-- determines if the file is worth loading
-- i'd really like to find a better method for this
function checkFileLimiters(item, lim)
	if type(lim) == "table" then
		-- kind limiter
		if lim.kind ~= nil then
			-- redefine...
			if lim.kind == "AnyHelm" then
				lim.kind = {"Helm", "Circlet", "Pelt", "PrimalHelm"}
			elseif lim.kind == "ClassHelm" then
				lim.kind = {"Pelt", "PrimalHelm"}
			elseif lim.kind == "AnyShield" then
				lim.kind = {"Shield", "AuricShields", "VoodooHeads"}
			elseif lim.kind == "ClassShield" then
				lim.kind = {"AuricShields", "VoodooHeads"}
			elseif lim.kind == "AnyWeapon" then
				lim.kind = { -- actually more efficient than using noLimiters
					"Scepter", "Wand", "Staff", "Bow", "Axe", "Club", "Sword",
					"Hammer", "Knife", "Spear", "Polearm", "Crossbow", "Mace",
					"ThrowingKnife", "ThrowingAxe", "Javelin", "Weapon",
					"MeleeWeapon", "MissileWeapon", "ThrownWeapon", "ComboWeapon",
					"StavesAndRods", "Blunt", "HandToHand", "Orb", "AmazonBow",
					"AmazonSpear", "AmazonJavelin", "HandToHand2"}
			elseif lim.kind == "Charm" then
				lim.kind = {"SmallCharm", "MediumCharm", "LargeCharm"}
			elseif lim.kind == "Gem" then
				lim.kind = {"Amethyst", "Diamond", "Emerald", "Ruby",
					"Sapphire", "Topaz", "Skull"}
			end
			if type(lim.kind) == "string" then
				if not (lim.kind == _NewItemKind[item.baseItem.baseType.Type]
				or lim.kind == item.baseItem.code) then
					return false
				end
			elseif type(lim.kind) == "table" then
				local pass
				for i, v in ipairs(lim.kind) do
					if v == _NewItemKind[item.baseItem.baseType.Type] then
						pass = true ; break
					elseif v == item.baseItem.code then
						pass = true ; break
					end
				end
				if not pass then return false end
			else return false
			end
		end
		-- ethereal limiter
		if lim.ethereal ~= nil and lim.ethereal ~= item.flags.Ethereal then
			return false
		end
		-- passed all limiters
		-- must have kind or noLimiters
		return (lim.kind ~= nil or lim.noLimiters == true)
	end -- no limiters --> return nil
end
----------------------------------------------------------------------