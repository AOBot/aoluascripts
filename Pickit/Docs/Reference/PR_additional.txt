Identified
Unidentified
Ethereal
ItemLevel
ItemCode
ItemQuality
UniqueIndex
SetIndex
PrefixIndex
SuffixIndex
BaseDefense
SingleResist
DoubleResist
TripleResist
AllResist
MinResist
MaxResist
TotalResist

[SkillType]Skill
[SkillType]ChargesLevel
[SkillType]ChargesMax
[SkillType]OnStrikingLevel
[SkillType]OnStrikingChance
[SkillType]OnStruckLevel
[SkillType]OnStruckChance
*Ex: LightningSkill, LifeTapChargesLevel
*See "Skills.txt" for the full list of SkillTypes

[SkillTab]Tab
*Ex: PaladinCombatTab
*See "Skills.txt" for the full list of SkillTabs.

[CharacterClass]Skills
*Ex: PaladinSkills
*See "Skills.txt" for the full list of CharacterClasses