_PRMainDir = "scripts\\pickit\\"..Settings.Inventory.Pickit.."\\"
loadfile(_PRMainDir.."gateway.lua")()

function sellItem(item)
	if KeepForCubing(item) then return false end
	setVersion(item)
	if item.baseItem.baseType.Type ~= ItemKind.Quest then
        local sell = processItem(item, _Sell)

        if sell then return true
        elseif sell == nil and _SellUnlistedItems then return true
        end
    end
	return false
end

function identifyItem(item)
	setVersion(item)
	local ident = processItem(item, _Ident)

	if ident then return true end

	return false
end

function gambleItem(item)
	return
		(
			item.baseItem.baseType.Type == ItemKind.Circlet
			or item.baseItem.baseType.Type == ItemKind.Boots
			or item.baseItem.baseType.Type == ItemKind.Belt
			or item.baseItem.baseType.Type == ItemKind.Gloves			
			or item.baseItem.code == "rin"
			or item.baseItem.code == "amu"
		)
end

function dumpItem(item)
	if KeepForCubing(item) then return false end
	setVersion(item)
	local dump = processItem(item, _Dump)

	if dump then return true end

	return false
end

function buyShopItem(item)
	setVersion(item)
	local buy = processItem(item, _Buy)
	
	if buy then return true
	elseif buy == nil then return false
	end

	return false
end

function KeepForCubing(item)
	local checkitem, placeholder = 0, 0
	
	--if Settings.CubeGems then
		if #Settings.Craft.Flawed > 0 then
			for types = 1, #Settings.Craft.Flawed do
				if CHIPPED[types] == item.baseItem.code then
					return true
				end
			end
		end
	
		if #Settings.Craft.Normal > 0 then
			for types = 1, #Settings.Craft.Normal do
				if FLAWED[types] == item.baseItem.code then
					return true
				end
			end
		end
		
		if #Settings.Craft.Flawless > 0 then
			for types = 1, #Settings.Craft.Flawless do
				if NORMAL[types] == item.baseItem.code then
					return true
				end
			end
		end
		
		if #Settings.Craft.Perfect > 0 then
			for types = 1, #Settings.Craft.Perfect do
				if FLAWLESS[types] == item.baseItem.code or PERFECT[types] == item.baseItem.code then
					return true
				end
			end
		end
	--end
	
	--if Settings.CubeRunes then
		if #Settings.Craft.Rune > 0 then
			for checkrune = 1, #Settings.Craft.Rune do
				if RUNES[Settings.Craft.Rune[checkrune]] < 10 then
					placeholder = "r0"
					if placeholder..RUNES[Settings.Craft.Rune[checkrune]] == item.baseItem.code or placeholder..RUNES[Settings.Craft.Rune[checkrune]]-1 == item.baseItem.code then --checking if what we NEED to make or are trying to make is this item
						return true
					end
				end
				if RUNES[Settings.Craft.Rune[checkrune]] > 9 and RUNES[Settings.Craft.Rune[checkrune]] < 21 then
					placeholder = "r"
					if placeholder..RUNES[Settings.Craft.Rune[checkrune]] == item.baseItem.code or placeholder..RUNES[Settings.Craft.Rune[checkrune]]-1 == item.baseItem.code then
						return true
					end
					if FORMULA[RUNES[Settings.Craft.Rune[checkrune]]-1] == item.baseItem.code and (Ao.me.stash:findByCode(placeholder..RUNES[Settings.Craft.Rune[checkrune]]-1):size() + Ao.me.inventory:findByCode(placeholder..RUNES[Settings.Craft.Rune[checkrune]]-1):size()) > 2 then
						return true
					end
				end
				if RUNES[Settings.Craft.Rune[checkrune]] > 20 and RUNES[Settings.Craft.Rune[checkrune]] < 33 then
					placeholder = "r"
					if placeholder..RUNES[Settings.Craft.Rune[checkrune]] == item.baseItem.code or placeholder..RUNES[Settings.Craft.Rune[checkrune]]-1 == item.baseItem.code then
						return true
					end
					if FORMULA[RUNES[Settings.Craft.Rune[checkrune]]-1] == item.baseItem.code and (Ao.me.stash:findByCode(placeholder..RUNES[Settings.Craft.Rune[checkrune]]-1):size() + Ao.me.inventory:findByCode(placeholder..RUNES[Settings.Craft.Rune[checkrune]]-1):size()) > 1 then
						return true
					end
				end
			end
		end
	--end
	return false
end