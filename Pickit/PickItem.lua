loadfile("scripts\\includes.lua")()
IncludeSettings()
IncludeLibs()
_PRMainDir = "scripts\\pickit\\"..Settings.Inventory.Pickit.."\\"
loadfile(_PRMainDir.."gateway.lua")()
loadfile("scripts\\characters\\" ..Ao.character..".lua")()  --Dont ask me why but if its not there, we get errors... wtf???

pickItem = function(item)
	setVersion(item)
	if KeepForCubing(item) then
		Ao.pickit:add(item, 9)
		return Ao:RunNow("events\\pickit.lua", "Routine")
	end
	local checkitem = processItem(item, _Pick)
	if checkitem then
		Ao.pickit:add(item, tonumber(checkitem))
		Ao:set(item.uid, getEnumKey(AreaLevel,Ao.maps:getLevel()))
		return Ao:RunNow("events\\pickit.lua", "Routine")
	end
	if Ao.npcs:countInRadius(item.x, item.y, 10) > 10 then
		Log(1, "pickItem", "Skipping Pot/Scroll/Ammo/Gold Check Too many enemies")
		return
	end
	if belt.PotCheck(item) then
		Ao.pickit:add(item, 1)
	end
	if inventory.ScrollCheck(item) then
		Ao.pickit:add(item, 1)
	end
	if inventory.AmmoCheck(item) then
		Ao.pickit:add(item, 1)
	end
	if inventory.GoldCheck(item) then
		Ao.pickit:add(item, 1)
	end
	if not Ao.pickit:empty() then
		Ao:RunNow("events\\pickit.lua", "Routine")
	end
end

function CallPickitEvent(item, priority)
    Ao.pickit:add(item, priority)
	Ao:Run("events\\pickit.lua", "Routine")
end

